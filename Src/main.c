
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l4xx_hal.h"
#include "i2c.h"
#include "iwdg.h"
#include "lptim.h"
#include "usart.h"
#include "quadspi.h"
#include "rtc.h"
#include "spi.h"
#include "tim.h"
#include "usb_device.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "CYFISNP_Timer.h"
#include "CYFISNP_Protocol.h"
#include "CYFISNP_Private.h"
#include "CYFISNP_ISR.h"
#include "CYFISNP_Flash_Utility.h"
#include "CYFISNP_Eeprom.h"
#include "CYFISNP_Config.h"
#include "cyfi_regs.h"
#include "cyfi_radio.h"
#include "cyfi_hal.h"

#include "myusbd_custom_hid_if.h"
#include "MAX14676.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
#define BCD_IS_SUPPORTED    (CYFISNP_BCD_PAYLOAD_MAX != 0)	  // not supported

#define RF_BUF_NUM			    128	        // additional RF buffers

extern int8_t BCD_Cmd_Process(uint8_t *pBuffer, uint8_t USB_or_BLE);

uint8_t ga8TxBuffer[TX_BUF_SIZE] = {0};	// Tx buffer Frame, to the Host
uint8_t ga8RxBuffer[RX_BUF_SIZE] = {0};	// Rx buffer Frame, from the Host
uint8_t ga8RfBuffer[TX_BUF_SIZE] = {0};	// 2nd Tx buffer, for RF (18 Bytes used)
uint8_t gu8BindFlag              = 0;
uint8_t gu8LedTime               = 0;
uint8_t USB_OR_BLE               = BLE_CONNECTION;   // Default as BLE

__IO uint32_t CYFISNP_HeartBitTimer = 0;
__IO uint32_t GRN_Led_BlinkTimer    = 0;				// Green LED

uint32_t bleDownTimer           = 0;
uint32_t bleReBootTimer         = 0;
uint8_t  bleStatus              = 0;

uint32_t BTNDownTimer           = 0;
uint8_t  BTNStatus              = 0;

uint32_t gu32LedFlashTimer      = 0;
uint32_t gu32BuzzBeepTimer      = 0;		    // Buzz
uint8_t  gu8BuzzBeep            = 0;

CYFISNP_API_PKT *pRxApiPkt1; 		// ptr to FCD API data packet (Rx from Node)
struct {							          // 64 additional buffers for RF
	uint8_t         num;
	uint8_t			    inp;
	uint8_t			    outp;
	CYFISNP_API_PKT RxApiPkt[RF_BUF_NUM];
} gRxApiStruct={0};

const uint8_t hex[] = {"0123456789ABCDEF"};
uint8_t gaHubMid[1+1+7*2];                                                 //Usb Hid Serial Number (7 bytes: abcxxyy)

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/**
 * @brief  This function provides a delay (in microseconds)
 * @param  microseconds: delay in microseconds
 */
void Cy_DelayUS(uint32_t microseconds)
{
	__IO uint32_t count =0;
	__IO uint32_t count1 =0;
	#define count2    9
	while(count++ < microseconds)
	{		
		__RESET_WATCHDOG(); 
		while(count1++ < count2);
		count1 = 0;		
	}	
}

void Cy_DelayUS2(uint32_t microseconds)
{
	__IO uint32_t count =0;
	__IO uint32_t count1 =0;
	#define count2    9
	while(count++ < microseconds)
	{		
		//__RESET_WATCHDOG(); 
		while(count1++ < count2);
		count1 = 0;		
	}	
}

void RfDataCheck(void);
void serveRfPkt(CYFISNP_API_PKT *pRxApiPkt);
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
	RCC_CSR_reg = RCC->CSR;
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_LPTIM1_Init();
  MX_LPUART1_UART_Init();
  MX_QUADSPI_Init();
  MX_RTC_Init();
  MX_SPI1_Init();
  MX_TIM2_Init();
//MX_USB_DEVICE_Init();
  MX_IWDG_Init();
	
  /* USER CODE BEGIN 2 */
	HAL_NVIC_DisableIRQ(LPUART1_IRQn);	
	HAL_NVIC_DisableIRQ(EXTI0_IRQn);	
	HAL_NVIC_DisableIRQ(EXTI3_IRQn);
  HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
  HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);	
	
  max14676_init();	
	
	if(RCC_CSR_reg &(RCC_CSR_IWDGRSTF|RCC_CSR_SFTRSTF|RCC_CSR_OBLRSTF))//|RCC_CSR_PINRSTF))
	{
		//RCC_CSR_reg = 0;
		__HAL_RCC_CLEAR_RESET_FLAGS();		
	}
	else
		ReStart_Process();

  HAL_Delay(80);	
	RED_LED_On();
	
	HAL_Delay(100);	
	RED_LED_Off();	  
	BUZZ_Off();

  // Reboot BLE	
  PROG_Pin_mode(0);			
  PROG_Ctrl_LOW();
	HAL_Delay(5);
  BT_RST_LOW();
	HAL_Delay(5);
  BT_RST_HIGH();
	HAL_Delay(100);
	PROG_Pin_mode(1);			
		
	Radio_RST_LOW();
	HAL_Delay(50);	
	Radio_RST_HIGH();
	HAL_Delay(50);	
	Radio_RST_LOW();
	HAL_Delay(50);	
		
	CYFISNP_Write(CHANNEL_ADR, 0x55);
	BTNStatus = CYFISNP_Read(CHANNEL_ADR);
	if(BTNStatus == 0x55)
		GRN_LED_On();
	else
		RED_LED_On();
	
	HAL_Delay(1000);	
	GRN_LED_Off();
	
	while( !CYFISNP_Start()) 
	{
		__RESET_WATCHDOG(); 
	}

  {    
    byte const *pFlash = (byte const*)(CYFISNP_EEP_NET_REC_ADR);
    byte n;
		for(BTNStatus = 0; BTNStatus < 5; BTNStatus++) 
		{
			// 0: sopIdx 		a   0-9		( 2,  3)
			// 1: chBase 		b   0-5		( 4,  5)
			// 2: chHop  		c   0-7		( 6,  7)
			// 3: hubSeedMsb    xx		( 8,  9, 10, 11)
			// 4: hubSeedLsb    yy		(12, 13, 14, 15)
			n = pFlash[BTNStatus];
			if( BTNStatus <= 2 )      // a, b, c
			{	
				gaHubMid[BTNStatus]     = hex[n & 0x0F];
			} 
			else                      // xx, yy
			{		
				gaHubMid[(BTNStatus-3)*2 + 3] = hex[n >> 4];
				gaHubMid[(BTNStatus-3)*2 + 4] = hex[n & 0x0F];
			}
		}
		gaHubMid[7] = 0;
		
		MX_USB_DEVICE_Init();		
  }	
	
	Node_Table_Init();
	
	gRxApiStruct.num = gRxApiStruct.inp = gRxApiStruct.outp = 0;
	BTNStatus = 0;
		
	__HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_0);
	HAL_NVIC_EnableIRQ(EXTI0_IRQn);
	
  if (HAL_UART_Receive_IT(&hlpuart1, RxBuffer, 41) != HAL_OK)	
	{
		Error_Handler();
	}
	HAL_NVIC_EnableIRQ(LPUART1_IRQn);		
	HAL_NVIC_EnableIRQ(EXTI3_IRQn);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);		
	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
    __RESET_WATCHDOG(); 
		// -------------------------------------------------------------------
		// Blink the periodic "running" Green LED1
		// -------------------------------------------------------------------
		if( GRN_Led_BlinkTimer > BLINK_TIME )
		{			
		  if(CYFISNP_eProtState == CYFISNP_BIND_MODE)
			{
		    RED_LED_Toggle();
				GRN_LED_Off();
			}
			else
			{						
				if(ChagerStatusFlag == 0)
				{
					GRN_LED_Toggle();   
				}
				else if(ChagerStatusFlag == 1)         // Charge Done
				{
					GRN_LED_On();
					ChagerStatusFlag = 0xFF;		
          BUZZ_beep(40);					
				}
				else if( ChagerStatusFlag == 0xFF)
					;
				else
				{
					GRN_LED_Dime();
					//GRN_LED_Toggle();   
				}		  
			}
			GRN_Led_BlinkTimer = 0;
		}
			
		if(gu8BuzzBeep & CYFISNP_TimeExpired(&gu32BuzzBeepTimer))		// Buzz
		{ 
			gu8BuzzBeep = 0;
			BUZZ_Off();
		}

		// -------------------------------------------------------------------
		// Turn off Red LED2 when a packet was received
		// -------------------------------------------------------------------
		if( gu8LedTime && CYFISNP_TimeExpired(&gu32LedFlashTimer) )
		{
			gu8LedTime = 0;
			RED_LED_Off();				
			BLU_LED_Off();
		//GRN_LED_Off();			
		//CYFISNP_TimeSet(&gu32LedFlashTimer, BLINK_TIME2);
		}		
		
	  // -------------------------------------------------------------------
		// Start Binding as needed
		// -------------------------------------------------------------------
		if( /*CheckBindButton() ||*/ gu8BindFlag ) {
			gu8BindFlag = 0;
			CYFISNP_BindStart();
		}
		
		// -------------------------------------------------------------------
		// Run SNP state machine.
		// -------------------------------------------------------------------
		CYFISNP_Run();
		
		// -------------------------------------------------------------------
		// Handle received "forward channel" data packets from all nodes
		// -------------------------------------------------------------------
		RfDataCheck();
		if(	gRxApiStruct.num ) {
			pRxApiPkt1 = &gRxApiStruct.RxApiPkt[gRxApiStruct.outp];
			serveRfPkt(pRxApiPkt1); // Process Rx packet

			if( ++gRxApiStruct.outp >= RF_BUF_NUM )
				gRxApiStruct.outp = 0;
			gRxApiStruct.num -= 1;
		}
		
    if(ga8RxBuffer[0] != 0)
		{
			if(!USB_OR_BLE)
				BCD_Cmd_Process(ga8RxBuffer,0);
			else
				BCD_Cmd_Process(ga8RxBuffer,1);
			ga8RxBuffer[0] = 0;
		}
		
		if(bleStatus == 1)
		{		
      if(USB_OR_BLE)
			{				
				if(CYFISNP_TimeExpired(&bleDownTimer))
				{
					if(HAL_GPIO_ReadPin(BLE_STATUS_INT_GPIO_Port, GPIO_PIN_9))
					{
						bleStatus = 0xFF;
						//ChagerStatusFlag = 0;
						CYFISNP_Stop();
					}
					else
					{
						bleStatus = 0;
						bleReBootTimer = 0;						
					}
				}
		  }
		}
		else
		{
#if 1
			if(bleStatus == 0xFF) 
			{
					//  BLE Status Pin RISING Interrupt fail!
					if(!USB_OR_BLE)	
					{
						RED_LED_On();	
						HAL_Delay(1500);	
						
						//CYFISNP_Start();			
						//bleStatus = 0;		
						//bleReBootTimer = 0;
						NVIC_SystemReset();
					}
		  }			
#endif			
		}
		
		if(BTNStatus == 1)
		{			
			if(CYFISNP_TimeExpired(&BTNDownTimer))
			{							
        if(HAL_GPIO_ReadPin(BTN_GPIO_Port, GPIO_PIN_3) == GPIO_PIN_RESET)
				{
          //gu8BindFlag = 1;
				  RED_LED_On();
					BUZZ_On();
					HAL_Delay(100);
					BUZZ_Off();
					HAL_Delay(100);
					BUZZ_On();
					HAL_Delay(100);
					BUZZ_Off();
					RED_LED_Off();
					max14676_poweroff();
				}
				BTNStatus = 0;
			}
		}		
  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Configure LSE Drive Capability 
    */
  HAL_PWR_EnableBkUpAccess();

  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI
                              |RCC_OSCILLATORTYPE_LSE|RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_11;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 20;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV8;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_LPUART1
                              |RCC_PERIPHCLK_LPTIM1|RCC_PERIPHCLK_I2C1
                              |RCC_PERIPHCLK_USB;
  PeriphClkInit.Lpuart1ClockSelection = RCC_LPUART1CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_SYSCLK;
  PeriphClkInit.Lptim1ClockSelection = RCC_LPTIM1CLKSOURCE_HSI;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_MSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the main internal regulator output voltage 
    */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

    /**Enable MSI Auto calibration 
    */
  HAL_RCCEx_EnableMSIPLLMode();

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	CYFISNP_ISR_Interrupt();
}

void HAL_GPIO_EXTI_myCallback3(uint16_t GPIO_Pin)
{
  GPIO_PinState BTN_Int_Status;
	BTN_Int_Status = HAL_GPIO_ReadPin(BTN_GPIO_Port, GPIO_Pin);
	
	if(BTN_Int_Status == GPIO_PIN_RESET)
	{
		BTNStatus = 1;
		CYFISNP_TimeSet(&BTNDownTimer, 3000);		   // BTN activated after 3 seconds
	}
	else
	{
		BTNStatus = 0;
	}
}

void HAL_GPIO_EXTI_myIRQHandler3(uint16_t GPIO_Pin)
{
  /* EXTI line interrupt detected */
  if(__HAL_GPIO_EXTI_GET_IT(GPIO_Pin) != RESET)
  {
    __HAL_GPIO_EXTI_CLEAR_IT(GPIO_Pin);
    HAL_GPIO_EXTI_myCallback3(GPIO_Pin);
  }
}

void HAL_GPIO_EXTI_myCallback9(uint16_t GPIO_Pin)
{
  GPIO_PinState BLE_Int_Status;
	BLE_Int_Status = HAL_GPIO_ReadPin(BLE_STATUS_INT_GPIO_Port, GPIO_Pin);
	
	if(BLE_Int_Status ==  GPIO_PIN_SET)
	{
		gu8LedTime = 1;
		RED_LED_On();	  // will turn off after timer expires in the main()				
		CYFISNP_TimeSet(&gu32LedFlashTimer, BLINK_TIME2);		

	 //HAL_NVIC_DisableIRQ(EXTI0_IRQn);
    if(bleReBootTimer > 3)
		{
		//BLE Some Issue happened!
			NVIC_SystemReset();
		}
		
		bleStatus = 1;
		CYFISNP_TimeSet(&bleDownTimer, 150);		   // Shutdown CYFISNP after 150 milisenconds
	}
	else
	{	
		USB_OR_BLE = BLE_CONNECTION; 
		if(bleStatus == 0xFF)
		{
			CYFISNP_Start();			
		  bleStatus = 0;		
      bleReBootTimer++;
		}		
	//HAL_NVIC_EnableIRQ(EXTI0_IRQn);
	//NVIC_SystemReset();
	}
}

void HAL_GPIO_EXTI_myIRQHandler9(uint16_t GPIO_Pin)
{
  /* EXTI line interrupt detected */
  if(__HAL_GPIO_EXTI_GET_IT(GPIO_Pin) != RESET)
  {
    __HAL_GPIO_EXTI_CLEAR_IT(GPIO_Pin);
    HAL_GPIO_EXTI_myCallback9(GPIO_Pin);
  }
}

uint8_t USB_StatusA      = 0xFF;
uint8_t ChagerStatusFlag = 0;
void HAL_GPIO_EXTI_myCallback10(uint16_t GPIO_Pin)
{
  GPIO_PinState INT_CHG_Status;
  uint8_t       Int_StatusA; 
  uint8_t       Int_StatusB;
		
	INT_CHG_Status = HAL_GPIO_ReadPin(INT_CHG_GPIO_Port, GPIO_Pin);
	
	if(INT_CHG_Status ==  GPIO_PIN_RESET)
	{
		Int_StatusA = max14676_Read(MAX14676_INT_A);
		if(Int_StatusA & CHGSTAT_INT)
		{
			USB_StatusA = max14676_Read(MAX14676_STATUS_A);
			if(USB_StatusA == CHARGER_OFF)
			{
				ChagerStatusFlag = 0;
			}
			else if(USB_StatusA == CHARGER_TIMER_DONE)
			{
				ChagerStatusFlag = 1;
			}
			else
			{
				ChagerStatusFlag = 2;
			}
      //EnableCHG();			
		}				
		
		Int_StatusB = max14676_Read(MAX14676_INT_B);
		if(Int_StatusB & USBOK_INT)
		{
	    //EnableCHG();
		}				
		if((Int_StatusA & CHGSTAT_INT)||(Int_StatusB & USBOK_INT))
			EnableCHG();
	}
}

void HAL_GPIO_EXTI_myIRQHandler10(uint16_t GPIO_Pin)
{
	  /* EXTI line interrupt detected */
  if(__HAL_GPIO_EXTI_GET_IT(GPIO_Pin) != RESET)
  {
    __HAL_GPIO_EXTI_CLEAR_IT(GPIO_Pin);
    HAL_GPIO_EXTI_myCallback10(GPIO_Pin);
  }
}

void RfDataCheck(void)
{
	CYFISNP_API_PKT *pPkt; 		                   // ptr to FCD API data packet (Rx from Node)
	if( gRxApiStruct.num < RF_BUF_NUM ) {
    if( CYFISNP_RxdataPend())                  //TRUE  // Test for a pending Rx Pkt
		{
			pPkt = CYFISNP_RxdataGet();              // Get ptr to API Pkt Struct

			memcpy(&gRxApiStruct.RxApiPkt[gRxApiStruct.inp], pPkt, sizeof(CYFISNP_API_PKT));

			if( ++gRxApiStruct.inp >= RF_BUF_NUM )
				gRxApiStruct.inp = 0;	
			gRxApiStruct.num += 1;

	   CYFISNP_RxdataRelease(); // Must free Rx Packet buffer
		}
	}
}

void ClearCYFISNPHeartBit(void)
{
	 CYFISNP_HeartBitTimer = 0;
}

// ---------------------------------------------------------------------------
// serveRfPkt()
// ---------------------------------------------------------------------------
void serveRfPkt(CYFISNP_API_PKT *pRxApiPkt)
{
	byte *pdat, len;

	byte *p=ga8RfBuffer;

	__RESET_WATCHDOG();//M8C_ClearWDT;//AndSleep;
	
  BLU_LED_On();	gu8LedTime = 1;	// will turn off after timer expires in the main()
	CYFISNP_TimeSet(&gu32LedFlashTimer, BLINK_TIME2);
	
	ClearCYFISNPHeartBit();

	// typedef struct {                        //
	//     BYTE length;                        // Payload length
	//     BYTE rssi;                          // RSSI of packet
	//     BYTE type;                          // Packet type
	//     BYTE devId;                         // [7:0] = Device ID
	//     BYTE payload[14];                   //
	// } CYFISNP_API_PKT;
	//
	// payload[0] is Seq/Len
	// payload[1] is start of data
	//

	if( pRxApiPkt->type == CYFISNP_API_TYPE_BIND_RSP_ACK ) 
	{
		*p++ = MSG_HUB_TO_HOST_BIND_MODE;	// 0x12
		*p++ = 1;	// bound
		*p++ = pRxApiPkt->devId;
	} 
	else 
	{
	#ifdef JASON_COMMENT
		if( pRxApiPkt->length == (CHUNK_SHELF_MODE_U+1) ) {
			if( pRxApiPkt->payload[1] != 0 ) { 			// U1, Start
				// Usb cable plugged
				if( addNode_ShelfMode(pRxApiPkt->devId) ) {
					// if we are in Upload mode, but no Node is active now, start uploading this new one
					if( gu8UploadFlag && (CYFISNP_sysMode == 0) )
						CYFISNP_sysMode = getNode_ShelfMode();	// and set the timer
				}
			} else {									// U0, End
				// Usb cable unplugged
				removeNode_ShelfMode(pRxApiPkt->devId);			// this may end shelf mode
			}
		} else if( pRxApiPkt->length == (CHUNK_SHELF_MODE_D+1) ) {
			if( pRxApiPkt->payload[1] != 0 ) { 			// D1, Conti
				// wait
				setTimer_ShelfMode();
			} else {									// D0, End
				// go to next node
				CYFISNP_sysMode = getNode_ShelfMode();			// and set the timer
			}
		} 
		else if( (pRxApiPkt->length == (CHUNK_POWER_ON+1)) || (pRxApiPkt->length == (CHUNK_POWER_OFF+1)) ) 
		{
			if( numNode_ShelfMode() && isNode_ShelfMode(pRxApiPkt->devId) )
				removeNode_ShelfMode(pRxApiPkt->devId);				// this may end shelf mode
		}
	#endif	
		*p++ = MSG_HUB_TO_HOST_NODE_DATA;	// 0x10
		*p++ = pRxApiPkt->devId;
		*p++ = pRxApiPkt->rssi;
		*p++ = pRxApiPkt->length;

		len  = pRxApiPkt->length;         	// max 14
		pdat = &pRxApiPkt->payload[0];
		while(len--)
			*p++ = *pdat++;

		//TX8SW_PutChar(hex[pRxApiPkt->payload[0]>>4]);	// put the SeqNo on terminal for debugging

		// schedule the beacon for a little while later, we do not want to shoot a beacon when a node is uploading
#if 0//def JASON_MODIFICATION		                                            // Commented by Jason Chen, 2014.03.31
		if( (CYFISNP_sysMode>=1) && (CYFISNP_sysMode<=CYFISNP_MAX_NODES) )
		{
			scheduleBeacon();
		}
#endif		
	}	
	if(!USB_OR_BLE)
		SendToHost(ga8RfBuffer,0);	
	else
		SendToHostBLEUpload(ga8RfBuffer);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
