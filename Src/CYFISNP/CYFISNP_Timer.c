//*****************************************************************************
//*****************************************************************************
//  FILENAME: CYFISNP_Protocol.h
//  Version: 0.99, Updated on 20/07/2013

//
//  DESCRIPTION: <HUB>Star Network Protocol Header
//-----------------------------------------------------------------------------
//  Copyright (c) Cypress Semiconductor 2013. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#include "CYFISNP_Timer.h"
#include "CYFISNP_ISR.h"

__IO uint32_t CYFISNP_SleepTimer_TickCount;

// ---------------------------------------------------------------------------
//
// CYFISNP_TimeSet()     -  Set caller's timer for polling
// CYFISNP_TimeExpired() - Poll caller's timer
//
// ---------------------------------------------------------------------------


void CYFISNP_TimeSet(uint32_t *pTimer,uint32_t time)
{
	#if 0
    uint16_t tmpTime;  
    tmpTime = CYFISNP_SleepTimer_TickCount;
    *pTimer = tmpTime + time;
	#else
	  *pTimer = time;
	#endif
}

uint8_t CYFISNP_TimeExpired(uint32_t *pTimer)
{
	#if 0
    uint16_t tmpTime;
    tmpTime = CYFISNP_SleepTimer_TickCount;
    tmpTime = *pTimer - tmpTime;
    if(tmpTime > 0)
        return 0;
    else
       return 1;
	#else
		if(*pTimer == 0)
			return 1;
		else
			return 0;
	#endif
}
/* [] END OF FILE */
