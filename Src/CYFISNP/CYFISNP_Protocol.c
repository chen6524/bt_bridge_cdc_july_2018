//*****************************************************************************
//*****************************************************************************
//  FILENAME: CYFISNP_Protocol.c
//  Version:0.99 , Updated on 20/07/2013 
//
//  DESCRIPTION: <HUB> Star Network Protocol Protocol implementation
//-----------------------------------------------------------------------------
//  Copyright (c) Cypress Semiconductor 2013. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#define CYFISNP_PROTOCOL_C
#include "main.h"
#include "tim.h"                               // part specific constants and macros
#include "cyfi_radio.h"
#include "CYFISNP_Protocol.h"
#include "CYFISNP_ISR.h"

#include <string.h>
//#include "typedef.h"

#include "CYFISNP_Flash_Utility.h"

#define CY8C_KITx

// ---------------------------------------------------------------------------
// The defines are immutable
// ---------------------------------------------------------------------------
#define SNP_MAX_CHANNELS     (uint8_t)78      // 6 sets * 13 ch = 78 ch total
#define SNP_MAX_CH_SUBSETS   (uint8_t)6       //
#define TRUE                 (uint8_t)1
#define FALSE                (uint8_t)0 
#define CYFISNP_NUM_PN_CODES (uint8_t)10

//typedef uint16 WORD;
//extern uint8 rxIsrBufLen;
//extern uint8 rxIsrBufRssi;
extern __IO uint32_t CYFISNP_SleepTimer_TickCount;
extern uint8_t CYFISNP_State;
extern uint8_t txRetVec;
extern IsrPkt rxIsrBufPkt;

const uint8 *EEPROM_Pointer = myEEPROM;//NET_PARAMETER;//EEPROM;

// ---------------------------------------------------------------------------
// Indicates whether BCD support is required
// ---------------------------------------------------------------------------
//#define BCD_IS_SUPPORTED    (CYFISNP_BCD_PAYLOAD_MAX != 0)

// ---------------------------------------------------------------------------
//
// PUBLIC VARIABLES
//
// ---------------------------------------------------------------------------
CYFISNP_PROT_STATE  CYFISNP_eProtState;
byte                CYFISNP_bCurrentChannel;
byte                CYFISNP_bCurrentChannel_store;
CYFISNP_API_PKT     CYFISNP_RxApiPkt; // Public for ISR (not API)


#if BCD_IS_SUPPORTED
// ---------------------------------------------------------------------------
//
// CYFISNP_BcdBufs is a array of CYFISNP_MAX_NODES Back Channel data buffers
//
// ---------------------------------------------------------------------------

CYFISNP_BCD_PKT CYFISNP_BcdBufs[CYFISNP_MAX_NODES];
#endif // BCD_IS_SUPPORTED

                                     
// ---------------------------------------------------------------------------
//
// data Mode RSSI
//
// CYFISNP_RssiAveQ4 holds 4-bit readings with a K value of 4 applied
//
// New raw readings accululate as follows:   ave =  ave - (ave/2^K) + raw.
// With raw = 0xF, ave reaches a maximum of 0xF0 = 0xF0 - (0xF0/16) + 0x0F
//
// See 25-May-2006 EDN  http://www.edn.com/contents/images/6335310.pdf
// ---------------------------------------------------------------------------
#define K_VALUE             (uint8_t)4    // averaging coefficient
#define ON_CH_RSSI_THRESH   (uint8_t)8    // Bad Channel when averageRSSI exceeds this.
byte CYFISNP_RssiAveQ4; // [7:4] = average 4-bit RSSI
static word rssiSampleTimer;     // Sample at 64Hz timer tick rate.



// ---------------------------------------------------------------------------
//
// LOCAL FUNCTION DECLARATIONS
//
// ---------------------------------------------------------------------------
static void gotoPingMode    (void);
static void pingRun         (void);
static void dataRun         (void);
// -----------------------------------
static void makeNetParams   (void);
static void setNetParams    (void);
static void nextChan        (void);
static void pingNextChan    (void);

static void setMaxPaLevel   (void);
static void sendBeacon      (void);
static void bcdServeWall    (void);
static void sampleRssi      (void);
static void bcdSendPacket   (byte devId);
static void bcdSendPacket1  (byte devId);
static void setBindChannel  (void);
static void bindAllow       (CYFISNP_API_PKT *pApiPkt);
static void purgeRxdata     (void);
static void bindStop        (void);
static void bindRun         (void);
static void phySynth        (BOOL ackEn);
static void goTodataMode    (void);




// ---------------------------------------------------------------------------
// SNP PACKET HEADERS (some also exist in CYFISNP_Config.h)
// ---------------------------------------------------------------------------
#define PKT_BINDREQ_TYPE    (uint8_t)0x00
#define PKT_BINDREQ_MASK    (uint8_t)0xFC        // [1:0] are node attributes
#define PKT_BIND_REQ_LEN    (uint8_t)7
// ---------------------------------
#define PKT_CONREQ_TYPE     (uint8_t)0x00        // Uses Hub Seed
#define PKT_CONREQ_LEN      (uint8_t)6
#define PKT_BINDRSP_TYPE    (uint8_t)0x10        // Uses NODE MID[0][1] as CRC Seed[15:0]
#define PKT_BINDRSPDEL_TYPE (uint8_t)0x11
#define PKT_BINDRSP_LEN     (uint8_t)10

#define PKT_CONRSP_TYPE     (uint8_t)0x10        // Uses Seed given by Node in ConReq
#define PKT_CONRSP_LEN      (uint8_t)6
#define PKT_UNBIND_TYPE     (uint8_t)0x10        // Uses Seed given Node devId
#define PKT_UNBIND_LEN      (uint8_t)2

#define PKT_data_BCDR_MASK      (uint8_t)0x04        // BCDR bit SET in Rx data Packet
#define PKT_data_TYPE_MASK      (uint8_t)0xFE
#define PKT_data_CONF_TYPE      (uint8_t)0x20
#define PKT_data_CONFBCDR_TYPE  (uint8_t)(PKT_data_CONF_TYPE + PKT_data_BCDR_MASK)
#define PKT_data_SYNC_TYPE      (uint8_t)0x22
#define PKT_data_SYNCBCDR_TYPE  (uint8_t)(PKT_data_SYNC_TYPE + PKT_data_BCDR_MASK)
#define PKT_SEQUNK              (uint8_t)3        // Unknown

#define PKT_UNKNOWN_TYPE    (uint8_t)0xFF


// ---------------------------------------------------------------------------
//
// LOCAL VARIABLES
//
// ---------------------------------------------------------------------------
#if AUTO_ASSIGN_DEVICE_ID
    //#if CYFISNP_DEBUG
    //    BYTE nextDevId;         // Public for debug view only
    //#else
        static byte nextDevId;
    //#endif
#endif


// ---------------------------------------------------------------------------
//
// Local variables to support Wall Powered nodes
//
// ---------------------------------------------------------------------------
//static 
BOOL wallNodePresent;    // At LEAST 1 wall-powered Node is present

#define BEACON_TIME       (10000/CYFISNP_TIMER_UNITS)   // 1 sec
uint32_t beaconTimer;        // Nominally 1 second Beacon timer

// ---------------------------------------------------------------------------
// bcdTimer provides basic units for BCD retry delay
// ---------------------------------------------------------------------------
#define BCD_TIME    (20/CYFISNP_TIMER_UNITS)
uint32_t bcdTimer;
//word my_bcdTimer;

// ---------------------------------------------------------------------------
// bcdRetryDelay - Free running "master timer" to regulate wall-powered
//                 retransmissions.  Increments each Run() iteration.
// Although it occupies 8-bits, only the lower 6-bits are used in comparisons
//  because only 6-bits are allocated in the _BcdBufs array.
// ---------------------------------------------------------------------------
/*static*/ uint8_t bcdRetryDelay;

// ---------------------------------------------------------------------------
// BCD_TIMEOUT_COUNT - (A magic count)
//      Since the radio doesn't handle packets larger than 40 Bytes and the
//      _BcdBufs array allocates 6-bits to hold the "length" field, there are
//      a 23 values that can never occur in a real BCD packet.
// ---------------------------------------------------------------------------
#define BCD_TIMEOUT_COUNT   63

#define BCD_RETRY_MAX_15    15      // Limited by 4-bit field in _BcdBufs[]
// ---------------------------------------------------------------------------

/* ---------------------------------------------------------------------------
	bindAllow() - writes the node record to the flash
 --------------------------------------------------------------------------- */
//byte testIDmy[8];
static void bindAllow(CYFISNP_API_PKT *pApiPkt)
{
    CYFISNP_EEP_DEV_REC devRec;

    CYFISNP_phyIdle();     // Block Rx data while writing Flash

    // -----------------------------------------------------------------------
    // Create local RAM Device Record and write it to FLASH
    // -----------------------------------------------------------------------
    devRec.flg       = pApiPkt->type & PKT_FLAGS_MASK;
    devRec.devId     = pApiPkt->devId;
    devRec.devMid[0] = pApiPkt->payload[0];
    devRec.devMid[1] = pApiPkt->payload[1];
    devRec.devMid[2] = pApiPkt->payload[2];
    devRec.devMid[3] = pApiPkt->payload[3];
    devRec.rspDelay  = pApiPkt->payload[4];
    devRec.unused_1  = 0;
//  (void)memcpy(testIDmy, (byte*)&devRec, 8);
    CYFISNP_devRecWrite(pApiPkt->devId, &devRec); // Write to FLASH

    CYFISNP_phyRxGo();             // Permit Rx data now
      

	/* -------------------------------------------------------------------
       If a wall-powered Node is bound, then start Beacon transmissions
    ------------------------------------------------------------------- */
   if ((devRec.flg & CYFISNP_PWR_TYPE_MASK) == CYFISNP_PWR_WALL
   && wallNodePresent == FALSE) 
    {
        wallNodePresent = TRUE;
        CYFISNP_TimeSet(&beaconTimer, BEACON_TIME);
        CYFISNP_TimeSet(&bcdTimer,    BCD_TIME);
    }        
}


/* ---------------------------------------------------------------------------

CYFISNP_UnbindNodeID() - Unbind devId.  Returns FALSE if invalid devId specified.

--------------------------------------------------------------------------- */
BOOL CYFISNP_UnbindNodeID(byte NodeID)
{
    CYFISNP_EEP_DEV_REC devRec;

    if (NodeID == 0 || NodeID > CYFISNP_MAX_NODES) {
        return(FALSE);
    }

    /* -----------------------------------------------------------------------
      Only erase if FLASH Record is occupied (indicated by devId != 0)
    ----------------------------------------------------------------------- */
    if (*(EEPROM_Pointer + GET_DEV_INDEX(NodeID) + 1) != 0) {
        devRec.devId     = 0;
        devRec.flg       = 0;
        devRec.devMid[0] = 0;
        devRec.devMid[1] = 0;
        devRec.devMid[2] = 0;
        devRec.devMid[3] = 0;
        devRec.rspDelay  = 0;
        devRec.unused_1  = 0;
        CYFISNP_devRecWrite(NodeID,&devRec); // Erase Node record from FLASH
    }
    return(TRUE);
}

/* ---------------------------------------------------------------------------
   CYFISNP_phyRxGo() - Set RX_GO bit in Radio
  -------------------------------------------------------------------------- */
void CYFISNP_phyRxGo(void)
{
    // IRQ should NOT happen.
    if (CYFISNP_ePhyState != CYFISNP_PHY_RXGO) {
        CYFISNP_SetXactConfig(0
                                | ACK_EN
                                | ACK_TO_8X
                                | END_STATE_RXSYNTH);
        CYFISNP_ePhyState = CYFISNP_PHY_RXGO;
        CYFISNP_startReceive();                
    }
}

/* ----------------------------------------------------------------------------
   CYFISNP_phyIdle() - Change radio to IDLE, may involve stopping RxGo ISR
                    Involves stopping Rx ISR.
//---------------------------------------------------------------------------- */
void CYFISNP_phyIdle(void)
{
    if (CYFISNP_ePhyState == CYFISNP_PHY_RXGO) {
        // If ISR runs, exit w/o RxGo
        CYFISNP_ePhyState = CYFISNP_PHY_IDLE;
        CYFISNP_SetXactConfig(0
                                | ACK_EN
                                | ACK_TO_8X
                                | END_STATE_IDLE);

        // -------------------------------------------------------------------
        // Don't use _Abort() because if Rx packet arrives, ISR should
        //  handle the packet, NOT _Abort().
        // -------------------------------------------------------------------
        CYFISNP_Write(RX_ABORT_ADR,
                      ABORT_EN);            // Disconnect antenna
        Cy_DelayUS2(2000);                   //Cpu_Delay100US(20);
        CYFISNP_ForceState(END_STATE_IDLE);
        CYFISNP_Write(RX_ABORT_ADR, 0);     // Restore antenna
    }
    else {
        CYFISNP_ForceState(END_STATE_IDLE);
        CYFISNP_ePhyState = CYFISNP_PHY_IDLE;
    }
}

//----------------------------------------------------------------------------
//
// phySynth() - Change radio to SYNTH, may involve stopping RxGo ISR
//                  Involves stopping Rx ISR.
//
//----------------------------------------------------------------------------
static void phySynth(BOOL ackEn)
{
    if (CYFISNP_ePhyState == CYFISNP_PHY_RXGO) {
        CYFISNP_ePhyState = CYFISNP_PHY_SYNTH;       // If ISR runs, exit w/o RxGo
        if (ackEn) {
            CYFISNP_SetXactConfig(0
                                | ACK_EN
                                | ACK_TO_8X
                                | END_STATE_RXSYNTH);
        } else {
            CYFISNP_SetXactConfig(0
                                | ACK_TO_8X
                                | END_STATE_RXSYNTH);
        }

        // -------------------------------------------------------------------
        // Don't use _Abort() because if Rx packet arrives, ISR should
        //  handle the packet, NOT _Abort().
        // -------------------------------------------------------------------
        // Disconnect antenna
        CYFISNP_Write(RX_ABORT_ADR,
                              ABORT_EN);
        // If Rx packet is arriving, SOP will arrive within 200 uS
        Cy_DelayUS2(300);                   // Cpu_Delay100US(2);// 200 uS Delay
        if (CYFISNP_Read(RX_IRQ_STATUS_ADR)
                                & SOFDET_IRQ)
        {
            // Rx packet will ALWAYS complete within 2 mS
            //  and Radio will be in RX_SYNTH after this.
            Cy_DelayUS2(3000);          // Cpu_Delay100US(20);
        } else {
            // Impossible to get Rx Packet, so Safe to FORCE the radio
            CYFISNP_ForceState(END_STATE_RXSYNTH);
        }
        // Restore antenna
        CYFISNP_Write(RX_ABORT_ADR, 0);
    }
    else {
        CYFISNP_ForceState(END_STATE_RXSYNTH);
        CYFISNP_ePhyState = CYFISNP_PHY_SYNTH;
    }
}


// ---------------------------------------------------------------------------
// CYFISNP_NextChan() - API Test Only
// ---------------------------------------------------------------------------
void CYFISNP_NextChan(void)
{
#if 1               // Should do a PING first on next channel
    gotoPingMode();
#else               // This goes to next channel WITHOUT doing a PING
    nextChan();     // ++ channel
    setNetParams(); // Write parameters to Radio and stay in data_MODE
#endif
}
// ---------------------------------------------------------------------------
// nextChan() -
// ---------------------------------------------------------------------------
static void nextChan(void)
{
    char hop;
    hop = *(EEPROM_Pointer + 2) + 1;
    hop = (hop * 2) + (hop * 4);                // SNP_MAX_CH_SUBSETS;
  //CYFISNP_bCurrentChannel += hop;
  //CYFISNP_bCurrentChannel = CYFISNP_EEP_NET_REC_ADR->chBase + hop;  // make the current channel fixd
    CYFISNP_bCurrentChannel = *(EEPROM_Pointer + 1) + hop;            // make the current channel fixd
    if (CYFISNP_bCurrentChannel  > CYFISNP_CHAN_MAX) {
        CYFISNP_bCurrentChannel -= SNP_MAX_CHANNELS;
    }
    while ((signed char)CYFISNP_bCurrentChannel < CYFISNP_CHAN_MIN) {
        CYFISNP_bCurrentChannel += hop;
    }
}

void MyNextChan(void)
{
    char hop;
    hop = *(EEPROM_Pointer + 2) + 1;
    hop = (hop * 2) + (hop * 4);                // SNP_MAX_CH_SUBSETS;
  //CYFISNP_bCurrentChannel += hop;
  //CYFISNP_bCurrentChannel = CYFISNP_EEP_NET_REC_ADR->chBase + hop;  // make the current channel fixd
    CYFISNP_bCurrentChannel = *(EEPROM_Pointer + 1) + hop;            // make the current channel fixd
    if (CYFISNP_bCurrentChannel  > CYFISNP_CHAN_MAX) {
        CYFISNP_bCurrentChannel -= SNP_MAX_CHANNELS;
    }
    while ((signed char)CYFISNP_bCurrentChannel < CYFISNP_CHAN_MIN) {
        CYFISNP_bCurrentChannel += hop;
    }
}


// ---------------------------------------------------------------------------
//  setNetParams() - Write Network parameters to radio
// ---------------------------------------------------------------------------
static void setNetParams(void)
{
    CYFISNP_PHY_STATE tmpPhyState;

    if (CYFISNP_ePhyState == CYFISNP_PHY_SYNTH
     || CYFISNP_ePhyState == CYFISNP_PHY_RXGO) {
        tmpPhyState = CYFISNP_ePhyState;   // Remember original state
        CYFISNP_phyIdle();         // Idle synthesizer before write Ch
        
    }
    CYFISNP_SetChannel   (CYFISNP_bCurrentChannel);
    CYFISNP_SetSopPnCode(*(EEPROM_Pointer));
    CYFISNP_SetCrcSeed  ((word)((*(EEPROM_Pointer + 3)) << 8)
                                | *(EEPROM_Pointer + 4));
    if (tmpPhyState != CYFISNP_ePhyState) {         // If stopped synthesizer
        if (tmpPhyState == CYFISNP_PHY_SYNTH)       // Then restore to SYNTH
            phySynth(TRUE);                 // expect AutoAck
        else if (tmpPhyState == CYFISNP_PHY_RXGO)   // or RXGO
            CYFISNP_phyRxGo();
    }
}

// ---------------------------------------------------------------------------
//
// CYFISNP_Start() - Powerup initialization (Radio powers-up in IDLE)
//
// ---------------------------------------------------------------------------
byte CYFISNP_Start(void)
{

    if (CYFISNP_eProtState != 0 && CYFISNP_eProtState != CYFISNP_STOP_MODE) {
        return 0;         // START only works at powerup and from STOP
    }
     //CYFISNP_SPIM_Start();
     //EEPROM_Start();

//    EEPROM_EraseSector(0);
//    SleepTimer_Start();
//    SleepTimer_ISR_Start();
    //CYFISNP_ISR_Start();
    if(!CYFISNP_PhyStart()) {
    
        // CYFISNP_OutStr("\n\rRadio Init Failed!\n\r");
        //while(1);
        return 0;
    }

    CYFISNP_Write(XTAL_CTRL_ADR, XOUT_FNC_GPIO);


    makeNetParams();            // Ensure Hub Network Params are in FLASH
	  CYFISNP_SetPreambleCount(PREAMBLE_LEN_RST);

    CYFISNP_hubSeedMsb = *(EEPROM_Pointer + 3);
    CYFISNP_hubSeedLsb = *(EEPROM_Pointer + 4);

    CYFISNP_SetXactConfig( 0
                         | ACK_EN
                         | ACK_TO_8X
                         | END_STATE_RXSYNTH
                         );

    CYFISNP_radioTxConfig = CYFISNP_Read(TX_CFG_ADR);

    CYFISNP_bCurrentChannel = *(EEPROM_Pointer + 1);
    CYFISNP_bCurrentChannel_store = CYFISNP_bCurrentChannel;
    
        
    //gotoPingMode();

    // -----------------------------------------------------------------------
    // Scan entire Node Bind table
    //    typedef struct {                // ---------------------------------
    //    BYTE flg;                       //
    //    BYTE devId;                     // [7:0] = devID, 0 = empty
    //    BYTE devMid[CYFISNP_SIZEOF_MID];      //
    //    BYTE rspDelay;                  //
    //    BYTE unused_1;                  //
    //} CYFISNP_EEP_DEV_REC;     // ---------------------------------
    // -----------------------------------------------------------------------
    {
        byte testDevId;
        #if AUTO_ASSIGN_DEVICE_ID
            #define EMPTY 0
            nextDevId       = EMPTY;
        #endif
        wallNodePresent = FALSE;

        for(testDevId=1; testDevId <= CYFISNP_MAX_NODES; ++testDevId ) {
            #if AUTO_ASSIGN_DEVICE_ID
                // -----------------------------------------------------------
                // Find first empty Node entry in Flash table
                // -----------------------------------------------------------
                if (nextDevId == EMPTY && *(EEPROM_Pointer + GET_DEV_INDEX(testDevId) + 1 )  == 0) {
                    nextDevId = testDevId;  // Use 1st available as nextDevId
                }
            #endif

            // ---------------------------------------------------------------
            // Find if any assigned Nodes are Wall Powered
            // (Wall powered nodes mean hub needs to provide periodic Beacon)
            // ---------------------------------------------------------------
            if ((wallNodePresent == FALSE)
                 &&   (*(EEPROM_Pointer + GET_DEV_INDEX(testDevId)) & CYFISNP_PWR_TYPE_MASK)
                                 == CYFISNP_PWR_WALL) 
						{
                wallNodePresent = TRUE;
                CYFISNP_TimeSet(&beaconTimer, BEACON_TIME);
                CYFISNP_TimeSet(&bcdTimer, BCD_TIME);
            }
        }
    }
    #if AUTO_ASSIGN_DEVICE_ID
        if (nextDevId == EMPTY) {
            nextDevId = 1;          // MAX nodes are bound, recycle node IDs
        }
    #endif
    //gotoPingMode();
    nextChan();
    if(wallNodePresent)// == TRUE)
      goTodataMode();
    else
      gotoPingMode();
    
    return(1);
}


// ---------------------------------------------------------------------------
//
// CYFISNP_Stop() - Can be used to "abort" a pending transmission.
//
// ---------------------------------------------------------------------------
void CYFISNP_Stop(void)
{
    CYFISNP_PhyStop();
    //CYFISNP_ISR_Disable();                                     // Removed, 2015.07.20
//    SleepTimer_Stop();
//    SleepTimer_ISR_Stop();
    CYFISNP_eProtState = CYFISNP_STOP_MODE;
}


// ---------------------------------------------------------------------------
//
// CYFISNP_Run() - Polled periodic Protocol service
//
// ---------------------------------------------------------------------------
void CYFISNP_Run(void)
{
    //byte eProtState = (byte)(CYFISNP_eProtState & 0xF0);
    switch (CYFISNP_eProtState & 0xF0) {
        case CYFISNP_BIND_MODE:     bindRun();     break;
        case CYFISNP_PING_MODE:     pingRun();     break;
        case CYFISNP_data_MODE:     dataRun();     break;
        default:
           //CYFISNP_eProtState = CYFISNP_data_MODE;
           //dataRun();   
           //goTodataMode();
           break;
            
    }
}

// ---------------------------------------------------------------------------
//
//  PING MODE -  Send Ping w/ZeroSeedCRC and HubSop.  Reject ch if get AutoAck
//
//  Take 32 RSSI readings as fast as possible (12 uS is fastest).
//   If any reading is above threshold (28), then reject this channel.
//
// RSSI channel threshold during Ping Mode.  Starts at START_THRESHOLD and
//  threshold incremetns each cycle through channel subset until a channel
//  is found with 32 consecutive ASAP RSSI readings below threshold.
//
// NOTE: the Low Noise Amplifier LNA and Attenuation alters the RSSI readings
//       and must be handled in the future TBD
// ---------------------------------------------------------------------------
#define     PING_RSSI_START_THRESHOLD   18
static byte pingRssiThreshold;
static unsigned char pingTxPkt;


// ---------------------------------------------------------------------------
// gotoPingMode() -
// ---------------------------------------------------------------------------
static void gotoPingMode(void)
{
    CYFISNP_eProtState = CYFISNP_PING_MODE;
    pingRssiThreshold = PING_RSSI_START_THRESHOLD;
    pingNextChan();
    setMaxPaLevel();
}

void gotoMyPingMode(void)
{
    CYFISNP_eProtState = CYFISNP_PING_MODE;
    pingRssiThreshold = PING_RSSI_START_THRESHOLD;
    pingNextChan();
    setMaxPaLevel();
}

// ---------------------------------------------------------------------------
//
// pingRun() - Read RSSI on current channel, if HIGH or get Ping AutoAck
//             THEN change to next channel and return.
//              ELSE goto data Mode.
//
// ---------------------------------------------------------------------------
static void pingRun(void)
{
    char ivar;
    char raw;
    // -----------------------------------------------------------------------
    // Read RSSI 32 times looking for all readings below floating threshold
    // -----------------------------------------------------------------------
    (void)CYFISNP_Read(RSSI_ADR);       // dummy RSSI read
    for (ivar=32; ivar != 0; --ivar) {
        raw = CYFISNP_Read(RSSI_ADR) & RSSI_LVL_MSK;
        if (raw > pingRssiThreshold) {
            break;
        }
    }
    // -----------------------------------------------------------------------
    // If RSSI is HIGH, reject this channel
    // -----------------------------------------------------------------------
    if (ivar != 0) {
        pingNextChan();
    }
    // -----------------------------------------------------------------------
    // Else RSSI is OK, try a Tx Ping...
    // -----------------------------------------------------------------------
    else {
        pingTxPkt = CYFISNP_PKT_PING_TYPE;
        //CYFISNP_TxHandlerNone();            // Block ISR Tx handler
        txRetVec = 0;
        CYFISNP_SetPtr(&pingTxPkt);
        CYFISNP_StartTransmit(0, CYFISNP_PKT_PING_LEN);
      //while ((CYFISNP_State & CYFISNP_COMPLETE) == 0);  // Wait DONE
			  while ((CYFISNP_State & CYFISNP_COMPLETE) == 0)
				{
					__RESET_WATCHDOG(); 
				}
        // -------------------------------------------------------------------
        // If got AutoAck, then reject this channel
        // -------------------------------------------------------------------
        if ((CYFISNP_State & CYFISNP_ERROR) == 0) {
            pingNextChan();
        }
        // -------------------------------------------------------------------
        // Else no Ping AutoAck, THIS CHANNEL IS GOOD FOR data
        // -------------------------------------------------------------------
        else {
            goTodataMode();
        }
    }
}

// ---------------------------------------------------------------------------
//
// pingNextChan()
//
// ---------------------------------------------------------------------------
static void pingNextChan(void)
{
    // -----------------------------------------------------------------------
    // Change radio channel
    // -----------------------------------------------------------------------
    CYFISNP_phyIdle();              // Idle Radio before write Ch
    nextChan();
    CYFISNP_SetChannel   (CYFISNP_bCurrentChannel);
    CYFISNP_SetSopPnCode(*(EEPROM_Pointer));
    CYFISNP_SetCrcSeed  (CYFISNP_CRC_ZERO_SEED);

    // -----------------------------------------------------------------------
    // Radio finishes transmit in RECEIVE MODE so can read RSSI
    // -----------------------------------------------------------------------
    CYFISNP_SetXactConfig(0 | ACK_EN
                            | ACK_TO_8X
                            | END_STATE_RX);
    CYFISNP_ForceState(END_STATE_RX);

    // -------------------------------------------------------------------
    // Increment RSSI Threshold each cycle through Channel Subset
    // -------------------------------------------------------------------
    if (CYFISNP_bCurrentChannel == *(EEPROM_Pointer + 1)) {
        pingRssiThreshold = (pingRssiThreshold + 1) & RSSI_LVL_MSK;
        if (pingRssiThreshold < PING_RSSI_START_THRESHOLD) {
            pingRssiThreshold = PING_RSSI_START_THRESHOLD;
        }
    }
}

// ---------------------------------------------------------------------------
//
//  goTodataMode() - Enter data Mode
//
// ---------------------------------------------------------------------------
static void goTodataMode(void)
{
    setNetParams();
    setMaxPaLevel();
    CYFISNP_phyRxGo();
    CYFISNP_eProtState = CYFISNP_data_MODE;
    rssiSampleTimer = CYFISNP_SleepTimer_TickCount;
    CYFISNP_RssiAveQ4 = 0;
    (void)CYFISNP_Read(RSSI_ADR);   // Dummy RSSI read
}


void goToMydataMode(void)
{
    setNetParams();
    setMaxPaLevel();
    CYFISNP_phyRxGo();
    CYFISNP_eProtState = CYFISNP_data_MODE;
    rssiSampleTimer = CYFISNP_SleepTimer_TickCount;
    CYFISNP_RssiAveQ4 = 0;
    (void)CYFISNP_Read(RSSI_ADR);   // Dummy RSSI read
}


// ---------------------------------------------------------------------------
//
//  dataRun() - Run data Mode
//
// ---------------------------------------------------------------------------
static void dataRun(void)
{
    // -----------------------------------------------------------------------
    // If a Wall powered node is bound, then provide associated services
    // -----------------------------------------------------------------------
    if (wallNodePresent)// == TRUE) 
    {  
        // -------------------------------------------------------------------
        // Periodically transmit a Beacon
        // -------------------------------------------------------------------
        if (CYFISNP_TimeExpired(&beaconTimer)) {
            CYFISNP_TimeSet(&beaconTimer, BEACON_TIME);
            sendBeacon();
        }
        #if BCD_IS_SUPPORTED
        bcdServeWall();  // Periodically serve pending wall powered BCD packets
        #endif              
    }
    // -----------------------------------------------------------------------
    // Periodic RSSI sampling
    // -----------------------------------------------------------------------
    if (rssiSampleTimer != CYFISNP_SleepTimer_TickCount) 
    {         
         //rssiSampleTimer  = CYFISNP_SleepTimer_TickCount;
         //sampleRssi();
    }
}





#if BCD_IS_SUPPORTED
// ---------------------------------------------------------------------------
//
// bcdServeWall() - Periodically serve any pending wall powereed BCD packets
//
// ---------------------------------------------------------------------------
//static uint8_t tempCount = 0;
static void bcdServeWall(void)
{
    static byte testId;
	  byte delay;

    #define BCD_LEN_OFS 0
    #define BCD_DLY_OFS 1

    CYFISNP_BCD_PKT *pBcdBuf;  // Ptr to a specific BCD struct
    
    if (CYFISNP_TimeExpired(&bcdTimer)){// == TRUE) {
        CYFISNP_TimeSet(&bcdTimer, BCD_TIME);        
        ++bcdRetryDelay;     
		}		 

    // Check next device ID
    if (++testId > CYFISNP_MAX_NODES){
        testId = 1;
    }
				
    pBcdBuf = &CYFISNP_BcdBufs[testId-1];       
    // -----------------------------------------------------------------------
    // Exit if no BCD is pending
    // -----------------------------------------------------------------------
    if ((pBcdBuf->wprCntLen & BCDBUFS_LEN_MASK) == 0)
        return;
		      
    // -----------------------------------------------------------------------
    // Exit if not wall powered node
    // -----------------------------------------------------------------------
		// GET_DEV_INDEX(testId)  ===> testId << 3
//#define PWR_TYPE (*(EEPROM_Pointer + GET_DEV_INDEX(testId)) & CYFISNP_PWR_TYPE_MASK)
//    if (PWR_TYPE != CYFISNP_PWR_WALL) {      
//        return;
//    }

    // -----------------------------------------------------------------------
    // Exit if delay hasn't been reached (only care about lower 6-bits)
    // -----------------------------------------------------------------------
		delay = pBcdBuf->wprCntDly & BCDBUFS_DELAY_MASK;
    if (((bcdRetryDelay - delay) & ((BCDBUFS_DELAY_MASK+1)>>1)))
        return;
    		
    // -----------------------------------------------------------------------
    // Exit if retransmit TIMEOUT has been reached
    // -----------------------------------------------------------------------
    if ((pBcdBuf->wprCntLen & BCDBUFS_LEN_MASK) == BCD_TIMEOUT_COUNT) {
        return;
    }

    // -----------------------------------------------------------------------
    // Send a BCD packet to wall powered node now
    // -----------------------------------------------------------------------    
    bcdSendPacket(testId);		
}
#endif // BCD_IS_SUPPORTED


// ---------------------------------------------------------------------------
// sendBeacon() - Send beacon for wall-powered node(s)
// ---------------------------------------------------------------------------
extern __IO uint32_t CYFISNP_HeartBitTimer;
static void sendBeacon(void)
{
#if 0	
    byte beaconTxPkt[4];
    
    phySynth(FALSE);        // Radio in Synth before Tx, no AutoAck
    CYFISNP_SetCrcSeed(CYFISNP_CRC_ZERO_SEED);
    beaconTxPkt[0] =   CYFISNP_PKT_PING_TYPE;
    beaconTxPkt[1] =   *(EEPROM_Pointer + 3);
    beaconTxPkt[2] =   *(EEPROM_Pointer + 4);
    beaconTxPkt[3] =   CYFISNP_bCurrentChannel;

    //CYFISNP_TxHandlerNone();            // Block ISR Tx handler
    txRetVec = 0;
    CYFISNP_SetPtr(beaconTxPkt);
    CYFISNP_StartTransmit(0, CYFISNP_PKT_BEACON_LEN);
    while ((CYFISNP_State & CYFISNP_COMPLETE) == 0)  // Wait DONE
		{
			Cy_DelayUS2(5);	
		}

    setNetParams();
    CYFISNP_phyRxGo();          // Also, enable AutoAck
#else
		if(CYFISNP_HeartBitTimer > (18*BEACON_TIME))      // Reset whole system if no wireless packets come in after 90 seconds.
		{
			NVIC_SystemReset();
		}
#endif		
}

// ---------------------------------------------------------------------------
//
// sampleRssi() - Periodically sample and monitor RSSI
//
// Read RSSI, if SOP set, pass value to ISR and skip this sample.
//  because it will probably be during an arriving packet.
//
// Must manage GIE because CYFISNP_Read() uses SPI which disables GIE and
//  restores to a fixed exit state.  If ISR hits after the Read() but
//  before storing in CYFISNP_RssiPolled variable, then ISR won't see RSSI
//  if it needs to consult CYFISNP_RssiPolled variable.  When reenable GIE,
//  must make SPI exit w/GIE enabled BEFORE manually enable GIE.
// ---------------------------------------------------------------------------
static void sampleRssi(void)
{
    char raw;
    char filtered;
    extern byte CYFISNP_RssiPolled;
    //CyGlobalIntDisable;                         // 2.                      GIE = 0
    DisableInterrupts;                 
    raw = CYFISNP_Read(RSSI_ADR);
    CYFISNP_RssiPolled = raw & RSSI_LVL_MSK;
   
    //CyGlobalIntEnable;                          // 2.                      GIE = 1
    EnableInterrupts; 

    if (raw & SOP_RSSI) {  // If SOP flag set,
        return;                             //  then discard this sample window
    }

    // -----------------------------------------------------------------------
    // Read 5-bit RSSI, truncate to 4-bits and add to IIR filter
    // K_VALUE = 4 gives 34-sample risetime (500 mS @16mS/sample or 2 Hz).
    // -----------------------------------------------------------------------
    raw = (raw & RSSI_LVL_MSK) >> 1;   // Reduce to 4-bit value
    CYFISNP_RssiAveQ4 = CYFISNP_RssiAveQ4
                 - (CYFISNP_RssiAveQ4 >> K_VALUE) + raw;

    filtered = CYFISNP_RssiAveQ4 >> K_VALUE;   // Ave RSSI value {0-15}

#if !CYFISNP_TEST_DISABLE_RSSI_CHANNEL_CHANGE
    if (filtered > ON_CH_RSSI_THRESH) {
        gotoPingMode();
    }
#endif
}




// ---------------------------------------------------------------------------
//
// Bind-related parameters
//
// ---------------------------------------------------------------------------
static unsigned char bindChIdx;
byte CYFISNP_BindChannel;       // Public for debug use only

// ---------------------------------------------------------------------------
// BIND_CH_WAIT_TIMER = Time to spend on each channel before advance.  Stay
//                      on each channel long enough for node to scan to it.
// ---------------------------------------------------------------------------
#define BIND_CH_WAIT_TIME   (2000/CYFISNP_TIMER_UNITS)
/*static*/ uint32_t bindChWaitTimer;

// ---------------------------------------------------------------------------
// bindChCount = # of channels to visit during Binding
// ---------------------------------------------------------------------------
static byte bindChCount;

// ---------------------------------------------------------------------------
// _BindProgress = ISR progress through the Bind Req/Rsp/Ack
// ---------------------------------------------------------------------------
extern BOOL CYFISNP_BindProgress;
#define PASSED_BIND_NONE        0
//rwr#define PASSED_BIND_REQ         1
//rwf#define PASSED_BIND_RSP         2
//rwf#define PASSED_BIND_RSP_ACK     3

// ---------------------------------------------------------------------------
// apiHasBindResult - Already gave API a Bind Result, don't duplicate.
// ---------------------------------------------------------------------------
static BOOL apiHasBindResult;

// ---------------------------------------------------------------------------
//
//  CYFISNP_BindStart() - From data Mode to Bind Mode
//
// ---------------------------------------------------------------------------
void CYFISNP_BindStart(void)
{
#ifdef CY8C_KIT
    //CYFISNP_OutStr("\n\rStart Bind Mode");
    LCD_ClearDisplay();
	  LCD_PrintString("BIND_MODE");
#endif
    bindChIdx = 0;
    setBindChannel();
    CYFISNP_eProtState = CYFISNP_BIND_MODE;
    bindChCount = sizeof(CYFISNP_BIND_CH_SEQ);
    CYFISNP_TimeSet(&bindChWaitTimer, BIND_CH_WAIT_TIME);
    CYFISNP_IsrRxOverflow = FALSE;
    CYFISNP_BindProgress = PASSED_BIND_NONE; // For ISR
    apiHasBindResult = FALSE;
}

// ---------------------------------------------------------------------------
//
//  bindStop() - From Bind Mode to Ping Mode
//
// ---------------------------------------------------------------------------
static void bindStop(void)
{
#ifdef CY8C_KIT
    //CYFISNP_OutStr("\n\rExit Bind Mode");
    LCD_ClearDisplay();
	LCD_PrintString("BIND_STOP");
#endif
    gotoPingMode();
}

// ---------------------------------------------------------------------------
//
//  bindRun() - Polled via Protocol CYFISNP_Run() polling function
//
// ---------------------------------------------------------------------------
static void bindRun(void)
{
    if (CYFISNP_TimeExpired(&bindChWaitTimer))
    {
        if (--bindChCount == 0) {
            bindStop();
        }
        else
        {
            CYFISNP_TimeSet(&bindChWaitTimer, BIND_CH_WAIT_TIME);
            if (++bindChIdx >= sizeof(CYFISNP_BIND_CH_SEQ)) {
                bindChIdx = 0;
            }
            setBindChannel();
        }
    }
}

static void setBindChannel(void)
{
    CYFISNP_phyIdle();
    CYFISNP_BindChannel = CYFISNP_BIND_CH_SEQ[bindChIdx];
    CYFISNP_SetChannel   (CYFISNP_BindChannel);
    CYFISNP_SetSopPnCode (CYFISNP_BIND_MODE_SOP);
    CYFISNP_SetCrcSeed   (CYFISNP_BIND_MODE_CRC_SEED);
    CYFISNP_phyRxGo();
}



#if BCD_IS_SUPPORTED
// ---------------------------------------------------------------------------
//
// CYFISNP_TxdataPend()
//
// For Wall powered nodes:         TxLen  TxCnt
//                        EMPTY |    0      0  |
//                        ACKED |    0      NZ |
//                      TIMEOUT |    63     -  | #define BCD_TIMEOUT_COUNT 63
//                      PENDING |   !63     -  |
// ---------------------------------------------------------------------------
    enum CYFISNP_TX_data_PEND      // Ret EMPTY,ACKED,TIMEOUT,PENDING
CYFISNP_TxdataPend (byte devId)
{
    CYFISNP_BCD_PKT *pBcdBuf = &CYFISNP_BcdBufs[devId-1];
    
    if ((pBcdBuf->wprCntLen & BCDBUFS_LEN_MASK) == 0) 
    {        
        if ((pBcdBuf->wprCntDly & 0xC0) == 0
          &&(pBcdBuf->wprCntLen & 0xC0) == 0)   // TxLen == 0 && TxCnt == 0
        {
            return(CYFISNP_TX_data_EMPTY);
        }
        else {                                  // TxLen == 0 && TxCnt != 0
            pBcdBuf->wprCntDly = 0;
            pBcdBuf->wprCntLen = 0;
            return(CYFISNP_TX_data_ACKED);
        }
    }       // end TxLen == 0
    if ((pBcdBuf->wprCntLen & BCDBUFS_LEN_MASK) == BCD_TIMEOUT_COUNT) 
    {
        pBcdBuf->wprCntLen = 0;                 // TxLen == TIMEOUT
        pBcdBuf->wprCntDly = 0;
        return(CYFISNP_TX_data_TIMEOUT);
    }    
    // TxLen != 0 && TxCnt != 0
    return(CYFISNP_TX_data_PENDING);
}

// ---------------------------------------------------------------------------
//
// CYFISNP_TxdataPut() - Put Tx data in buffer for later transmission.
//
// Returns FALSE if malformed packet.
//   Overwrites pending packet if present
// ---------------------------------------------------------------------------
BOOL CYFISNP_TxdataPut (CYFISNP_API_PKT *pApiPkt) {
    byte devId;
    char len;
    unsigned char *pMySrc;
    unsigned char *pDst;
    CYFISNP_BCD_PKT *pBcdBuf;

    devId = pApiPkt->devId;
    pBcdBuf = &CYFISNP_BcdBufs[devId-1];

    // -----------------------------------------------------------------------
    // Sanity check devId and packet length
    // -----------------------------------------------------------------------
    if (          devId > CYFISNP_MAX_NODES
     || pApiPkt->length > CYFISNP_BCD_PAYLOAD_MAX) {
        return FALSE;
    }

    // -----------------------------------------------------------------------
    // Convert API packet type to Protocol Packet Type Byte
    // -----------------------------------------------------------------------
#if 0		
    switch (pApiPkt->type) {
        case CYFISNP_API_TYPE_CONF:
        case CYFISNP_API_TYPE_CONF_BCDR:
            pBcdBuf->bcd_data[BCDBUFS_PROTOCOL_OFS] = CYFISNP_PKT_data_BCD_CONF;
            break;
        case CYFISNP_API_TYPE_SYNC:
        case CYFISNP_API_TYPE_SYNC_BCDR:
            pBcdBuf->bcd_data[BCDBUFS_PROTOCOL_OFS] = CYFISNP_PKT_data_BCD_SYNC;
            break;
        default:
            return FALSE;       // Unrecognized API packet type
    }
#endif		
    
    // -----------------------------------------------------------------------
    // Set the length and copy the packet data to the BCD array
    // -----------------------------------------------------------------------
		pBcdBuf->bcd_data[BCDBUFS_PROTOCOL_OFS] = CYFISNP_PKT_data_BCD_CONF;  // We always send this type of bcd packet at this firmware, 2017.09.27  
    pBcdBuf->wprCntLen = pApiPkt->length + 1;  // inc Prot Byte
    pMySrc = &pApiPkt->payload[0];
    pDst = &pBcdBuf->bcd_data[BCDBUFS_API_data_OFS];
    for (len=CYFISNP_BCD_PAYLOAD_MAX; len != 0 ; --len) {
        *pDst++ = *pMySrc++;
    }

    // #######################################################################
    //
    // IF PACKET TO A WALL POWERED NODE,
    //  THEN ATTEMPT ONE TRANSMISSION HERE.
    //
    // #######################################################################
//    if ((*(EEPROM_Pointer + GET_DEV_INDEX(devId)) & CYFISNP_PWR_TYPE_MASK)      It is not nesesary in our application
//       == CYFISNP_PWR_WALL)
    {        
        pBcdBuf->wprCntDly = (bcdRetryDelay + 1) & BCDBUFS_DELAY_MASK;
			  pBcdBuf->wprCnt = 0;                                                 
        bcdSendPacket(devId);               
    }
    return TRUE;
}


// ---------------------------------------------------------------------------
//
// bcdSendPacket() - Send a BCD packet to the specified Wall Powered devId
//
// Handle first "quick" attempt and possible subsequent periodic retries.
//
// Waits for AutoAck and maintains retry Count/Delay as necessary.
//
// ---------------------------------------------------------------------------
static void bcdSendPacket(byte devId)
{
    CYFISNP_BCD_PKT *pBcdBuf = &CYFISNP_BcdBufs[devId-1];

    phySynth(TRUE);         // Put Radio in Synth (ie: stop Rx), w/AutoAck

    // -----------------------------------------------------------------------
    // Use Node's CRC Seed (HubSeed ^ devId)
    // -----------------------------------------------------------------------
    CYFISNP_SetCrcSeed(((word)(*(EEPROM_Pointer + 3)) << 8)|(*(EEPROM_Pointer + 4) ^ devId));
	    
	
    //CYFISNP_TxHandlerNone();        // Block ISR Tx protocol handler
    txRetVec = 0;
    CYFISNP_SetPtr       (  &pBcdBuf->bcd_data[0]);
    CYFISNP_StartTransmit(0, pBcdBuf->wprCntLen & BCDBUFS_LEN_MASK);
    while ((CYFISNP_State & CYFISNP_COMPLETE) == 0)
		{
			Cy_DelayUS2(5);
		}	  
			
    // Wait until Tx Complete
    // -----------------------------------------------------------------------
    // If AutoAck failed, update Tx count and Tx delay
    // -----------------------------------------------------------------------
    if ((CYFISNP_State & CYFISNP_ERROR) != 0) {
        byte count;         // # Tx retries (increment to _BCD_WALL_RETRY_MAX
        byte delay;         // delay beween retries (max 63)

        count  = (pBcdBuf->wprCntLen & (~BCDBUFS_LEN_MASK)) >> 4; // [3:2] = Tx retry count
        count |= (pBcdBuf->wprCntDly & 0xC0)                >> 6; // [3:0] = Tx retry count

        // -----------------------------------------------------------------------
        // If retry count was at MAX, indicate TIMEOUT
        // -----------------------------------------------------------------------
        if (count++ == BCD_RETRY_MAX_15) {
            pBcdBuf->wprCntLen = BCD_TIMEOUT_COUNT;
        }

        // -----------------------------------------------------------------------
        // else calculate new retry delay (quasi exponential backoff)
        // -----------------------------------------------------------------------
        else {                                      // Allow retransmit
            if (count <= 9) delay = 1<<(count>>1);  // 0=1,2=2,4=4,6=8,8=16
            else            delay = 31;             // 31 is longest delay
            delay += bcdRetryDelay;                 // Free-run value to retry
            delay &= BCDBUFS_DELAY_MASK;

            pBcdBuf->wprCntLen = ((count << 4) & 0xC0)
                               | (pBcdBuf->wprCntLen & BCDBUFS_LEN_MASK);
            pBcdBuf->wprCntDly = (count << 6) | delay;
        }
    }
    // -----------------------------------------------------------------------
    // Else got AutoAck mark message as ACKED
    // -----------------------------------------------------------------------
    else {
        pBcdBuf->wprCntLen &= ~BCDBUFS_LEN_MASK;    // Len=0, leave count alone
    }

    setNetParams();
    CYFISNP_phyRxGo();          // Also, enable AutoAck
}

// ---------------------------------------------------------------------------
//
// bcdSendPacket() - Send a BCD packet to the specified Wall Powered devId
//
// Handle first "quick" attempt and possible subsequent periodic retries.
//
// Waits for AutoAck and maintains retry Count/Delay as necessary.
//
// ---------------------------------------------------------------------------
static void bcdSendPacket1(byte devId)
{
	  uint8  status;
	
    CYFISNP_BCD_PKT *pBcdBuf = &CYFISNP_BcdBufs[devId-1];

    phySynth(TRUE);         // Put Radio in Synth (ie: stop Rx), w/AutoAck

    // -----------------------------------------------------------------------
    // Use Node's CRC Seed (HubSeed ^ devId)
    // -----------------------------------------------------------------------
    CYFISNP_SetCrcSeed(((word)(*(EEPROM_Pointer + 3)) << 8)|(*(EEPROM_Pointer + 4) ^ devId));
	    
	
    //CYFISNP_TxHandlerNone();        // Block ISR Tx protocol handler
    txRetVec = 0;
    CYFISNP_SetPtr       (  &pBcdBuf->bcd_data[0]);
	  HAL_NVIC_DisableIRQ(EXTI0_IRQn);
    CYFISNP_StartTransmit(0, pBcdBuf->wprCntLen & BCDBUFS_LEN_MASK);
	
	  do {
	     status = CYFISNP_GetTransmitState();
			 if((status & (TXC_IRQ | TXE_IRQ)) != 0)
				 break;
			 else
				 Cy_DelayUS2(5);			
	  } while (1);

	  //CYFISNP_EndTransmit();
		__HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_0);
		HAL_NVIC_EnableIRQ(EXTI0_IRQn);
		
    // -----------------------------------------------------------------------
    // If AutoAck failed, update Tx count and Tx delay
    // -----------------------------------------------------------------------
    if ((CYFISNP_State & CYFISNP_ERROR) != 0) {
        byte count;         // # Tx retries (increment to _BCD_WALL_RETRY_MAX
        byte delay;         // delay beween retries (max 63)

        //count  = (pBcdBuf->wprCntLen & (~BCDBUFS_LEN_MASK)) >> 4; // [3:2] = Tx retry count
        //count |= (pBcdBuf->wprCntDly & 0xC0)                >> 6; // [3:0] = Tx retry count
			    count = pBcdBuf->wprCnt;

        // -----------------------------------------------------------------------
        // If retry count was at MAX, indicate TIMEOUT
        // -----------------------------------------------------------------------
        if (count++ == BCD_RETRY_MAX_15) {
            pBcdBuf->wprCntLen = BCD_TIMEOUT_COUNT;
					  pBcdBuf->wprCnt = 0;
        }

        // -----------------------------------------------------------------------
        // else calculate new retry delay (quasi exponential backoff)
        // -----------------------------------------------------------------------
        else {                                      // Allow retransmit
            if (count <= 9) delay = 1<<(count>>1);  // 0=1,2=2,4=4,6=8,8=16
            else            delay = 31;             // 31 is longest delay
            delay += bcdRetryDelay;                 // Free-run value to retry
            delay &= BCDBUFS_DELAY_MASK;

            //pBcdBuf->wprCntLen = ((count << 4) & 0xC0) 
            //                   | (pBcdBuf->wprCntLen & BCDBUFS_LEN_MASK);					
            //pBcdBuf->wprCntDly = ((count << 6) & 0xC0) | delay;
            pBcdBuf->wprCntLen = pBcdBuf->wprCntLen & BCDBUFS_LEN_MASK;					
					  pBcdBuf->wprCntDly = delay;
					  pBcdBuf->wprCnt    = count;
        }
    }
    // -----------------------------------------------------------------------
    // Else got AutoAck mark message as ACKED
    // -----------------------------------------------------------------------
    else {
        //pBcdBuf->wprCntLen &= ~BCDBUFS_LEN_MASK;    // Len=0, leave count alone
			  pBcdBuf->wprCntLen = 0;
			//CYFISNP_EndTransmit();
    }

    setNetParams();
    CYFISNP_phyRxGo();          // Also, enable AutoAck
}
#endif // BCD_IS_SUPPORTED



// ---------------------------------------------------------------------------
//
// CYFISNP_RxdataPend() - ISR asynchronously posts packet to RxApiPkt buffer, so
//                ONLY a call from the Application can examine this buffer.
//
// ---------------------------------------------------------------------------
    BOOL                            // Ret TRUE if Rx data pending
CYFISNP_RxdataPend(void)
{
    if (CYFISNP_RxApiPkt.length == 0) {
        return(FALSE);
    }
    if (CYFISNP_RxApiPkt.length <= 2) {    // If Rx pkt too small
        CYFISNP_RxdataRelease();           //  then discard it
        return(FALSE);
    }

    // -----------------------------------------------------------------------
    // If in BIND_MODE, intercept Rx packets before handoff to API
    // -----------------------------------------------------------------------
    if (CYFISNP_eProtState == CYFISNP_BIND_MODE) {
        // -------------------------------------------------------------------
        // If Bind Request, then try to assign an ID
        // -------------------------------------------------------------------
        if ((CYFISNP_RxApiPkt.type & PKT_BINDREQ_MASK) == PKT_BINDREQ_TYPE)
        {
            #if AUTO_ASSIGN_DEVICE_ID   // Give next free Device ID
                if (CYFISNP_RxApiPkt.devId == 0) 
                {
                    int tmpId = 1;
                    // CYFISNP_EEP_DEV_REC const * pFlash;
                    // pFlash = &CYFISNP_EEP_DEV_REC_ADR[1];
                    for(tmpId=1; tmpId <= CYFISNP_MAX_NODES; ++tmpId) 
                    {
                        if (*(EEPROM_Pointer + GET_DEV_INDEX(tmpId) + 1) == 0) 
                        {
                            CYFISNP_RxApiPkt.devId = (byte)tmpId;                                                      
                            bindAllow(&CYFISNP_RxApiPkt);
                            break;          // Assign first available devId
                        }
                    }
                }
                else 
                {
                   bindStop();             // Exit w/bind failure
                }

            #else // Give requested devId, replacing existing if needed
                if (CYFISNP_RxApiPkt.devId <= CYFISNP_MAX_NODES
                  &&  CYFISNP_RxApiPkt.devId != 0) 
                  {
                     bindAllow(&CYFISNP_RxApiPkt);
                  }
                  else 
                  {
                    if (CYFISNP_RxApiPkt.devId == 0)
                    {
                    }
                    //CYFISNP_OutStr("\n\r<BindReq reject: on-the-fly Node>");
                    else
                    {
                    }
                    //CYFISNP_OutStr("\n\r<BindReq reject: bind table FULL>");
                    bindStop();             // Exit w/bind failure
                  }
            #endif
            purgeRxdata();      // Discard Rx packet and anything queued by ISR            
            return(FALSE);
        }
        // -------------------------------------------------------------------
        else if (CYFISNP_RxApiPkt.type == PKT_BINDRSP_TYPE) {
            if (apiHasBindResult == FALSE) {
                apiHasBindResult = TRUE;
                CYFISNP_RxApiPkt.type = CYFISNP_API_TYPE_BIND_RSP_ACK;
                CYFISNP_RxApiPkt.length = 6;  // Just use MID from pkt
            }
            else {
                purgeRxdata();      // Discard Rx packet and anything queued by ISR
                return(FALSE);
            }
        }
        // -------------------------------------------------------------------
        else if (CYFISNP_RxApiPkt.type == PKT_BINDRSPDEL_TYPE) {
            if (apiHasBindResult == FALSE) {
                CYFISNP_RxApiPkt.type = CYFISNP_API_TYPE_BIND_RSP_ACK;
                CYFISNP_RxApiPkt.length = 6;  // Just use MID from pkt
            }
            else {
                purgeRxdata();      // Discard Rx packet and anything queued by ISR
                return(FALSE);
            }
            bindStop();
        }
        // -------------------------------------------------------------------
        else {
            purgeRxdata();      // Discard Rx packet and anything queued by ISR
            return(FALSE);
        }
              
    }  // END if (in BIND_MODE && have packet)

    // -----------------------------------------------------------------------
    // ELSE Normal data_MODE (not Binding)
    // -----------------------------------------------------------------------
    else {                                              // else if have Rx pkt
        switch (CYFISNP_RxApiPkt.type & PKT_data_TYPE_MASK) {
        case PKT_data_CONF_TYPE:            // Confirmed, no BCDR
            CYFISNP_RxApiPkt.type = CYFISNP_API_TYPE_CONF;
            break;
        case PKT_data_CONFBCDR_TYPE:        // Confirmed, w/BCDR
            CYFISNP_RxApiPkt.type = CYFISNP_API_TYPE_CONF_BCDR;
            break;
        case PKT_data_SYNC_TYPE:            // Sync, no BCDR
            CYFISNP_RxApiPkt.type = CYFISNP_API_TYPE_SYNC;
            break;
        case PKT_data_SYNCBCDR_TYPE:        // Sync, w/BCDR
            CYFISNP_RxApiPkt.type = CYFISNP_API_TYPE_SYNC_BCDR;
            break;
        default:
            CYFISNP_RxdataRelease();   // Discard Rx packet
            break;
        }
    }
    return(TRUE);
}



// ---------------------------------------------------------------------------
//
// CYFISNP_RxdataGet() - Return apiPkt->length != 0 if data pending
//                 If so, process data and call CYFISNP_RxdataRelease()
//
// ---------------------------------------------------------------------------
    CYFISNP_API_PKT *      // Ret ptr to an API packet
CYFISNP_RxdataGet_old(void)
{
    if (CYFISNP_RxApiPkt.length >= 2) {
        CYFISNP_RxApiPkt.length -= 2;  // API payload length
    }
    return (&CYFISNP_RxApiPkt);
}


// ---------------------------------------------------------------------------
//
// CYFISNP_RxReleaseRxdata() - Release Rx Packet buffer for reuse (MUST DO THIS)
//
// The radio ISR has a internal buffer: rxIsrBufPkt.  When the ISR receives a
//  packet and _RxApiPkt is free, then it copies rxIsrBufPkt to RxIsrBuf and
//  issue RxGo to the radio.  However if _RxApiPkt is occupied, the ISR holds
//  the Rx packet in its internal rxIsrBufPkt buffer, sets IsrRxOverflow
//  and DOES NOT issue RxGo (ie: it stops AutoAcking arriving packets).
//  So RxdataRelease() FIRST releases RxApiPkt, THEN checks for overflow and
//   recovers if overflow occurred.
//
// ---------------------------------------------------------------------------
void CYFISNP_RxdataRelease_old(void)
{
//    extern BYTE CYFISNP_RxIsrBuf;

    // -----------------------------------------------------------------------
    // FIRST release the RxApiPkt buffer (Rx packet ISR may hit at any time)
    // -----------------------------------------------------------------------
    CYFISNP_RxApiPkt.type = CYFISNP_API_TYPE_NULL;
    CYFISNP_RxApiPkt.length = 0;

    // -----------------------------------------------------------------------
    // SECOND check for ISR overflow (ie: it has stopped receiving packets)
    // -----------------------------------------------------------------------
    if (CYFISNP_IsrRxOverflow) {
        // -------------------------------------------------------------------
        // Manually copy the ISR buffer to the RxIsrBuf
        // -------------------------------------------------------------------
//        CYFISNP_RxApiPkt.length = rxIsrBufLen;
//        CYFISNP_RxApiPkt.rssi = rxIsrBufRssi;
        uint8 *pSrc =     (uint8 *)    &rxIsrBufPkt;
        uint8 *pDst = (byte *)&CYFISNP_RxApiPkt;
        char len = *pSrc;
        for (len += 2; len != 0; --len) {
            *pDst++ = *pSrc++;
        }

        // -------------------------------------------------------------------
        // Release the overflow and enable the radio receiver
        // -------------------------------------------------------------------
        CYFISNP_IsrRxOverflow = 0;

        CYFISNP_phyRxGo();        
    }
}

// ---------------------------------------------------------------------------
//
// purgeRxdata() - Discard Rx Packet and anything queued by ISR
//
// ---------------------------------------------------------------------------
static void purgeRxdata(void)
{
    CYFISNP_RxdataRelease();
    CYFISNP_RxdataRelease();
}



// ---------------------------------------------------------------------------
//
// CYFISNP_GetNodeID -
//
// ---------------------------------------------------------------------------
    byte                        // 5-bit Device ID (NULL not found)
CYFISNP_GetNodeID(byte *mid)      // Ptr to MID to locate
{
     unsigned char jvar;
    char testDev;
    for (testDev=1; testDev <= CYFISNP_MAX_NODES; ++testDev) {
        for (jvar = 0; jvar < CYFISNP_SIZEOF_MID; ++jvar) {
            if (mid[jvar] != *(EEPROM_Pointer + GET_DEV_INDEX(testDev) + 2 + jvar)) {
                break;
            }
        }
        if (jvar == CYFISNP_SIZEOF_MID) {
            return(*(EEPROM_Pointer + GET_DEV_INDEX(testDev) + 1));
        }
    }
    return 0;
}


// ---------------------------------------------------------------------------
//
// CYFISNP_GetNodeMid() -
//
// ---------------------------------------------------------------------------
    BOOL                        // TRUE = loaded a MID
CYFISNP_GetNodeMid(
        byte *mid,              // Destination for located MID
        byte NodeID)             // Device ID to locate
{
      unsigned char ivar;
    if (*(EEPROM_Pointer + GET_DEV_INDEX(NodeID)+ 1) != 0) {
        for (ivar=0; ivar != CYFISNP_SIZEOF_MID; ++ivar) {
            mid[ivar] = *(EEPROM_Pointer + GET_DEV_INDEX(NodeID) + 2 + ivar);
        }
        return (TRUE);
    }
    return(FALSE);
}

// ---------------------------------------------------------------------------
//
// CYFISNP_GetHubMid() - Retrives HUB MID
//
// ---------------------------------------------------------------------------
BOOL CYFISNP_GetHubMid(byte *mid)
{
	CYFISNP_SetPtr(mid);
	CYFISNP_SetLength(6);
	CYFISNP_GetFuses();
	return TRUE;
}

// ---------------------------------------------------------------------------
//
// CYFISNP_SetHubMid(BYTE *mid) - Sets custom NetParams for given HUB MID
// ---------------------------------------------------------------------------
#define HOP_MASK        7   // Hop range {0...7]

BOOL CYFISNP_GetHubNet(CYFISNP_EEP_NET_REC *net, byte* mid)
{
//	CYFISNP_EEP_NET_REC net;

	// -------------------------------------------------------------------
	// Calculate SOP Index modulo CYFISNP_CODE_MAX (or 10)
	// -------------------------------------------------------------------
	net->sopIdx    =  (mid[0] << 2) + mid[1] + mid[2];
	while (net->sopIdx >= CYFISNP_NUM_PN_CODES ) {
		net->sopIdx -= CYFISNP_NUM_PN_CODES;
	}

	// -------------------------------------------------------------------
	// Calculate Channel Hop
	// -------------------------------------------------------------------
	net->chHop = (mid[0] - mid[1]) & HOP_MASK;

	// -------------------------------------------------------------------
	// Calculate Channel Base modulo SNP_MAX_CH_SUBSETS (or 6)
	// -------------------------------------------------------------------
	net->chBase     = (mid[1] >> 2) - (mid[0] << 5) + mid[2];
	while (net->chBase >= SNP_MAX_CH_SUBSETS )  {
		net->chBase -= SNP_MAX_CH_SUBSETS;
	}  // chBase {0,1,2,3,4,5}
	if ((signed char)net->chBase < CYFISNP_CHAN_MIN) {
		net->chBase += net->chHop * SNP_MAX_CH_SUBSETS;
	}  // now chBase is at least _CHAN_MIN

	// -------------------------------------------------------------------
	// Calculate Hub CRC Seed
	// -------------------------------------------------------------------
	{
		byte *pch = &mid[1];
		do {
			net->hubSeedMsb = mid[0] ^ *pch++;
		} while (net->hubSeedMsb == 0);
	}
	net->hubSeedLsb = (mid[0] >> 2) ^ (mid[4] << 2);

	// -------------------------------------------------------------------
	// Store network information in Flash for all subsequent use
	// -------------------------------------------------------------------
//	CYFISNP_netRecWrite(&net);                      // ?????????????????????????????????????????
	return TRUE;
}

#if 0
BOOL CYFISNP_SetHubMid(byte *mid)
{
	CYFISNP_EEP_NET_REC net;

	// -------------------------------------------------------------------
	// Calculate SOP Index modulo CYFISNP_CODE_MAX (or 10)
	// -------------------------------------------------------------------
	net.sopIdx    =  (mid[0] << 2) + mid[1] + mid[2];
	while (net.sopIdx >= CYFISNP_NUM_PN_CODES ) {
		net.sopIdx -= CYFISNP_NUM_PN_CODES;
	}

	// -------------------------------------------------------------------
	// Calculate Channel Hop
	// -------------------------------------------------------------------
	net.chHop = (mid[0] - mid[1]) & HOP_MASK;

	// -------------------------------------------------------------------
	// Calculate Channel Base modulo SNP_MAX_CH_SUBSETS (or 6)
	// -------------------------------------------------------------------
	net.chBase     = (mid[1] >> 2) - (mid[0] << 5) + mid[2];
	while (net.chBase >= SNP_MAX_CH_SUBSETS )  {
		net.chBase -= SNP_MAX_CH_SUBSETS;
	}  // chBase {0,1,2,3,4,5}
	if ((signed char)net.chBase < CYFISNP_CHAN_MIN) {
		net.chBase += net.chHop * SNP_MAX_CH_SUBSETS;
	}  // now chBase is at least _CHAN_MIN

	// -------------------------------------------------------------------
	// Calculate Hub CRC Seed
	// -------------------------------------------------------------------
	{
		byte *pch = &mid[1];
		do {
			net.hubSeedMsb = mid[0] ^ *pch++;
		} while (net.hubSeedMsb == 0);
	}
	net.hubSeedLsb = (mid[0] >> 2) ^ (mid[4] << 2);

	// -------------------------------------------------------------------
	// Store network information in Flash for all subsequent use
	// -------------------------------------------------------------------
	CYFISNP_netRecWrite(&net);                      // ?????????????????????????????????????????
	return TRUE;
}
#else
BOOL CYFISNP_SetHubMid(CYFISNP_EEP_NET_REC *net)
{
	//CYFISNP_EEP_NET_REC net;
	// -------------------------------------------------------------------
	// Store network information in Flash for all subsequent use
	// -------------------------------------------------------------------
	CYFISNP_netRecWrite(net);                      // ?????????????????????????????????????????	
	return TRUE;
}
#endif


// ---------------------------------------------------------------------------
//
// void makeNetParams(void) - Hub synthesizes Network Parameters at first PowerUp
//                   and copies to FLASH for use FOREVER.
//
//  The goal here is to produce a unique set of Hub Network Parameters:
//      CRC Seed    (65,535 possible)
//      SOP Index   (    10 possible)
//      Channel Set (     6 possible)
//
//  This implementation uses only the Radio's Manufacturing ID (MID),
//   however it could methods such as:
//          A) shifting a P/N generator.
//          B) timing an external event
//          C) etc.
//
//         mid[5]   mid[4]   mid[3]   mid[2]   mid[1]   mid[0]
//        76543210 76543210 76543210 76543210 76543210 76543210
//        aaaBByyy yWWWWWWL LLLLLLLL LLLLLwww wwXXXXXX XYYYYYYY
//                          aaa = VERSION
//                           BB = VENDOR
//                        YYYYY = YEAR
//                       WWWWWW = WORKWEEK
//               LLLLLLLLLLLLLL = LOT
//                        wwwww = WAFER
//                      XXXXXXX = X_COORD
//                      YYYYYYY = Y_COORD
// ---------------------------------------------------------------------------
/*
typedef struct {                // -------------------------------------------
    byte sopIdx;                //
    byte chBase;                //
    byte chHop;                 //
    byte hubSeedMsb;            // Hub CRC Seed[15:8]
    byte hubSeedLsb;            // Hub CRC Seed[ 7:0]
                                // -------------------------------------------
    byte devId;                 // only used by Nodes
    byte nodeSeedMsb;           // used only by Nodes
    byte nodeSeedLsb;           // used only by Nodes
} CYFISNP_EEP_NET_REC;
*/
static void makeNetParams(void)
{
    // -----------------------------------------------------------------------
    // Only do this if very FIRST powerup after fresh code loaded
    // -----------------------------------------------------------------------
  //  if (*(EEPROM_Pointer + 3) == 0
  //   && *(EEPROM_Pointer + 4) == 0) 
   {
      
		  unsigned char mid[6];
		  CYFISNP_EEP_NET_REC net;
		  
      CYFISNP_GetHubMid(&mid[0]); 
      CYFISNP_GetHubNet(&net, mid); 
      
      if((CYFISNP_EEP_NET_REC_ADR->sopIdx     != net.sopIdx)    
       ||(CYFISNP_EEP_NET_REC_ADR->chBase     != net.chBase)    
       ||(CYFISNP_EEP_NET_REC_ADR->chHop      != net.chHop)     
       ||(CYFISNP_EEP_NET_REC_ADR->hubSeedMsb != net.hubSeedMsb)
       ||(CYFISNP_EEP_NET_REC_ADR->hubSeedLsb != net.hubSeedLsb)
        )
      {        
		     CYFISNP_SetHubMid(&net);				
      }
   }
}

// Florian please Implement CYFISNP_netRecWrite for your MCU
// Contact ankc@cypress.com if you need support
// ---------------------------------------------------------------------------
//
// CYFISNP_netRecWrite() - Write Network Record to FLASH
//
// ---------------------------------------------------------------------------
//
//#define CMD_BPROG   0x20
//#define CMD_ERASE   0x40
#define ERR_SUCCESS 0
#define ERR_FLASH   1
#define ERR_VERIFY  2
#define NET_PARAMETER (byte*)NET_PARAMETER_ADDR//EEPROM_Pointer//0x1A00   // Binding data is stored at here, moved to "CYFISNP_Protocol.h"

//typedef byte(*pFct) (byte,byte*,byte);

//byte CYFISNP_FIMG_ADDR[80];                                     
//#define CYFISNP_FIMG_SIZE   80    /* 0x3f=63 */

static byte writeBuf[(CYFISNP_MAX_NODES+1)*8];
#define PROG_LEN    ((CYFISNP_MAX_NODES+1)*8)

static byte myNetNvramWrite(byte *src, byte len)
{
    word i;
    byte retVal;
    byte* addrp = NET_PARAMETER;	
 
    // check if already the same
    if( memcmp(addrp, src, len)==0 ) 
		{
        retVal = ERR_SUCCESS;			
        return retVal;
    }

    // memset(writeBuf, 0, 512);
    (void)memcpy(writeBuf, addrp, PROG_LEN);
    (void)memcpy(writeBuf, src, len); 
    
    for(i = len;i < PROG_LEN;i++) 
    {
      writeBuf[i] = 0;
    }

    DisableInterrupts;
    retVal = ERR_SUCCESS;
		
		if(Flash_Program(writeBuf, PROG_LEN))
		{
      retVal = ERR_FLASH;
      EnableInterrupts;
      return retVal;
	  }        

		EnableInterrupts;
		
    addrp   = NET_PARAMETER;
    if( memcmp(addrp, src, len) ) {
      retVal = ERR_VERIFY;
    }
    return retVal;
}

static byte myDevNvramWrite(byte devId, byte *src, byte len)
{
    byte* addrp   = (byte*)(NET_PARAMETER + devId * 8);
    byte* devAddr = (byte*)(NET_PARAMETER);
    byte retVal =0;                                               //Agust 29, Jason chen
    
    if((devId >CYFISNP_MAX_NODES )||(devId <=0))
       return ERR_FLASH;
    
    // check if already the same
    if( memcmp(addrp, src, len)==0 ) {
        retVal = ERR_SUCCESS;
        return retVal;
    }
				    
    addrp = (byte*)NET_PARAMETER;
    (void)memcpy(writeBuf, devAddr, PROG_LEN);
    (void)memcpy(&writeBuf[devId*8], src, len);                            

    DisableInterrupts;
    retVal = ERR_SUCCESS;

		if(Flash_Program(writeBuf, PROG_LEN))
		{
      retVal = ERR_FLASH;
      EnableInterrupts;
      return retVal;
	  }        

    EnableInterrupts;
   
    addrp = (byte*)(NET_PARAMETER + devId * 8);
    if( memcmp(addrp, src, len) )
        retVal = ERR_VERIFY;
		return retVal;                // Agust 29, Jason Chen
}


byte NetEraseZero(byte eraseIdx)
{
    word i;
    byte* addrp = NET_PARAMETER;
    byte retVal;
    
    // check if eraseIdx value valid
    if( eraseIdx < 1) {
        retVal = ERR_SUCCESS;
        return retVal;
    }
    
    DisableInterrupts;
    retVal = ERR_SUCCESS;
		
    (void)memcpy(writeBuf, addrp, PROG_LEN);    
    
    for(i = eraseIdx * 8;i<PROG_LEN;i++) 
    {      
      writeBuf[i] = 0;
    }
       
		if(Flash_Program(writeBuf, PROG_LEN))
		{
      retVal = ERR_FLASH;
      EnableInterrupts;
      return retVal;
	  }        
    
    EnableInterrupts;    
    return retVal;            
}

void InitddNetTable(void) 
{
  
  if((!CYFISNP_EEP_NET_REC_ADR->sopIdx)&&(!CYFISNP_EEP_NET_REC_ADR->chBase)&&(!CYFISNP_EEP_NET_REC_ADR->chHop)&&
     (!CYFISNP_EEP_NET_REC_ADR->hubSeedMsb)&&(!CYFISNP_EEP_NET_REC_ADR->hubSeedLsb))
     
  {
          
  
  }
}


void CYFISNP_netRecWrite(CYFISNP_EEP_NET_REC *pRam)                // Ptr to network record to write
{
    byte writeStatus = 0xFF;
    
    writeStatus = myNetNvramWrite((byte*)pRam,8);
        
    while(writeStatus != ERR_SUCCESS)
    {
   #ifdef  CY8C_KIT
        // put error string when using debug
        LCD_ClearDisplay();
	      LCD_PrintString("Check EEPROM");
        while(1); 
   #endif
    }
}


// ---------------------------------------------------------------------------
//
// CYFISNP_GetDieTemp() - Get DIE temperature for Flash Write ops
//
// ---------------------------------------------------------------------------
byte CYFISNP_GetDieTemp(void) {

    return 20;

}

void ClearNetTable(void)               // not used
{
	#if 0
    word i;
    pFct pflash;
    byte* addrp = (byte*)NET_PARAMETER;
//    byte retVal =0;   //Agust 25, Jason chen
    //byte writeBuf[512];    

  
  //if((!CYFISNP_EEP_NET_REC_ADR->sopIdx)&&(!CYFISNP_EEP_NET_REC_ADR->chBase)&&(!CYFISNP_EEP_NET_REC_ADR->chHop)&&
  //   (!CYFISNP_EEP_NET_REC_ADR->hubSeedMsb)&&(!CYFISNP_EEP_NET_REC_ADR->hubSeedLsb))
  
    //byte writeBuf[512];    
    
    
    DisableInterrupts; 
 //   retVal = ERR_SUCCESS;

//    #pragma MESSAGE DISABLE C1805

    // copy the function to RAM
//    pflash = (pFct)CYFISNP_FIMG_ADDR;
//    (void)memcpy((void*)pflash, (void*)flashCYFISNP, CYFISNP_FIMG_SIZE);
		
 #if 1   
    memset(writeBuf, 0, 512);
          
    // Step 1: Erase (512 bytes)
    if( (*pflash)(CMD_ERASE, addrp, 0)!=ERR_SUCCESS ) {
//        retVal = ERR_FLASH;
        goto done;
    }


     // Step 2: Write the data
    for(i=0; i<512; i++) 
    {    
        __RESET_WATCHDOG();   
        if( (*pflash)(CMD_BPROG, addrp, writeBuf[i])!=ERR_SUCCESS ) 
        {
//            retVal = ERR_FLASH;
            goto done;
        }
    }
    
 
    addrp = (byte*)(NET_PARAMETER+512);
      
   
    // Step 1: Erase (512 bytes)
    if( (*pflash)(CMD_ERASE, addrp, 0)!=ERR_SUCCESS ) {
//        retVal = ERR_FLASH;
        goto done;
    }

    // Step 2: Write the data
    for(i=0; i<512; i++) 
    {      
        __RESET_WATCHDOG(); 
        if( (*pflash)(CMD_BPROG, addrp, writeBuf[i])!=ERR_SUCCESS ) 
        {
//            retVal = ERR_FLASH;
            goto done;
        }
    }

    addrp = (byte*)NET_PARAMETER;
    // Step 3: Verify
    //if( memcmp(addrp, src, len) ) {
    //    retVal = ERR_VERIFY;
    //    goto done;
    //}
  #endif

done:

    EnableInterrupts; 
    return;
		#endif
}


// ---------------------------------------------------------------------------
//
// CYFISNP_devRecWrite() - Write Device Record to FLASH (as Nodes Bind and Unbind)
//
// ---------------------------------------------------------------------------
void CYFISNP_devRecWrite(byte devId,CYFISNP_EEP_DEV_REC *pRam)                   // Device ID entry to write    // Ptr to record structure to write
{
    byte writeStatus = 0xFF;   
    
    writeStatus = myDevNvramWrite(devId,(byte*)pRam,8);
    while(writeStatus != ERR_SUCCESS)
    {
      #ifdef CY8C_KIT
        // put error string when using debug
        LCD_ClearDisplay();
	      LCD_PrintString("Check EEPROM");
        while(1); 
      #endif
    }

}


static void     writeHwPa           (byte paLevel);
#if(CYFISNP_EXTERNAL_PA)
static void     externalPaEnable    (void);
static void     externalPaDisable   (void);
#endif
// ---------------------------------------------------------------------------
// setMaxPaLevel() - Set Maximum Tx power
// ---------------------------------------------------------------------------
static void setMaxPaLevel(void)
{
    writeHwPa(PA_LEVEL_USED);
}

// ---------------------------------------------------------------------------
// writeHwPa() - Write PA Level to hardware
// ---------------------------------------------------------------------------
static void writeHwPa(
    byte paLevel
    ) {

    // -----------------------------------------------------------------------
    // Disable interrupt when changing _radioTxConfig because SNP ISR
    //  also reads and writes to this register
    // -----------------------------------------------------------------------
    //CyGlobalIntDisable;        // Keep ISR from changing _radioTxConfig
    DisableInterrupts;

    CYFISNP_radioTxConfig &= ~PA_VAL_MSK;
    CYFISNP_radioTxConfig |= paLevel & PA_VAL_MSK;
    CYFISNP_SetTxConfig(CYFISNP_radioTxConfig);

#if CYFISNP_EXTERNAL_PA
    if (paLevel >= EXT_PA_ON )  externalPaEnable();
    else                        externalPaDisable();
#endif

    //CyGlobalIntEnable;         // ISR can change radioTxConfig
    EnableInterrupts; 
}


// If external PA hardware is installed (either Disabled or Enabled)
#if CYFISNP_EXTERNAL_PA
// ---------------------------------------------------------------------------
//
// externalPaEnable()  - Enable  external PA
// externalPaDisable() - Disable external PA
//
// ---------------------------------------------------------------------------
static void externalPaEnable(void)
{
    char regVal;
    // -----------------------------------------------------------------------
    // PACTL_pin = PACTL Function
    // -----------------------------------------------------------------------
    regVal  =  CYFISNP_Read(IO_CFG_ADR);
    regVal &= ~PACTL_GPIO;
    CYFISNP_Write(IO_CFG_ADR, regVal);

    // -----------------------------------------------------------------------
    //  XOUT_pin = ~PACTL Function (inverse of PACTL)
    // -----------------------------------------------------------------------
    regVal  =  CYFISNP_Read(XTAL_CTRL_ADR);
    regVal &= ~XOUT_FNC_MSK;
    regVal |=  XOUT_FNC_PA_N;
    CYFISNP_Write(XTAL_CTRL_ADR, regVal);
}

static void externalPaDisable(void)
{
    char regVal;

    // -----------------------------------------------------------------------
    // Values for PACTL and XOUT when configured for Vendor_A GPIO
    // PACTL = GPIO = 0 (no power for External PA)
    //  XOUT = GPIO = 1 (connect Radio directly to antenna)
    // -----------------------------------------------------------------------
    regVal  =  CYFISNP_Read(GPIO_CTRL_ADR);
    regVal |=  XOUT_OP;                 //  XOUT = 1
    regVal &= ~PACTL_OP;                // PACTL = 0
    CYFISNP_Write(GPIO_CTRL_ADR, regVal);

    // -----------------------------------------------------------------------
    //  PACTL_pin = GPIO Function  (= LOW)
    // -----------------------------------------------------------------------
    regVal  = CYFISNP_Read(IO_CFG_ADR);
    regVal |= PACTL_GPIO;
    CYFISNP_Write(IO_CFG_ADR, regVal);

    // -----------------------------------------------------------------------
    //  XOUT_pin = GPIO Function (= HIGH)
    // -----------------------------------------------------------------------
    regVal  =  CYFISNP_Read(XTAL_CTRL_ADR);
    regVal &= ~XOUT_FNC_MSK;
    regVal |=  XOUT_FNC_GPIO;
    CYFISNP_Write(XTAL_CTRL_ADR, regVal);
}
#endif //  #if CYFISNP_EXTERNAL_PA

byte numNode_ShelfMode(void)
{
	return 0;//ShelfMode_Struct.Rec.nodes;
}
// ###########################################################################
