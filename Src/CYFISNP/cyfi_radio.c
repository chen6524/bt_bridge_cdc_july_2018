//*****************************************************************************
//*****************************************************************************
//  FILENAME: CYFISNP_Protocol.h
//  Version: 0.99, Updated on 20/07/2013

//
//  DESCRIPTION: <HUB>Star Network Protocol Header
//-----------------------------------------------------------------------------
//  Copyright (c) Cypress Semiconductor 2013. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#include "cyfi_hal.h"
#include "cyfi_radio.h"
//#include "CYFISNP_SPIM.h"
//#include "EEPROM.h"

extern void Cy_DelayUS2(uint32_t microseconds);


#define ValidBitsAreEnabled     (CYFISNP_Read(RX_CFG_ADR) & VLD_EN)
#define IsCYFISNP_Sleep         ((CYFISNP_XactConfig & END_STATE_MSK) == END_STATE_SLEEP)

/*Variables used */
extern uint8 * CYFISNP_Ptr;
uint8  CYFISNP_XactConfig; 		 /* Current value of XACT_CFG_ADDR register */
uint8  CYFISNP_State;      /* The current state of CYFISNP_ */
static uint8  packetLen;
uint8  CYFISNP_RetryCount;
//static uint8  tregVal;            Agust 29, Jason chen
static uint8  fine_tun_d1 =10;

/* PN code tables 
 * These are the 20 Multiplicative codes.
 *
 * Bit ordering for these codes:
 *  The LSB of the first byte is the first bit transceived.
 *  The MSB of the first byte is the eighth bit transceived.
 *  The MSB of the eighth byte is the last bit transceived.
 */
#define PNCODE_LENGTH 8
#define PNCODE_GROUP  10

static const uint8  PnCode[] ={//PNCODE_GROUP][PNCODE_LENGTH
      0x3C, 0x37, 0xCC, 0x91, 0xE2, 0xF8, 0xCC, 0x91, /* PN Code 0 */
      0x9B, 0xC5, 0xA1, 0x0F, 0xAD, 0x39, 0xA2, 0x0F, /* PN Code 1 */
      0xEF, 0x64, 0xB0, 0x2A, 0xD2, 0x8F, 0xB1, 0x2A, /* PN Code 2 */
      0x66, 0xCD, 0x7C, 0x50, 0xDD, 0x26, 0x7C, 0x50, /* PN Code 3 */
      0x5C, 0xE1, 0xF6, 0x44, 0xAD, 0x16, 0xF6, 0x44, /* PN Code 4 */
      0x5A, 0xCC, 0xAE, 0x46, 0xB6, 0x31, 0xAE, 0x46, /* PN Code 5 */
      0xA1, 0x78, 0xDC, 0x3C, 0x9E, 0x82, 0xDC, 0x3C, /* PN Code 6 */
      0xB9, 0x8E, 0x19, 0x74, 0x6F, 0x65, 0x18, 0x74, /* PN Code 7 */
      0xDF, 0xB1, 0xC0, 0x49, 0x62, 0xDF, 0xC1, 0x49, /* PN Code 8 */
      0x97, 0xE5, 0x14, 0x72, 0x7F, 0x1A, 0x14, 0x72, /* PN Code 9 */
};


static void wakeupRadio    (void);
static void forceState      (uint8 newState);


/*******************************************************************************
 * Function Name: uint8 CYFISNP_PhyStart(void)
 *******************************************************************************
 * Summary:
 * Initialize and configures the Radio.
 ******************************************************************************/
uint8 CYFISNP_PhyStart(void)
{
/*
    if( CYFISNP_NODE_Init( (CYFISNP_NODE_DEF_ACK_ENABLE | CYFISNP_NODE_DEF_ACK_TIMEOUT | CYFISNP_NODE_DEF_END_STATE), // para1
                           (CYFISNP_NODE_DEF_DATA_RATE | CYFISNP_NODE_DEF_TX_POWER) ) ) { // para2
        CYFISNP_NODE_SetFrameConfig(CYFISNP_NODE_DEF_SOP_EN | CYFISNP_NODE_DEF_SOP_LEN | CYFISNP_NODE_DEF_LEN_EN | CYFISNP_NODE_DEF_SOP_TSH);    // FRAMING_CFG_ADR
        CYFISNP_NODE_SetThreshold64(CYFISNP_NODE_DEF_64_THOLD);  // DATA64_THOLD_ADR
        CYFISNP_NODE_SetThreshold32(CYFISNP_NODE_DEF_32_THOLD);  // DATA32_THOLD_ADR
        CYFISNP_NODE_SetPreambleCount(CYFISNP_NODE_DEF_PREAMBLE_CNT);
*/        
   
    CYFISNP_Init((ACK_EN | ACK_TO_8X | END_STATE_SLEEP),DATMODE_8DR | PA_4_DBM);       // 32 8DE   (256kbps)
  	CYFISNP_SetFrameConfig(SOP_EN | LEN_EN | 0x04);
    CYFISNP_SetThreshold64(DAT64_THRESH_RST);
    CYFISNP_SetThreshold32(DAT32_THRESH_RST);
    CYFISNP_SetPreambleCount(PREAMBLE_LEN_RST);
    return(1);
}

/*******************************************************************************
 * Function Name: void CYFISNP_Phystop(void)
 *******************************************************************************
 * Summary:
 * Aborts the Radio and puts it into sleep and disbles the SPI.
 ******************************************************************************/
void CYFISNP_PhyStop(void)
{
    (void)CYFISNP_Abort();
    CYFISNP_ForceState(END_STATE_SLEEP);
    //CYFISNP_SPIM_Stop();
}


/*******************************************************************************
 * Function Name: void CYFISNP_Init(uint8  xactConfig, uint8  txConfig)
 *******************************************************************************
 * Summary:
 * Initializes the CYFISNP_ module.
 ******************************************************************************/
void CYFISNP_Init(uint8  xactConfig, uint8  txConfig)
{
	uint8  tmp;

	CYFISNP_XactConfig = XACT_CFG_RST;
	CYFISNP_Reset();

	/*  Force RXF clock on streaming driver */
	CYFISNP_Write(CLK_EN_ADR, RXF);
	/*  Use auto-cal for VCO. 
	 *  Register 0x34 stays default to allow auto-cal to be used.
	 */
	CYFISNP_Write(AUTO_CAL_TIME_ADR, AUTO_CAL_TIME_MAX);

	/*  AutoCal offset to -4. */
	CYFISNP_Write(AUTO_CAL_OFFSET_ADR, AUTO_CAL_OFFSET_MINUS_4);

	/*  For questionable KBM legacy reasons, set IRQ active HIGH as default */
	tmp = CYFISNP_Read(IO_CFG_ADR);
	tmp |= IRQ_POL;
	CYFISNP_Write(IO_CFG_ADR, tmp);
	 
	/*  Set XTAL Startup delay to 150uSec to handle warm restarts of the XTAL. */
	CYFISNP_Write(XTAL_CFG_ADR, 0x08); 

	/*  Enable HiLo for quick-turn-around. Use low side injection for receive - 
	 *  this should result in non-inverted data, so no need to hit the invert 
	 *  bit. Turn off AGC and force the LNA on.
	 */
	CYFISNP_Write(RX_CFG_ADR,   ((RX_CFG_RST | FASTTURN_EN | LNA_EN ) 
						   & ~( HI | RXOW_EN | AUTO_AGC_EN )));

	/*  Set the TX Offset to +1MHz.
	 *  THIS MEANS THE ACTUAL TRANSMIT CARRIER WILL BE 1MHz ABOVE THE PLL
	 *  FREQUENCY PROGRAMMED IN THE CHANNEL OR A & N REGISTERS.
	 *  CYFISNP_SetFrequency COMPENSATES FOR THIS OFFSET.
	 */
	CYFISNP_Write(TX_OFFSET_LSB_ADR, 0x55);
	CYFISNP_Write(TX_OFFSET_MSB_ADR, 0x05);
	/*  Set the CYFISNP_ transaction and TX configuration. */
	CYFISNP_SetXactConfig(xactConfig);
	CYFISNP_SetTxConfig(txConfig); 
	CYFISNP_State = CYFISNP_IDLE;              
}

/*******************************************************************************
 * Function Name: void CYFISNP_SetChannel(uint8  channel)
 *******************************************************************************
 * Summary:
 * Sets the CYFISNP_ channel to a specified frequency. The frequency has the value
 * of (2402 + channel) MHz.
 ******************************************************************************/
void CYFISNP_SetChannel(uint8  channel)
{		
	if(channel < CHANNEL_MAX) 
		CYFISNP_Write(CHANNEL_ADR, channel + 1);		
}

/*******************************************************************************
 * Function Name: uint8  CYFISNP_GetChannel(void)
 *******************************************************************************
 * Summary:
 * Returns the 8-bit channel number. The frequency is interpreted as
 * (2402 + channel) MHz.
 ******************************************************************************/
uint8  CYFISNP_GetChannel(void)
{
	return (CYFISNP_Read(CHANNEL_ADR)-1);
}

/*******************************************************************************
 * Function Name: void CYFISNP_SetTxConfig(uint8  config)
 *******************************************************************************
 * Summary:
 * Set the transmitter configuration
 ******************************************************************************/
void CYFISNP_SetTxConfig(uint8  config)
{
	CYFISNP_Write(TX_CFG_ADR, config);    
}

/*******************************************************************************
 * Function Name: uint8  CYFISNP_GetTxConfig(void)
 *******************************************************************************
 * Summary:
 * Get the transmitter configuration
 ******************************************************************************/
uint8  CYFISNP_GetTxConfig(void)
{
	return CYFISNP_Read(TX_CFG_ADR);
}

/*******************************************************************************
 * Function Name: void CYFISNP_SetXactConfig(uint8  config)
 *******************************************************************************
 * Summary:
 * Set the transaction configuration
 ******************************************************************************/
void CYFISNP_SetXactConfig(uint8  config)
{
	CYFISNP_XactConfig = config & ~FRC_END_STATE;
	CYFISNP_Write(XACT_CFG_ADR, config);
}

/*******************************************************************************
 * Function Name: uint8  CYFISNP_GetXactConfig(void)
 *******************************************************************************
 * Summary:
 * Get the transaction configuration
 ******************************************************************************/
uint8  CYFISNP_GetXactConfig(void)
{
	return CYFISNP_Read(XACT_CFG_ADR);
}

/*******************************************************************************
 * Function Name: void CYFISNP_SetFrameConfig(uint8 config)
 *******************************************************************************
 * Summary:
 * Set the framing configuration
 ******************************************************************************/
void CYFISNP_SetFrameConfig(uint8 config)
{
	CYFISNP_Write(FRAMING_CFG_ADR, config);
}

/*******************************************************************************
 * Function Name: uint8 CYFISNP_GetFrameConfig(void)
 *******************************************************************************
 * Summary:
 * Get the framing configuration
 ******************************************************************************/
uint8 CYFISNP_GetFrameConfig(void)
{
	return CYFISNP_Read(FRAMING_CFG_ADR);
}

/*******************************************************************************
 * Function Name: void CYFISNP_SetThreshold32(uint8  threshold)
 *******************************************************************************
 * Summary:
 * Set the threshold for the 32 chip data modes
 ******************************************************************************/
void CYFISNP_SetThreshold32(uint8  threshold)
{
	CYFISNP_Write(data32_THOLD_ADR, threshold);
}

/*******************************************************************************
 * Function Name: uint8  CYFISNP_GetThreshold32(void)
 *******************************************************************************
 * Summary:
 * Get the threshold for the 32 chip data modes
 ******************************************************************************/
uint8  CYFISNP_GetThreshold32(void)
{
	return CYFISNP_Read(data32_THOLD_ADR);
}

/*******************************************************************************
 * Function Name: void CYFISNP_SetThreshold64(uint8  threshold)
 *******************************************************************************
 * Summary:
 * Set the threshold for the 64 chip data modes
 ******************************************************************************/
void CYFISNP_SetThreshold64(uint8  threshold)
{
	CYFISNP_Write(data64_THOLD_ADR, threshold);
}

/*******************************************************************************
 * Function Name: uint8  CYFISNP_GetThreshold64(void)
 *******************************************************************************
 * Summary:
 * Get the threshold for the 64 chip data modes
 ******************************************************************************/
uint8  CYFISNP_GetThreshold64(void)
{
	return CYFISNP_Read(data64_THOLD_ADR);
}

/*******************************************************************************
 * Function Name: void CYFISNP_SetPreambleCount(uint8  count)
 *******************************************************************************
 * Summary:
 * Set the preamble repetition count
 ******************************************************************************/
void CYFISNP_SetPreambleCount(uint8  count)
{
	uint8  buf[3];
	/* Preamble file register is 3 bytes and must always be accessed 3 times
	 * so do a throw away burst read of 2 to move it a total of 3 accesses. */
	CYFISNP_SetPtr(buf);
	CYFISNP_SetLength(3);

	/*Read modify and write First byte of the array would be Preamble count. */
	CYFISNP_FileRead(PREAMBLE_ADR, 3);
	buf[0] = count;
	CYFISNP_FileWrite(PREAMBLE_ADR, 3);
}

/*******************************************************************************
 * Function Name: uint8  CYFISNP_GetPreambleCount(void)
 *******************************************************************************
 * Summary:
 * Get the preamble repetition count
 ******************************************************************************/
uint8  CYFISNP_GetPreambleCount(void)
{
	/* The preamble file register is 3 bytes and must always be accessed 3 times 
	 * so do a burst read of the whole 3 byte file register.
	 * The first byte is the count.  
	 */
	uint8  buf[3];
	CYFISNP_SetPtr(buf);
	CYFISNP_SetLength(3);
	CYFISNP_FileRead(PREAMBLE_ADR, 3);
	return buf[0];
}

/*******************************************************************************
 * Function Name: void CYFISNP_SetPreamblePattern(unsigned short pattern)
 *******************************************************************************
 * Summary:
 * Set the preamble pattern
 ******************************************************************************/
void CYFISNP_SetPreamblePattern(uint16 pattern)
{
	/* Save the pattern.Gotta access the whole 3 byte file -do throw away read of preamblecount.
	 * Write the pattern as a 2 byte file register burst. */
	uint8  buf[3];
	CYFISNP_SetPtr(buf);
	CYFISNP_SetLength(3);
	CYFISNP_FileRead(PREAMBLE_ADR, 3);
	buf[1] = (uint8 )(pattern >> 0);
	buf[2] = (uint8 )(pattern >> 8);
	CYFISNP_FileWrite(PREAMBLE_ADR, 3);
}

/*******************************************************************************
 * Function Name: uint16  CYFISNP_GetPreamblePattern(void)
 *******************************************************************************
 * Summary:
 * Get the preamble pattern
 ******************************************************************************/
uint16  CYFISNP_GetPreamblePattern(void)
{
	/* The preamble file register is 3 bytes and must always be accessed 3 times
	 * so do a burst read of the whole 3 byte file register.
	 * The first byte is the count and the pattern is the 2nd and 3rd bytes.
	 */
	uint8  buf[3];
	CYFISNP_SetPtr(buf);
	CYFISNP_SetLength(3);
	CYFISNP_FileRead(PREAMBLE_ADR, 3);
	return ((buf[2]<<8) | buf[1]);
}

/*******************************************************************************
 * Function Name: void CYFISNP_SetCrcSeed(uint16  crcSeed)
 *******************************************************************************
 * Summary:
 * Set CRC seed for both transmit and receive
 ******************************************************************************/
void CYFISNP_SetCrcSeed(uint16  crcSeed)
{
	CYFISNP_Write(CRC_SEED_LSB_ADR,(uint8)crcSeed);
	CYFISNP_Write(CRC_SEED_MSB_ADR,(uint8)(crcSeed>>8));
}

/*******************************************************************************
 * Function Name: uint16  CYFISNP_GetCrcSeed(void)
 *******************************************************************************
 * Summary:
 * Get CRC seed for both transmit and receive
 ******************************************************************************/
uint16  CYFISNP_GetCrcSeed(void)
{
	uint16 crcSeed;
	crcSeed = CYFISNP_Read(CRC_SEED_LSB_ADR);
	crcSeed |= (CYFISNP_Read(CRC_SEED_MSB_ADR) <<8);
	return crcSeed;
}

/*******************************************************************************
 * Function Name: void CYFISNP_GetFuses(void)
 *******************************************************************************
 * Summary:
 * Get the fuse values from the CYFISNP_. Store 6 bytes at the buffer specified by 
 * last call to CYFISNP_SetPtr()
 ******************************************************************************/
void CYFISNP_GetFuses(void)
{
	uint8 * ptr;
	uint8  ivar;
	CYFISNP_Write(MFG_ID_ADR, 0xff);   /* Turn on fuse read bias */                     
	CYFISNP_FileRead(MFG_ID_ADR, 6);   /* Read the fuses */
	CYFISNP_Write(MFG_ID_ADR, 0);      /* Disable reading fuses */

	/* Invert Manufacturing ID if want to subsequently decompose into various
	 * manufacturing fields (version/vendor/year/workweek/Lot/Wafer/X/Y/etc)
	 * Leave as-is if just want a globally unique string.
	 */
	ptr = CYFISNP_Ptr;
	for(ivar=0; ivar < 6; ++ivar) {
			*ptr ^= 0xFF;
			++ptr;
	}
}

/*******************************************************************************
 * Function Name: void CYFISNP_SetConstSopPnCode(const uint8  *pdata)
 *******************************************************************************
 * Summary:
 * Set start of packet PN Code by address
 ******************************************************************************/
void CYFISNP_SetConstSopPnCode(const uint8  *pdata)
{
	uint8  ivar;
	ivar = PNCODE_LENGTH;
	do {
		CYFISNP_Write(SOP_CODE_ADR, *pdata);
		++pdata;
	} while (--ivar != 0);
}

/*******************************************************************************
 * Function Name: void CYFISNP_SetConstdataPnCode(const uint8  *pdata)
 *******************************************************************************
 * Summary:
 * Get start of Packet PN Code by address
 ******************************************************************************/
void CYFISNP_SetConstdataPnCode(const uint8  *pdata)
{
	uint8  ivar;
	ivar = 16;      /* Copy 16 bytes */
	do {
		CYFISNP_Write(data_CODE_ADR, *pdata);
		++pdata;
	} while (--ivar != 0);
}

/*******************************************************************************
 * Function Name: void CYFISNP_SetSopPnCode(uint8  patternNum)
 *******************************************************************************
 * Summary:
 * Set the Start Of Packet PN Code, by patten number
 ******************************************************************************/
void CYFISNP_SetSopPnCode(uint8  patternNum)
{   
	CYFISNP_SetConstSopPnCode(PnCode + patternNum * PNCODE_LENGTH);
}

/*******************************************************************************
 * Function Name: void CYFISNP_StartTransmit(uint8  retryCt, uint8 len)
 *******************************************************************************
 * Summary:
 * Start transmitting a packet. The packet buffer location must
 * have previously been set via CYFISNP_SetPtr().
 * 
 * After calling CYFISNP_StartTransmit(), progress should be checked
 * via CYFISNP_GetTransmitState(). When CYFISNP_GetTransmitState()
 * indicates COMPETE, then CYFISNP_EndTransmit() should be called.
 *
 * After calling CYFISNP_StartTransmit() NO CALLS can be made to the
 * configuration access routines until the transmit operation is
 * terminated with a call to CYFISNP_EndTransmit() or CYFISNP_Abort().
 * Until one of those calls is made to end the transmit operation
 * the only other call supported is CYFISNP_GetTransmitState().
 ******************************************************************************/
void CYFISNP_StartTransmit(uint8  retryCt, uint8 len)
{
	CYFISNP_RetryCount = retryCt;
	packetLen = len;
	CYFISNP_RestartTransmit();
}

/*******************************************************************************
 * Function Name:  void CYFISNP_RestartTransmit(void) 
 *******************************************************************************
 * Summary:
 * Start transmitting a packet.
 ******************************************************************************/
void CYFISNP_RestartTransmit(void)  
{
	wakeupRadio();
	CYFISNP_State = CYFISNP_TX;  
	CYFISNP_Write(TX_LENGTH_ADR, packetLen);

#ifdef SET_TX_GO_BEFORE_data /* (aggressive) */
	CYFISNP_Write(TX_CTRL_ADR, TX_GO | TX_CLR | TXC_IRQ | TXE_IRQ);
	CYFISNP_FileWrite(TX_BUFFER_ADR, packetLen);
#else   /* write tx data, then set TX_GO (conservative) */
	CYFISNP_Write     (TX_CTRL_ADR, TX_CLR);
	CYFISNP_FileWrite(TX_BUFFER_ADR, packetLen);
	CYFISNP_Write     (TX_CTRL_ADR, TX_GO | TXC_IRQ | TXE_IRQ);
#endif
}

/*******************************************************************************
 * Function Name: CYFISNP_STATE CYFISNP_GetTransmitState(void)
 *******************************************************************************
 * Summary:
 * Get the state of the current transmit operation.
 * This call should be made after starting a transmit
 * operation with the CYFISNP_StartTransmit function.
 *
 * Although the bits in hardware status register clear
 * automatically, make them sticky until the CYFISNP_EndReceive
 ******************************************************************************/
uint8  CYFISNP_GetTransmitState(void)
{        
	uint8  temp=0;
	if (IsCYFISNP_InterruptActive() != 0) {    
		/* Any IRQ indicates TX_GO is finished */
	   temp = CYFISNP_ReadStatusDebounced(TX_IRQ_STATUS_ADR);
	   temp &= (TXE_IRQ | TXC_IRQ);
	   CYFISNP_State |= temp;
	   if (temp == TXC_IRQ) {
				/* Transmission is complete */
			   return CYFISNP_State;
		} 
		else {
				/*  During Transaction Ack Rx window, if SOP is received then get
				 *  RXC/RXE (not TXC/TXE).  CYFISNP_Init() ties RXC/RXE to IRQ.
				 *  Reading RX_IRQ_STATUS_ADR ensures IRQ from RXC/RXE deasserts.
				 */
				temp = CYFISNP_Read(RX_IRQ_STATUS_ADR);    
				CYFISNP_State |= (TXE_IRQ | TXC_IRQ);  
				if (CYFISNP_RetryCount > 0) {
					--CYFISNP_RetryCount;
					temp = 0;
					while(temp < fine_tun_d1) {
						Cy_DelayUS2(100);
						temp++;
					}
					CYFISNP_RestartTransmit();
				}
			}
	}
	return CYFISNP_State;
}

/*******************************************************************************
 * Function Name: void CYFISNP_EndTransmit(void)
 *******************************************************************************
 * Summary:
 * Complete a transmit operation
 ******************************************************************************/
void CYFISNP_EndTransmit(void)
{
	CYFISNP_State = CYFISNP_IDLE;
}

/*******************************************************************************
 * Function Name: uint8  CYFISNP_BlockingTransmit(uint8  retryCt, uint8 len)
 *******************************************************************************
 * Summary:
 * Transmit a packet. Block execution until it completes.
 * The packet source data address is set via CYFISNP_SetPtr().
 ******************************************************************************/
uint8  CYFISNP_BlockingTransmit(uint8  retryCt, uint8 len)
{
	uint8  status;

	CYFISNP_StartTransmit(retryCt, len);

	do {
		status = CYFISNP_GetTransmitState();
	} while ((status & (TXC_IRQ | TXE_IRQ)) == 0);

	CYFISNP_EndTransmit();
	return status;
}

/*******************************************************************************
 * Function Name:  void CYFISNP_StartReceive(void)
 *******************************************************************************
 * Summary:
 * The location and size of the Rx buffer must have previously 
 * set via CYFISNP_SetPtr() and CYFISNP_SetLength().
 *
 * After calling CYFISNP_StartReceive(), receive progress should be 
 * checked via CYFISNP_GetReceiveState(). When CYFISNP_GetReceiveState()
 * indicates CYFISNP_COMPLETE, then call CYFISNP_EndReceive().
 *
 * Receive is started using the "RX_GO" bit. All interesting
 * interrupt enables are set and CYFISNP_GetReceiveState() can be
 * polled in systems, or can be called via an ISR if the CYFISNP_'s
 * IRQ signal generates a CPU interrupt.
 *
 * After calling CYFISNP_StartReceive() NO CALLS can be made to the
 * configuration access routines until the receive operation is
 * terminated with a call to CYFISNP_EndReceive() or CYFISNP_Abort().
 * Until one of those calls is made to end the receive operation
 * the only other calls supported are CYFISNP_GetReceiveState and
 * CYFISNP_GetRssi.
 ******************************************************************************/
 void CYFISNP_StartReceive(void)
 {
	CYFISNP_State = CYFISNP_RX;
	wakeupRadio();                                      // start oscillator
	CYFISNP_Write(RX_CTRL_ADR, RX_GO | RXC_IRQ | RXE_IRQ); // start receive        
	/*tregVal = */CYFISNP_Read(RX_IRQ_STATUS_ADR);                 // Clr SOP detect bit       Agust 29, Jason chen
 }

/*******************************************************************************
 * Function Name:  uint8  CYFISNP_GetReceiveState(void)
 *******************************************************************************
 * Summary:
 * Return the state of current receive operation.  This call 
 * is made after starting a receive via CYFISNP_StartReceive().
 *
 * Although the bits in the state register in the hardware clear
 * automatically, we make them sticky until CYFISNP_EndReceive()
 ******************************************************************************/
uint8  CYFISNP_GetReceiveState(void)
{
	uint8  status;
	if (IsCYFISNP_InterruptActive() != 0) {
		status = CYFISNP_ReadStatusDebounced(RX_IRQ_STATUS_ADR);
		status &= (RXBERR_IRQ | RXE_IRQ | RXC_IRQ);
		CYFISNP_State |= status;
	}

	if (CYFISNP_State & (RXBERR_IRQ | RXE_IRQ)) {  
		/* RXBERR or RXE implies RXC  */
		CYFISNP_State |= (RXC_IRQ | RXE_IRQ);
	}
	return CYFISNP_State; 
}

/*******************************************************************************
 * Function Name:  uint8 CYFISNP_EndReceive(void)
 *******************************************************************************
 * Summary:
 * Complete a receive operation.
 ******************************************************************************/
 
 
#if 0 
byte CYFISNP_NODE_EndReceive(void)
{
    byte rxCfg;
    byte BytesToRead;

    // read current VLD_EN setting
    rxCfg = CYFISNP_NODE_Read(CYFISNP_NODE_RX_CFG_ADR);                          // [0x06]

    // total number of bytes in Rx FIFO
    CYFISNP_NODE_RxCount = CYFISNP_NODE_Read(CYFISNP_NODE_RX_COUNT_ADR);         // [0x09]
    if( rxCfg & CYFISNP_NODE_VLD_EN )
        CYFISNP_NODE_RxCount *= 2;      // 2x, every data byte has a valid byte

    BytesToRead = CYFISNP_NODE_RxCount - CYFISNP_NODE_BytesRead;   // subtract # bytes we've extracted
    if( CYFISNP_NODE_State & CYFISNP_NODE_RXE_IRQ )  // gets updated with RadioGetReceiveState()
        BytesToRead = CYFISNP_NODE_LP_FIFO_SIZE;   // if error, read all of Rx Buffer, regardless of RX_COUNT_ADR

    CYFISNP_NODE_FileReadWip(CYFISNP_NODE_RX_BUFFER_ADR, BytesToRead);           // [0x21], file register

    // if user wanted END_STATE_SLEEP, then undo
    // RadioStartReceive()'s intermediate END_STATE_IDLE override
    if( CYFISNP_NODE_IsSleepDesired() ) 
    {
        CYFISNP_NODE_GoToEndState();       // workaround if user's end state is Sleep
    }

    // is awake
    return CYFISNP_NODE_RxCleanup();
} 
#endif 
  
uint8 CYFISNP_EndReceive(void)
{
	uint8  count, t3;
	uint8 retval;
	
  //uint8 * ptr;
  //ptr = CYFISNP_Ptr;


	t3 = CYFISNP_Read(RX_CFG_ADR);
	/*Reading the Number of data received at Rx */
	count = CYFISNP_Read(RX_COUNT_ADR);   
	if (t3 & VLD_EN) {
			/* Load data + valid bits */
			//count += count;           ?????????????????????????????????????????????????????????
			count *=2;
	}

	/* data Received from Rx*/
	retval = count;                    


	if ((CYFISNP_State & RXE_IRQ) != 0) {  
				/* Buffer Error Condition */
			t3 = CYFISNP_ReadStatusDebounced(RX_IRQ_STATUS_ADR);
			if(t3 & RXC_IRQ) {
						/* Unload FULL RxFIFO */
					count = CYFISNP_FIFO_LEN;         
			}
	}

	CYFISNP_FileRead(RX_BUFFER_ADR, count);
	
	if(!(CYFISNP_XactConfig & END_STATE_MSK)) {
			/* Write desired End State using FORCE_END_STATE feature */
			CYFISNP_Write(XACT_CFG_ADR, CYFISNP_XactConfig | FRC_END_STATE);
			while ((CYFISNP_Read(XACT_CFG_ADR) & FRC_END_STATE) != 0) {
				/* Wait for FORCE_END to complete */
			}
	}        
	CYFISNP_State = CYFISNP_IDLE; 
	return retval;
}

/*******************************************************************************
 * Function Name: void CYFISNP_ForceState(uint8  endStateBitsOnly)
 *******************************************************************************
 * Summary:
 * Force CYFISNP_ to desired state
 ******************************************************************************/
void CYFISNP_ForceState(uint8  endStateBitsOnly)
{
	if (IsCYFISNP_Sleep) {
			forceState(END_STATE_IDLE);     /* wake Radio */
	}
	forceState(endStateBitsOnly);       /* Force the CYFISNP_ to state specified */
}

/*******************************************************************************
 * Function Name: static void forceState(uint8  newState)
 *******************************************************************************
 * Summary:
 * Force CYFISNP_ to desired state
 ******************************************************************************/
static void forceState(uint8  newState)
{
	uint16  delay_5ms;
	newState |= (CYFISNP_XactConfig & ~END_STATE_MSK) | FRC_END_STATE;

	for (;;) {
			CYFISNP_SetXactConfig(newState);
			delay_5ms = (5000 / CYFISNP_READ_MICROSEC) + 1;
			do {
					if ((CYFISNP_Read(XACT_CFG_ADR) & FRC_END_STATE) == 0) 
						return;						
			} while (--delay_5ms != 0);
			
			/*  FORCE_END failed to complete, implying going SLEEP to IDLE and 
			 *  oscillator failed to start.  Recover by returning to SLEEP and
			 *   trying to start oscillator again.
			 */
			CYFISNP_SetXactConfig((newState & ~END_STATE_MSK) | END_STATE_SLEEP);
	}
}

/*******************************************************************************
 * Function Name: static void wakeupRadio(void)
 *******************************************************************************
 * Summary:
 * Wake up the Radio from sleep 
 ******************************************************************************/
static void wakeupRadio(void)       
{
        if (IsCYFISNP_Sleep) {
                forceState(END_STATE_IDLE);
                /* Restore user's desired unforced transitional end-state  */
                 CYFISNP_SetXactConfig((CYFISNP_XactConfig & ~END_STATE_MSK) 
                                               | END_STATE_SLEEP);
        }
}

/*******************************************************************************
 * Function Name: uint8 CYFISNP_GetRssi(void)
 *******************************************************************************
 * Summary:
 * Return receiver signal strength indicator
 ******************************************************************************/
uint8 CYFISNP_GetRssi(void)
{
	return CYFISNP_Read(RSSI_ADR);
}

/*******************************************************************************
 * Function Name: uint8 CYFISNP_GetRssi(void)
 *******************************************************************************
 * Summary:
 * Abort a receive operation.
 *
 * Note:
 * A race condition exists where RXC (and auto ack) occurs just prior
 * to CYFISNP_Abort().  There's also a problem where we can't abort an
 * in-process receive.  Therefore, CYFISNP_Abort() disconnects the Rx 
 * antenna and waits until things "settle-down" before exiting RX Mode.
 ******************************************************************************/
uint8 CYFISNP_Abort(void)
{
	uint8  retval;

	retval = CYFISNP_ABORT_SUCCESS; 
	if(CYFISNP_State & CYFISNP_RX) {         
		/* Abort handling is RX specific. */

		CYFISNP_Write(RX_ABORT_ADR, ABORT_EN); 
		/* Disconnect Rx antenna/loopback */
#if 0
		delay_50us = (50 / CYFISNP_READ_MICROSEC) + 1;
		do {
			CYFISNP_State |= CYFISNP_Read(RX_IRQ_STATUS_ADR);
		} while (--delay_50us != 0);
#endif
		CYFISNP_State |= CYFISNP_ReadStatusDebounced(RX_IRQ_STATUS_ADR);
		
		if (CYFISNP_State & SOFDET_IRQ) {
			while ((CYFISNP_GetReceiveState() & CYFISNP_COMPLETE) == 0) {
				/* Allow Rx to finish naturally */
			}
			CYFISNP_Write(RX_ABORT_ADR, 0x00);
			return CYFISNP_EndReceive();   /* Return w/something received */
		}

		/* goto desired end-state via FRC_END_STATE */
		CYFISNP_Write( XACT_CFG_ADR, CYFISNP_XactConfig | FRC_END_STATE );
		while((CYFISNP_Read(XACT_CFG_ADR) & FRC_END_STATE) != 0) {
				/* Wait for FORCE_END_STATE to finish */
		}       

		CYFISNP_State = CYFISNP_IDLE; 
		CYFISNP_Write(RX_ABORT_ADR, 0x00);
	}
	return retval;
}

/*******************************************************************************
 * Function Name: uint8 CYFISNP_Poll(void)
 *******************************************************************************
 * Summary:
 * Manage the CYFISNP_ in a polling loop.
 * Result is monitored in global variable: CYFISNP_State
 ******************************************************************************/
uint8 CYFISNP_Poll(void) 
{
	if ((CYFISNP_State & CYFISNP_TX) != 0) {         
			(void)CYFISNP_GetTransmitState();          
	}
	else if ((CYFISNP_State & CYFISNP_RX) != 0) {    
			(void)CYFISNP_GetReceiveState();           
	}
	return CYFISNP_State;
}

/*******************************************************************************
 * Function Name: void CYFISNP_Interrupt(void)
 *******************************************************************************
 * Summary:
 * ISR, wrapper for direct vector ISR vector
 * Result is monitored in global variable: CYFISNP_State
 ******************************************************************************/
void CYFISNP_Interrupt(void) 
{
	(void)CYFISNP_Poll();
}

/*******************************************************************************
 * Function Name: uint8 CYFISNP_GetReceiveStatus(void)
 *******************************************************************************
 * Summary:
 * Get the receiver status register.
 ******************************************************************************/
uint8 CYFISNP_GetReceiveStatus(void)
{
	return CYFISNP_Read(RX_STATUS_ADR);
}

