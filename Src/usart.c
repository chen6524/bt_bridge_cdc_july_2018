/**
  ******************************************************************************
  * File Name          : USART.c
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "usart.h"

#include "gpio.h"

/* USER CODE BEGIN 0 */
#include "gpio.h"
#include "myusbd_custom_hid_if.h"

#include "CYFISNP_Timer.h"
#include "tim.h"
uint8_t RxBuffer[64];
uint8_t RxDecBuffer[64];

extern void Cy_DelayUS2(uint32_t microseconds);
/* USER CODE END 0 */

UART_HandleTypeDef hlpuart1;

/* LPUART1 init function */

void MX_LPUART1_UART_Init(void)
{

  hlpuart1.Instance = LPUART1;
  hlpuart1.Init.BaudRate = 115200;
  hlpuart1.Init.WordLength = UART_WORDLENGTH_8B;
  hlpuart1.Init.StopBits = UART_STOPBITS_1;
  hlpuart1.Init.Parity = UART_PARITY_NONE;
  hlpuart1.Init.Mode = UART_MODE_TX_RX;
  hlpuart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  hlpuart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  hlpuart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_RXOVERRUNDISABLE_INIT|UART_ADVFEATURE_DMADISABLEONERROR_INIT;
  hlpuart1.AdvancedInit.OverrunDisable = UART_ADVFEATURE_OVERRUN_DISABLE;
  hlpuart1.AdvancedInit.DMADisableonRxError = UART_ADVFEATURE_DMA_DISABLEONRXERROR;
  if (HAL_UART_Init(&hlpuart1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

void HAL_UART_MspInit(UART_HandleTypeDef* uartHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct;
  if(uartHandle->Instance==LPUART1)
  {
  /* USER CODE BEGIN LPUART1_MspInit 0 */

  /* USER CODE END LPUART1_MspInit 0 */
    /* LPUART1 clock enable */
    __HAL_RCC_LPUART1_CLK_ENABLE();
  
    /**LPUART1 GPIO Configuration    
    PC1     ------> LPUART1_TX
    PC0     ------> LPUART1_RX 
    */
    GPIO_InitStruct.Pin = BT_UART_TX_Pin|BT_UART_RX_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF8_LPUART1;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    /* LPUART1 interrupt Init */
    HAL_NVIC_SetPriority(LPUART1_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(LPUART1_IRQn);
  /* USER CODE BEGIN LPUART1_MspInit 1 */
    //hlpuart1.pRxBuffPtr = &RxBuffer[0];
		//hlpuart1.RxXferSize  = 20;
		//hlpuart1.RxXferCount = 20;   
  /* USER CODE END LPUART1_MspInit 1 */
  }
}

void HAL_UART_MspDeInit(UART_HandleTypeDef* uartHandle)
{

  if(uartHandle->Instance==LPUART1)
  {
  /* USER CODE BEGIN LPUART1_MspDeInit 0 */

  /* USER CODE END LPUART1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_LPUART1_CLK_DISABLE();
  
    /**LPUART1 GPIO Configuration    
    PC1     ------> LPUART1_TX
    PC0     ------> LPUART1_RX 
    */
    HAL_GPIO_DeInit(GPIOC, BT_UART_TX_Pin|BT_UART_RX_Pin);

    /* LPUART1 interrupt Deinit */
    HAL_NVIC_DisableIRQ(LPUART1_IRQn);
  /* USER CODE BEGIN LPUART1_MspDeInit 1 */

  /* USER CODE END LPUART1_MspDeInit 1 */
  }
} 

/* USER CODE BEGIN 1 */
extern int8_t Get_USB_Out_Buffer(uint8_t *pBuffer, uint8_t USB_or_BLE);
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
     uint8_t *p = RxDecBuffer;  
	   uint8_t len;
	
	  if(RxBuffer[huart->RxXferSize - 1] == 0x0D)//&&(RxBuffer[21] == 0x0A))
		{						
			if( huart->RxXferSize == 1)
			{
			  *p++ = 1;                       // packet size 20 bytes
        *p++ = 0x0D;				
			}
#if 0					
			else
			{
				*p++ = MSG_HUB_TO_HOST_UPLOAD;	  // 0x14			
				len = huart->RxXferSize;
				if(len > 40)
					len =40;
				len = len/2;
				len = len * 2;
				*p++ = len/2;                       // packet size 20 bytes
				len = DecodeToDec(RxBuffer, len, p);
			}
			
			SendToHost(ga8TxBuffer);
#else
			else
			{
				len = huart->RxXferSize;
				if(len > 40)
					len =40;
				len = len/2;
				len = len * 2;			
			  DecodeToDec(RxBuffer, len, RxDecBuffer);
				Get_USB_Out_Buffer(RxDecBuffer, 1);
			  //BCD_Cmd_Process(ga8TxBuffer,1);
			}
#endif			
						
			//for(n=0; n< huart->RxXferSize; n++)
			//	*p++ = *data++;	                // BLE Rx Data

			
			
			if (HAL_UART_Receive_IT(&hlpuart1, RxBuffer, 41) != HAL_OK)	
			{
				Error_Handler();
			}			
		}
		else
		{
			if (HAL_UART_Receive_IT(&hlpuart1, RxBuffer, 1) != HAL_OK)	
			{
				Error_Handler();
			}									
		}
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{

  if (HAL_UART_Receive_IT(&hlpuart1, RxBuffer, 1) != HAL_OK)	
	{
		Error_Handler();
	}
}

uint8_t DecodeToDec(uint8_t *in, uint8_t len, uint8_t * out)
{
	uint8_t i, j;
	uint8_t tmpH,tmpL;

	//if(len%2 != 0) len +=1;

	j = 0;
	for(i = 0; i < len/2 ; i++)
	{
		if((in[j] >= '0') && (in[j] <= '9'))
			tmpH = in[j] - '0';
		else
		{
			if((in[j] >= 'A') && (in[j] <= 'F'))
				tmpH = in[j] - 'A' + 10;
			else if((in[j] >= 'a') && (in[j] <= 'f'))
				tmpH = in[j] - 'a' + 10;
		}
		j++;
		if((in[j] >= '0') && (in[j] <= '9'))
			tmpL = in[j] - '0';
		else
		{
			if((in[j] >= 'A') && (in[j] <= 'F'))
				tmpL = in[j] - 'A' + 10;
			else if((in[j] >= 'a') && (in[j] <= 'f'))
				tmpL = in[j] - 'a' + 10;
		}
		j++;

		out[i] = (uint8_t)(tmpH * 16 + tmpL);
	}
	return len/2;
}


uint8_t* DecodeToHex(uint8_t *in, uint8_t len, uint8_t *out)
{
    uint8_t i, j;
	  uint16_t tmp;

	  j = 0;
    for (i = 0; i < len; i++) 
	  {			
			tmp = in[i];
			out[j++] = hex[(tmp >> 4) & 0x0F];
			out[j++] = hex[tmp & 0x0F];			
    }
    return out;
}


void UART_Send(uint8_t *txBuffer, uint16_t len)
{
	
	 //HAL_BUSY
	 while(!(hlpuart1.gState == HAL_UART_STATE_READY))
	 {
		 Cy_DelayUS2(50);
	 }		 
 //HAL_UART_Transmit(&hlpuart1, txBuffer, len, 100);//HAL_MAX_DELAY 
   HAL_UART_Transmit_IT(&hlpuart1, txBuffer, len);
	 HAL_Delay(10);	 
}

void UART_SendForNodeInfo(uint8_t *txBuffer, uint16_t len)
{
	
	 //HAL_BUSY
	 while(!(hlpuart1.gState == HAL_UART_STATE_READY))
	 {
		 Cy_DelayUS2(50);
	 }		 
   HAL_UART_Transmit(&hlpuart1, txBuffer, len, 100);
 //HAL_UART_Transmit_IT(&hlpuart1, txBuffer, len);
	 HAL_Delay(60);	 
}

void UART_SendUpload(uint8_t *txBuffer, uint16_t len)
{
	
	 //HAL_BUSY
	 while(!(hlpuart1.gState == HAL_UART_STATE_READY))
	 {
		 Cy_DelayUS2(50);
	 }		 
 //HAL_UART_Transmit(&hlpuart1, txBuffer, len, 100);//HAL_MAX_DELAY 
   HAL_UART_Transmit_IT(&hlpuart1, txBuffer, len);
	 //HAL_Delay(10);	 
	 Cy_DelayUS2(300);
}
/* USER CODE END 1 */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
