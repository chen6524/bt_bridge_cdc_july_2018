/**
  ******************************************************************************
  * @file           : usbd_custom_hid_if.c
  * @version        : v2.0_Cube
  * @brief          : USB Device Custom HID interface file.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "myusbd_custom_hid_if.h"

/* USER CODE BEGIN INCLUDE */
#include "spi.h"
#include "usart.h"
#include "tim.h"
#include "lptim.h"
#include "gpio.h"
#include "iwdg.h"
#include "CYFISNP_Protocol.h"
#include "CYFISNP_Flash_Utility.h"
#include "MAX14676.h"
/* USER CODE END INCLUDE */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/** @addtogroup STM32_USB_OTG_DEVICE_LIBRARY
  * @brief Usb device.
  * @{
  */

/** @addtogroup USBD_CUSTOM_HID
  * @{
  */

/** @defgroup USBD_CUSTOM_HID_Private_TypesDefinitions USBD_CUSTOM_HID_Private_TypesDefinitions
  * @brief Private types.
  * @{
  */

/* USER CODE BEGIN PRIVATE_TYPES */
/* USER CODE END PRIVATE_TYPES */

/**
  * @}
  */

/** @defgroup USBD_CUSTOM_HID_Private_Defines USBD_CUSTOM_HID_Private_Defines
  * @brief Private defines.
  * @{
  */

/* USER CODE BEGIN PRIVATE_DEFINES */
/* USER CODE END PRIVATE_DEFINES */

/**
  * @}
  */

/** @defgroup USBD_CUSTOM_HID_Private_Macros USBD_CUSTOM_HID_Private_Macros
  * @brief Private macros.
  * @{
  */

/* USER CODE BEGIN PRIVATE_MACRO */
/* USER CODE END PRIVATE_MACRO */

/**
  * @}
  */

/** @defgroup USBD_CUSTOM_HID_Private_Variables USBD_CUSTOM_HID_Private_Variables
  * @brief Private variables.
  * @{
  */

/** Usb HID report descriptor. */
__ALIGN_BEGIN static uint8_t CUSTOM_HID_ReportDesc_FS[USBD_CUSTOM_HID_REPORT_DESC_SIZE] __ALIGN_END =
{
  /* USER CODE BEGIN 0 */
    0x06, 0xFF, 0x00,                        // Usage Page
    0x09, 0x00,                              // Usage
    0xA1, 0x01,                              // Collection
    0x09, 0x01,                              // Usage
    0x15, 0x80,                              // Logical Minimum
    0x25, 0x7F,                              // Logical Maximum
    0x35, 0x00,                              // Physical Minimum
    0x45, 0xFF,                              // Physical Maximum
    0x75, 0x08,                              // Report Size
    0x95, 0x40,                              // Report Count
    0x81, 0x02,                              // Input
    0x09, 0x02,                              // Usage
    0x15, 0x80,                              // Logical Minimum
    0x25, 0x7F,                              // Logical Maximum
    0x35, 0x00,                              // Physical Minimum
    0x45, 0xFF,                              // Physical Maximum
    0x75, 0x08,                              // Report Size
    0x95, 0x40,                              // Report Count
    0x91, 0x02,                              // Output
  /* USER CODE END 0 */
  0xC0    /*     END_COLLECTION	             */
};

/* USER CODE BEGIN PRIVATE_VARIABLES */
/* USER CODE END PRIVATE_VARIABLES */

/**
  * @}
  */

/** @defgroup USBD_CUSTOM_HID_Exported_Variables USBD_CUSTOM_HID_Exported_Variables
  * @brief Public variables.
  * @{
  */
extern USBD_HandleTypeDef hUsbDeviceFS;

/* USER CODE BEGIN EXPORTED_VARIABLES */
/* USER CODE END EXPORTED_VARIABLES */
/**
  * @}
  */

/** @defgroup USBD_CUSTOM_HID_Private_FunctionPrototypes USBD_CUSTOM_HID_Private_FunctionPrototypes
  * @brief Private functions declaration.
  * @{
  */

static int8_t CUSTOM_HID_Init_FS(void);
static int8_t CUSTOM_HID_DeInit_FS(void);
//static int8_t CUSTOM_HID_OutEvent_FS(uint8_t event_idx, uint8_t state);
extern int8_t ReplyToHost(uint8_t *pBuffer);
int8_t Get_USB_Out_Buffer(uint8_t *pBuffer, uint8_t USB_or_BLE); 
/**
  * @}
  */

USBD_CUSTOM_HID_ItfTypeDef USBD_CustomHID_fops_FS =
{
  CUSTOM_HID_ReportDesc_FS,
  CUSTOM_HID_Init_FS,
  CUSTOM_HID_DeInit_FS,
//CUSTOM_HID_OutEvent_FS
  //ReplyToHost,
	Get_USB_Out_Buffer	
};

/** @defgroup USBD_CUSTOM_HID_Private_Functions USBD_CUSTOM_HID_Private_Functions
  * @brief Private functions.
  * @{
  */

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Initializes the CUSTOM HID media low layer
  * @retval USBD_OK if all operations are OK else USBD_FAIL
  */
static int8_t CUSTOM_HID_Init_FS(void)
{
  /* USER CODE BEGIN 4 */
  return (0);
  /* USER CODE END 4 */
}

/**
  * @brief  DeInitializes the CUSTOM HID media low layer
  * @retval USBD_OK if all operations are OK else USBD_FAIL
  */
static int8_t CUSTOM_HID_DeInit_FS(void)
{
  /* USER CODE BEGIN 5 */
  return (0);
  /* USER CODE END 5 */
}

/**
  * @brief  Manage the CUSTOM HID class events
  * @param  event_idx: Event index
  * @param  state: Event state
  * @retval USBD_OK if all operations are OK else USBD_FAIL
  */
extern uint8_t ga8TxBuffer[TX_BUF_SIZE];	// Tx buffer Frame, to the Host
extern uint8_t ga8RxBuffer[RX_BUF_SIZE];
extern uint8_t gu8BindFlag;

// ---------------------------------------------------------------------------
// putBcdPacket() - Keep Back Channel Data buffers full for testing (every 500mSec)
// ---------------------------------------------------------------------------
void putBcdPacket(byte devId, byte databyte)
{
    CYFISNP_API_PKT  txApiPkt;       // BCD API data packet (Tx to Node)
	//char ivar;
	//static pktSize[1+CYFISNP_MAX_NODES];	// index 0 is not used	
    //static BYTE const payload[1+28] = "0123456789ABCDE123456789ABCDE";

	//if( ++pktSize[devId] > CYFISNP_BCD_PAYLOAD_MAX ) 
	//{
	//	pktSize[devId] = 0;
	//}

	txApiPkt.payload[0] = databyte;

	txApiPkt.length = 1;
	txApiPkt.type = CYFISNP_API_TYPE_CONF;
	txApiPkt.devId = devId;
	CYFISNP_TxdataPut(&txApiPkt);

}

uint8_t TxHexBuffer[64] = {0};
//static int8_t CUSTOM_HID_OutEvent_FS  (uint8_t *recvBuffer)
//extern uint8_t USB_OR_BLE;

int8_t Get_USB_Out_Buffer(uint8_t *pBuffer, uint8_t USB_or_BLE)	
{
	
	//uint8_t ga8RxBuffer[RX_BUF_SIZE] ={0};	// Rx buffer Frame, from the Host	
	USB_OR_BLE = USB_or_BLE;
	memcpy(ga8RxBuffer, pBuffer, 64);
	return 0;		
}

static FLASH_OBProgramInitTypeDef mPOBInit;	  
static uint32_t userType, userConfig, rdpLevel;
static uint32_t optionType;

void EnterBootMode(void)
{
			HAL_FLASH_Unlock();
			__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_OPTVERR); 
			HAL_FLASH_OB_Unlock();
				
			HAL_FLASHEx_OBGetConfig((FLASH_OBProgramInitTypeDef*)&mPOBInit);
			optionType = mPOBInit.OptionType;
			userConfig = mPOBInit.USERConfig;
			rdpLevel = mPOBInit.RDPLevel;
			
			//if(userConfig == 0xFFFFF800)
			{
				mPOBInit.OptionType  = (/*OPTIONBYTE_RDP |*/ OPTIONBYTE_USER);
				mPOBInit.USERType    = OB_USER_nSWBOOT0 | OB_USER_nBOOT0;// | OB_USER_nBOOT1;   // 0x2000, 0x4000, 0x0200
				mPOBInit.USERConfig &= ~(FLASH_OPTR_nSWBOOT0|FLASH_OPTR_nBOOT0);                // FLASH_OPTR_nSWBOOT0, FLASH_OPTR_nBOOT0, FLASH_OPTR_nBOOT1
				//mPOBInit.USERConfig |= FLASH_OPTR_nSWBOOT0;
				//mPOBInit.USERConfig &= ~FLASH_OPTR_nBOOT0;
				//HAL_Delay(3000);																																						 
				if(HAL_FLASHEx_OBProgram((FLASH_OBProgramInitTypeDef*)&mPOBInit) == HAL_OK)
				{
					if (mPOBInit.OptionType == (/*OPTIONBYTE_RDP | */OPTIONBYTE_USER))
					{
						DisableCHG();
						RED_LED_On();
						HAL_Delay(500);
						RED_LED_Off();
						HAL_Delay(1000);
						//__RESET_WATCHDOG(); 
						HAL_FLASH_OB_Launch();
						NVIC_SystemReset();
						optionType = mPOBInit.OptionType;
					}
				}
				else
				{
					  BLU_LED_On();	gu8LedTime = 1;	// will turn off after timer expires in the main()
	          CYFISNP_TimeSet(&gu32LedFlashTimer, BLINK_TIME2);
				}
			}
			HAL_FLASHEx_OBGetConfig((FLASH_OBProgramInitTypeDef*)&mPOBInit);
			optionType = mPOBInit.OptionType;
			userConfig = mPOBInit.USERConfig;

			HAL_FLASH_OB_Lock();
			HAL_FLASH_Lock();	
}


int8_t BCD_Cmd_Process(uint8_t *pBuffer, uint8_t USB_or_BLE)	
{ 
  /* USER CODE BEGIN 6 */ 
	
	uint8_t n, *p=ga8TxBuffer;
	uint8_t btn = 0;

	__RESET_WATCHDOG();                                      //MX_IWDG_RESET();
	USB_OR_BLE = USB_or_BLE;
	
	// some of the commands need the button be pressed. I check once for all (BIND, CLONE, RESET)
	btn = 0xFF;		// error, button not pushed

	if( pBuffer[0] == MSG_HOST_TO_HUB_GET_INFO ) // 0x51
	{		
    uint8_t const *pFlash = (uint8_t const*)(CYFISNP_EEP_NET_REC_ADR);
		uint8_t const *pDate = __DATE__;	// Sep 18 2012
		*p++ = MSG_HUB_TO_HOST_GET_INFO;	// 0x11
		for(n=0; n<8; n++)
			*p++ = *pFlash++;	// 1: sopIdx, 2: chBase, 3: chHop, 4: hubSeedMsb, 5: hubSeedLsb, 6: xx, 7: xx, 8: xx

		*p++ = CYFISNP_MAX_NODES;			    // 9
		*p++ = CYFISNP_BCD_PAYLOAD_MAX;		// 10

		*p++ = CYFISNP_CHAN_MIN;			    // 11
		*p++ = CYFISNP_CHAN_MAX;			    // 12
		*p++ = CYFISNP_bCurrentChannel;		// 13
		*p++ = 0;//gu8UploadFlag;				  // 14
		*p++ = 0;							// 15
		*p++ = 0;//gu16SignHL/256;				// 16
		*p++ = 0;//gu16SignHL%256;				// 17

		for(n=0; n<12; n++)
			*p++ = *pDate++;				        // 18-29
		*p++ = 0;							            // 30
		SendToHost(ga8TxBuffer,USB_or_BLE);
	}else  
	if( pBuffer[0] == MSG_HOST_TO_HUB_BIND_MODE ) {	// 0x52
		*p++ = MSG_HUB_TO_HOST_BIND_MODE;					// 0x12
		// May 15, 2013: button check is not needed for Binding
		btn = 0;

		if( btn == 0xFF ) {
			*p++ = btn;	// 0xFF
    	} else if( CYFISNP_eProtState == CYFISNP_BIND_MODE ) {
			*p++ = 2;	// error, already in Bind mode
		} else if( 0){//gu8UploadFlag ) {
			*p++ = 3;	// error, Upload mode active
		} 
		//else if( numNode_ShelfMode() ) {
		//	*p++ = 4;	// Error, Wall node present			
		//} 
#if JASON_MODIFICATION		                                       // Commented by Jason Chen, 2014.03.31
		else if( CYFISNP_sysMode1 > CYFISNP_MAX_NODES )              // Added Jason Chen, 2014.03.21
		{
			*p++ = 5;	// error, Sync mode active
		} 
#endif		
		else {
			*p++ = 0;	// ok
			gu8BindFlag = 1;
		}
		SendToHost(ga8TxBuffer,USB_or_BLE);
	}
	else if( pBuffer[0] == MSG_HOST_TO_HUB_NODE_INFO )  	// 0x53
	{
		uint8_t idx, devId, grpNo;

      //for(grpNo=0; grpNo<(CYFISNP_MAX_NODES/7); grpNo++) 
		for(grpNo=0; grpNo<(CYFISNP_MAX_NODES/7)+1; grpNo++)  // Changed by Jason Chen, 20140317 fixed Just only can send 49 nodes msg
		{
		  __RESET_WATCHDOG();                                 //MX_IWDG_RESET();                                    // Added by Jason Chen, 20140312 fixed Just only can send 49 nodes msg
			p = ga8TxBuffer;
			memset(p, 0, TX_BUF_SIZE);
			*p++ = MSG_HUB_TO_HOST_NODE_INFO;				          // 0x13
			*p++ = grpNo;
			for(idx=1; idx<=7; idx++) 
			{
				devId = (grpNo*7) + idx;
				if( devId <= CYFISNP_MAX_NODES ) {
			        uint8_t const *pFlash = (uint8_t const*)(&CYFISNP_EEP_DEV_REC_ADR[devId]);
					for(n=0; n<8; n++)
						*p++ = *pFlash++;
				}
			}			
		  SendToHostUSB(ga8TxBuffer);
		}
	}
	else if( pBuffer[0] == MSG_HOST_TO_HUB_NODE_BLE_INFO )  	// 0x5B
	{
		uint8_t idx, devId, grpNo;

  //for(grpNo=0; grpNo<(CYFISNP_MAX_NODES/7); grpNo++) 
		for(grpNo=0; grpNo<(CYFISNP_MAX_NODES/2)+1; grpNo++)    // For BLE sending, two nodes each time
		{
		  __RESET_WATCHDOG();                                   //MX_IWDG_RESET();;
			p = ga8TxBuffer;
			memset(p, 0, TX_BUF_SIZE);
			*p++ = MSG_HUB_TO_HOST_NODE_BLE_INFO;				          // 0x1B
			*p++ = grpNo;
			for(idx=1; idx<=2; idx++) 
			{
				devId = (grpNo*2) + idx;
				if( devId <= CYFISNP_MAX_NODES ) 
				{
			    uint8_t const *pFlash = (uint8_t const*)(&CYFISNP_EEP_DEV_REC_ADR[devId]);
					for(n=0; n<8; n++)
						*p++ = *pFlash++;
				}
			}			
		  SendToHostBLEForNodeInfo(ga8TxBuffer);
		}
	}	
	else if( pBuffer[0] == MSG_HOST_TO_HUB_UNBIND_NODE ) {        // 0x58
		*p++ = MSG_HUB_TO_HOST_UNBIND_NODE;					                // 0x18
		if( (pBuffer[1]>=1) && (pBuffer[1]<=CYFISNP_MAX_NODES) ) {
			*p++ = pBuffer[1];		                                    // Ok (1-63), Nok otherwise
			memset(&pBuffer[2], 0, 8);
			CYFISNP_devRecWrite(pBuffer[1], (CYFISNP_EEP_DEV_REC*)&pBuffer[2]);
		} else
			*p++ = 0xFF;			// Error
		SendToHost(ga8TxBuffer,USB_or_BLE);
	}
	else if( pBuffer[0] == MSG_HOST_TO_HUB_BCD_CMD )              // 0x59
	{
		*p++ = MSG_HUB_TO_HOST_BCD_CMD;		     			                // 0x19
		if( (pBuffer[1]>=1) && (pBuffer[1]<=CYFISNP_MAX_NODES) ) 
		{
		  if(CYFISNP_TxdataPend(pBuffer[1]) != CYFISNP_TX_data_PENDING)
			{
         putBcdPacket(pBuffer[1], pBuffer[2]);
			  *p++ = pBuffer[1];		// Ok (1-103), Nok otherwise
			  *p++ = pBuffer[2];		// Ok, CMD
			  
			   RED_LED_On();	gu8LedTime = 1;	                       // will turn off after timer expires in the main()				
			   CYFISNP_TimeSet(&gu32LedFlashTimer, BLINK_TIME2*2);			   			   			  
			}
			else
			{
			  *p++ = 0xFF;			// Error			
			}
		} 
		else
			*p++ = 0xFF;			// Error
		//SendToHost(ga8TxBuffer,USB_or_BLE);
	}	
	else if( pBuffer[0] == MSG_HOST_TO_HUB_PWRON_CMD )        // Reset/Programming
	{
		if(pBuffer[1] == 0x01)
		{
			PROG_Pin_mode(0);			
			PROG_Ctrl_LOW();
			Cy_DelayUS2(1000);	
			BT_RST_LOW();
			Cy_DelayUS2(1000);	
			BT_RST_HIGH();
			Cy_DelayUS2(300*1000);				
			PROG_Pin_mode(1);						
		}
		else if(pBuffer[1] == 0x57)
		{
		//NVIC_SystemReset();
			PROG_Pin_mode(0);			
		//PROG_Ctrl_LOW();
			Cy_DelayUS2(1000);	
			BT_RST_LOW();
			Cy_DelayUS2(1000);	
			BT_RST_HIGH();
			Cy_DelayUS2(300*1000);				
			PROG_Pin_mode(1);						
			
		}
	}
	else if( pBuffer[0] == MSG_HOST_TO_HUB_PROFILE )        // 0x54  // for data Test
	{
		uint8_t tbl_id;
		
		if(pBuffer[1] < 1)
			return -1;
		
		*p++ = MSG_HUB_TO_HOST_PROFILE;		     			                // 0x19
		*p++ = pBuffer[1];
		*p++ = pBuffer[2];		
		
		tbl_id = pBuffer[1] - 1;		
		if(/* (tbl_id > 0) && */(tbl_id < HALF_NODE_SIZE) ) 
		{
			//uint8_t RCC_CSR[4];
			//RCC_CSR[0] = (RCC_CSR_reg >> 24) & 0xFF;
			//RCC_CSR[1] = (RCC_CSR_reg >> 16) & 0xFF;
			//RCC_CSR[2] = (RCC_CSR_reg >>  8) & 0xFF;
			//RCC_CSR[3] = (RCC_CSR_reg >>  0) & 0xFF;
			
			if(pBuffer[2] == 0)
			{			
				(void)memcpy((void*)p,(void*)pNodeTable[0]->nodeProfile[tbl_id].GID, 8 + 6);	   // GID + tAlarm + playerNo + (Name[0] ~ Name[5])
				//(void)memcpy((void*)p,(void*)RCC_CSR, 4);
			}
			else 
			{
				(void)memcpy((void*)p,(void*)&pNodeTable[0]->nodeProfile[tbl_id].name[6], 14);	 // Name[6] ~ Name[19]
			}
		} 
		else if( (tbl_id >= HALF_NODE_SIZE) && (tbl_id < CYFISNP_MAX_NODES) ) 
		{
			tbl_id = tbl_id - HALF_NODE_SIZE;
			if(pBuffer[2] == 0)
			{
				(void)memcpy((void*)p,(void*)pNodeTable[1]->nodeProfile[tbl_id].GID, 8 + 6);	   // GID + tAlarm + playerNo + (Name[0] ~ Name[5])
			}
			else 
			{
				(void)memcpy((void*)p,(void*)&pNodeTable[1]->nodeProfile[tbl_id].name[6], 14);	 // Name[6] ~ Name[19]
			}			
		}
		else
			*p++ = 0xFF;			// Error
		
		SendToHost(ga8TxBuffer,USB_or_BLE);		
	}		
	else if( pBuffer[0] == MSG_HOST_TO_HUB_ENTER_DFU )                                     // 0x54    for BLE Debugging output control
	{								
		//HAL_FLASHEx_OBGetConfig((FLASH_OBProgramInitTypeDef*)&mPOBInit);		
		if(pBuffer[1] == 1)
		{
			CYFISNP_Stop();
      //HAL_SPI_MspDeInit(&hspi1);
			//Radio_MOSI_LOW();
			//Radio_CS_LOW();
      //RF_POWER_Off();	
			//DisableCHG();
			EnterBootMode();
		}
		else
		{
			HAL_FLASH_Unlock();
			__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_OPTVERR); 
			HAL_FLASH_OB_Unlock();
	  //HAL_FLASH_OB_Launch();							
		//NVIC_SystemReset();
			while(1);
		}
		//UART_Send(TxHexBuffer, 3);	
	}	
	else if( pBuffer[0] == MSG_HOST_TO_HUB_BLE_STATUS )    // 0x5C    for BLE status indication
	{

	}
	
	return (0);
  /* USER CODE END 6 */ 
}


static int8_t CUSTOM_HID_OutEvent_FS(uint8_t event_idx, uint8_t state)
{
  /* USER CODE BEGIN 6 */
  return (0);
  /* USER CODE END 6 */
}

/* USER CODE BEGIN 7 */
static void PowerOnPacket_process(uint8_t* RxPacket)
{
		uint8_t seqn   = (RxPacket[4] & 0xF0) >> 4;
		uint8_t len    = (RxPacket[4] & 0x0F) >> 0;
		uint8_t nodeID = RxPacket[1];				
		uint8_t tbl_id = nodeID - 1;
		NODE_PROFILE * pTBL;
		
		if(nodeID < 1)
			return;
		
		if(tbl_id < HALF_NODE_SIZE)
		{
			pTBL = (NODE_PROFILE *)&pNodeTable[0]->nodeProfile[tbl_id];
		}
		else
		{
			 tbl_id = tbl_id - HALF_NODE_SIZE;
			 pTBL = (NODE_PROFILE *)&pNodeTable[1]->nodeProfile[tbl_id];
		}
			
		
		if((seqn == 0)&&(len == 0))
		{					
		}
		else if((seqn == 0)&&(len == 11))
		{
			(void)memcpy((void*)pTBL->GID, &RxPacket[5],           6);          // GID
			(void)memcpy((void*)pTBL->name,&RxPacket[5+6],         5);          // Name[0] ~Name[4]	
		}
		else if((seqn == 1)&&(len == 11))
		{
			(void)memcpy((void*)&pTBL->name[5], &RxPacket[5],      11);         // Name[5]~Name[16]					
		}
		else if((seqn == 2)&&(len == 11))
		{
			(void)memcpy((void*)&pTBL->name[5 + 11], &RxPacket[5], 4);          // Name[17]~Name[19]
			pTBL->playerNo = RxPacket[5+4];                                     // Player Number
		}
		else if((seqn == 3)&&(len == 11))
		{
			pTBL->tAlarm = RxPacket[5+1];                                       // tAlarm
		}
		else if((seqn == 4)&&(len == 3))
		{
			if((nodeID - 1) < HALF_NODE_SIZE)
				Table_Program((uint8_t*)pNodeTable[0], TABLE_SIZE, PROFILE_TABLE0_START_ADDR);
			else
				Table_Program((uint8_t*)pNodeTable[1], TABLE_SIZE, PROFILE_TABLE1_START_ADDR);	
		}				
		else
		{
		}
}

/**
  * @brief  USBD_CUSTOM_HID_SendReport_FS
  *         Send the report to the Host       
  * @param  report: the report to be sent
  * @param  len: the report length
  * @retval Result of the operation: USBD_OK if all operations are OK else USBD_FAIL
  */

int8_t SendToHost ( uint8_t *report, uint8_t USB_or_BLE)
{
		uint8_t len;//mlen,seqn;
	
	  if(!USB_or_BLE)
			USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS, report, USBD_CUSTOMHID_OUTREPORT_BUF_SIZE); 
		else
		{
				TxHexBuffer[0] = 0x02;                       // Uart protocol header
				TxHexBuffer[1] = len = 40;                   // Uart protocol frame data load length
							
				DecodeToHex(report,len/2,&TxHexBuffer[2]);		
				UART_Send(TxHexBuffer, len + 2);
		}
		
		if(report[0] == MSG_HUB_TO_HOST_NODE_DATA)
		{
			if (report[3] == (CHUNK_IMMEDIATE + 1))
			{
			}
			else if (report[3] == (CHUNK_POWER_ON + 1))
			{				
				PowerOnPacket_process(report);
			}
			else
			{
			}
				
		}
    return 0;
}


int8_t SendToHostUSB ( uint8_t *report)
{
		USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS, report, USBD_CUSTOMHID_OUTREPORT_BUF_SIZE); 
    return 0;
}

/*  
static int8_t USBD_CUSTOM_HID_SendReport_FS ( uint8_t *report,uint16_t len)
{
  return USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS, report, len); 
}
*/
void SendToHostBLE ( uint8_t *report )//,uint16_t len)
{
		uint8_t len;
	    
	//if(report[0] == MSG_HUB_TO_HOST_NODE_BLE_INFO)            
		{																
				TxHexBuffer[0] = 0x02;                       // Uart protocol header
				TxHexBuffer[1] = len = 40;                   // Uart protocol frame data load length
							
				DecodeToHex(report,len/2,&TxHexBuffer[2]);		
				UART_Send(TxHexBuffer, len + 2);			
		}
		
		if(report[0] == MSG_HUB_TO_HOST_NODE_DATA)    
		{
			if (report[3] == (CHUNK_IMMEDIATE + 1))
				BUZZ_beep(40);
			else if (report[3] == (CHUNK_POWER_ON + 1))
			{				
				PowerOnPacket_process(report);
			}
			else
			{
			}
		}
}

void SendToHostBLEForNodeInfo ( uint8_t *report )//,uint16_t len)
{
		uint8_t len;
	    
	//if(report[0] == MSG_HUB_TO_HOST_NODE_BLE_INFO)            
		{																
				TxHexBuffer[0] = 0x02;                       // Uart protocol header
				TxHexBuffer[1] = len = 40;                   // Uart protocol frame data load length
							
				DecodeToHex(report,len/2,&TxHexBuffer[2]);		
				UART_SendForNodeInfo(TxHexBuffer, len + 2);			
		}
		
		if(report[0] == MSG_HUB_TO_HOST_NODE_DATA)    
		{
			if (report[3] == (CHUNK_IMMEDIATE + 1))
				BUZZ_beep(40);
			else if (report[3] == (CHUNK_POWER_ON + 1))
			{				
				PowerOnPacket_process(report);
			}
			else
			{
			}
		}
}


void SendToHostBLEUpload ( uint8_t *report )//,uint16_t len)
{
		uint8_t len;
	    
	//if(report[0] == MSG_HUB_TO_HOST_NODE_BLE_INFO)            
		{																
				TxHexBuffer[0] = 0x02;                       // Uart protocol header
				TxHexBuffer[1] = len = 40;                   // Uart protocol frame data load length
							
				DecodeToHex(report,len/2,&TxHexBuffer[2]);		
				UART_SendUpload(TxHexBuffer, len + 2);			
		}
		
		if(report[0] == MSG_HUB_TO_HOST_NODE_DATA)    
		{
			if (report[3] == (CHUNK_IMMEDIATE + 1))
				BUZZ_beep(40);
			else if (report[3] == (CHUNK_POWER_ON + 1))
			{				
				PowerOnPacket_process(report);
			}
			else
			{
			}
		}	
}

/* USER CODE END 7 */

/* USER CODE BEGIN PRIVATE_FUNCTIONS_IMPLEMENTATION */
/* USER CODE END PRIVATE_FUNCTIONS_IMPLEMENTATION */
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

