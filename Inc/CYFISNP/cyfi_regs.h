//*****************************************************************************
//*****************************************************************************
//  FILENAME: cyfi_regs.h
//  Version: 0.99, Updated on 20/07/2013

//
//  DESCRIPTION: <HUB>Star Network Protocol Header
//-----------------------------------------------------------------------------
//  Copyright (c) Cypress Semiconductor 2013. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef _CYFI_REGS_H_
#define _CYFI_REGS_H_

/* Address of CYFI Radio Registers */
#define CHANNEL_ADR                                       (uint8_t)0x00
#define TX_LENGTH_ADR                                     (uint8_t)0x01
#define TX_CTRL_ADR                                       (uint8_t)0x02
#define TX_CFG_ADR                                        (uint8_t)0x03
#define TX_IRQ_STATUS_ADR                                 (uint8_t)0x04
#define RX_CTRL_ADR                                       (uint8_t)0x05
#define RX_CFG_ADR                                        (uint8_t)0x06
#define RX_IRQ_STATUS_ADR                                 (uint8_t)0x07
#define RX_STATUS_ADR                                     (uint8_t)0x08
#define RX_COUNT_ADR                                      (uint8_t)0x09
#define RX_LENGTH_ADR                                     (uint8_t)0x0A
#define PWR_CTRL_ADR                                      (uint8_t)0x0B
#define XTAL_CTRL_ADR                                     (uint8_t)0x0C
#define IO_CFG_ADR                                        (uint8_t)0x0D
#define GPIO_CTRL_ADR                                     (uint8_t)0x0E
#define XACT_CFG_ADR                                      (uint8_t)0x0F
#define FRAMING_CFG_ADR                                   (uint8_t)0x10
#define data32_THOLD_ADR                                  (uint8_t)0x11
#define data64_THOLD_ADR                                  (uint8_t)0x12
#define RSSI_ADR                                          (uint8_t)0x13
#define EOP_CTRL_ADR                                      (uint8_t)0x14
#define CRC_SEED_LSB_ADR                                  (uint8_t)0x15
#define CRC_SEED_MSB_ADR                                  (uint8_t)0x16
#define TX_CRC_LSB_ADR                                    (uint8_t)0x17
#define TX_CRC_MSB_ADR                                    (uint8_t)0x18
#define RX_CRC_LSB_ADR                                    (uint8_t)0x19
#define RX_CRC_MSB_ADR                                    (uint8_t)0x1A
#define TX_OFFSET_LSB_ADR                                 (uint8_t)0x1B
#define TX_OFFSET_MSB_ADR                                 (uint8_t)0x1C
#define MODE_OVERRIDE_ADR                                 (uint8_t)0x1D
#define RX_OVERRIDE_ADR                                   (uint8_t)0x1E
#define TX_OVERRIDE_ADR                                   (uint8_t)0x1F
/* File Registers */
#define TX_BUFFER_ADR                                     (uint8_t)0x20  /* TX Buffer - 16 bytes */
#define RX_BUFFER_ADR                                     (uint8_t)0x21  /* RX Buffer - 16 bytes */
#define SOP_CODE_ADR                                      (uint8_t)0x22  /* Framing Code - 8 bytes */
#define data_CODE_ADR                                     (uint8_t)0x23  /* data Code - 16 bytes */
#define PREAMBLE_ADR                                      (uint8_t)0x24  /* Preamble - 3 bytes*/
#define MFG_ID_ADR                                        (uint8_t)0x25  /* Laser Fuses - 8 bytes (2 hidden) */
/*End of File Registers */
#define XTAL_CFG_ADR                                      (uint8_t)0x26  /* XTAL Startup Delay */
#define CLK_OVERRIDE_ADR                                  (uint8_t)0x27  /*Clock Override */
#define CLK_EN_ADR                                        (uint8_t)0x28  /*Clock Enable */
#define RX_ABORT_ADR                                      (uint8_t)0x29 
#define AUTO_CAL_TIME_ADR                                 (uint8_t)0x32
#define AUTO_CAL_OFFSET_ADR                               (uint8_t)0x35
#define ANALOG_CTRL_ADR                                   (uint8_t)0x39


/*Register Fields and Default Values */
/* Channel register */
#define CHANNEL_RST                                       (uint8_t)0x48
#define CHANNEL_MSK                                       (uint8_t)0x7F
#define CHANNEL_MAX                                       (uint8_t)0x62
#define CHANNEL_MIN                                       (uint8_t)0x00
#define CHANNEL_2P498_GHZ                                 (uint8_t)0x62
#define CHANNEL_2P4_GHZ                                   (uint8_t)0x00

/* TX Length register */
#define TX_LENGTH_RST                                     (uint8_t)0x00
#define TX_LENGTH_MSK                                     (uint8_t)0xFF

/* TX Control register */
#define TX_CTRL_RST                                       (uint8_t)0x03

/* TX_CTRL bit masks */
#define TX_GO                                             (uint8_t)0x80
#define TX_CLR                                            (uint8_t)0x40

/* TX Configuration register */
#define TX_CFG_RST                                        (uint8_t)0x05

/* separate bit field masks */
#define TX_DATCODE_LEN_MSK                                (uint8_t)0x20
#define TX_DATMODE_MSK                                    (uint8_t)0x18
#define PA_VAL_MSK                                        (uint8_t)0x07

/* DATCODE_LEN register masks */
#define DATCODE_LEN_64                                    (uint8_t)0x20
#define DATCODE_LEN_32                                    (uint8_t)0x00

/* DATMODE register masks */
#define DATMODE_1MBPS                                     (uint8_t)0x00
#define DATMODE_8DR                                       (uint8_t)0x08
#define DATMODE_DDR                                       (uint8_t)0x10
#define DATMODE_SDR                                       (uint8_t)0x18

/* PA_SET register masks */
#define PA_N30_DBM                                        (uint8_t)0x00
#define PA_N25_DBM                                        (uint8_t)0x01
#define PA_N20_DBM                                        (uint8_t)0x02
#define PA_N15_DBM                                        (uint8_t)0x03
#define PA_N10_DBM                                        (uint8_t)0x04
#define PA_N5_DBM                                         (uint8_t)0x05
#define PA_0_DBM                                          (uint8_t)0x06
#define PA_4_DBM                                          (uint8_t)0x07

/* TX_IRQ bit masks */
#define XS_IRQ                                            (uint8_t)0x80
#define LV_IRQ                                            (uint8_t)0x40
#define TXB15_IRQ                                         (uint8_t)0x20
#define TXB8_IRQ                                          (uint8_t)0x10
#define TXB0_IRQ                                          (uint8_t)0x08
#define TXBERR_IRQ                                        (uint8_t)0x04
#define TXC_IRQ                                           (uint8_t)0x02
#define TXE_IRQ                                           (uint8_t)0x01

/* RX Control register */
#define RX_CTRL_RST                                       (uint8_t)0x07

/* RX_CTRL bit masks */
#define RX_GO                                             (uint8_t)0x80

/* RX Configuration register */
#define RX_CFG_RST                                        (uint8_t)0x92  /*Default Value */

#define AUTO_AGC_EN                                       (uint8_t)0x80
#define LNA_EN                                            (uint8_t)0x40
#define ATT_EN                                            (uint8_t)0x20
#define HI                                                (uint8_t)0x10
#define LO                                                (uint8_t)0x00
#define FASTTURN_EN                                       (uint8_t)0x08
#define RXOW_EN                                           (uint8_t)0x02
#define VLD_EN                                            (uint8_t)0x01


/*Analog Control Register */
#define ANALOG_CTRL_RST                                   (uint8_t)0x00
#define ALL_SLOW_EN                                       (uint8_t)0x01


/* RX IRQ register */

/* RX_IRQ bit masks */
#define RXOW_IRQ                                          (uint8_t)0x80
#define SOFDET_IRQ                                        (uint8_t)0x40
#define RXB16_IRQ                                         (uint8_t)0x20
#define RXB8_IRQ                                          (uint8_t)0x10
#define RXB1_IRQ                                          (uint8_t)0x08
#define RXBERR_IRQ                                        (uint8_t)0x04
#define RXC_IRQ                                           (uint8_t)0x02
#define RXE_IRQ                                           (uint8_t)0x01

/* RX Status register */
#define RX_ACK                                            (uint8_t)0x80
#define RX_PKTERR                                         (uint8_t)0x40
#define RX_EOPERR                                         (uint8_t)0x20
#define RX_CRC0                                           (uint8_t)0x10
#define RX_BAD_CRC                                        (uint8_t)0x08
#define RX_DATCODE_LEN                                    (uint8_t)0x04
#define RX_DATMODE_MSK                                    (uint8_t)0x03

/* RX Count register */
#define RX_COUNT_RST                                      (uint8_t)0x00
#define RX_COUNT_MSK                                      (uint8_t)0xFF

/* RX Length Field register */
#define RX_LENGTH_RST                                     (uint8_t)0x00
#define RX_LENGTH_MSK                                     (uint8_t)0xFF

/* Power Control register */
#define PWR_CTRL_RST                                      (uint8_t)0xA0
/* single flag bits & multi-bit-field masks */
#define PMU_EN                                            (uint8_t)0x80
#define LV_IRQ_EN                                         (uint8_t)0x40
#define PMU_SEN                                           (uint8_t)0x20
#define PFET_OFF                                          (uint8_t)0x10
#define LV_IRQ_TH_MSK                                     (uint8_t)0x0C
#define PMU_OUTV_MSK                                      (uint8_t)0x03

/* LV_IRQ_TH values */
#define LV_IRQ_TH_1P8_V                                   (uint8_t)0x0C
#define LV_IRQ_TH_2P0_V                                   (uint8_t)0x08
#define LV_IRQ_TH_2P2_V                                   (uint8_t)0x04
#define LV_IRQ_TH_PMU_OUTV                                (uint8_t)0x00

/* PMU_OUTV values */
#define PMU_OUTV_2P4                                      (uint8_t)0x03
#define PMU_OUTV_2P5                                      (uint8_t)0x02
#define PMU_OUTV_2P6                                      (uint8_t)0x01
#define PMU_OUTV_2P7                                      (uint8_t)0x00

/* Crystal Control register */
#define XTAL_CTRL_RST                                     (uint8_t)0x04

/* single flag bits & multi-bit-field masks */
#define XOUT_FNC_MSK                                      (uint8_t)0xC0
#define XS_IRQ_EN                                         (uint8_t)0x20
#define XOUT_FREQ_MSK                                     (uint8_t)0x07

/* XOUT_FNC values */
#define XOUT_FNC_XOUT_FREQ                                (uint8_t)0x00
#define XOUT_FNC_PA_N                                     (uint8_t)0x40
#define XOUT_FNC_RAD_STREAM                               (uint8_t)0x80
#define XOUT_FNC_GPIO                                     (uint8_t)0xC0

/* XOUT_FREQ values */
#define XOUT_FREQ_12MHZ                                   (uint8_t)0x00
#define XOUT_FREQ_6MHZ                                    (uint8_t)0x01
#define XOUT_FREQ_3MHZ                                    (uint8_t)0x02
#define XOUT_FREQ_1P5MHZ                                  (uint8_t)0x03
#define XOUT_FREQ_P75MHZ                                  (uint8_t)0x04

/* I/O Configuration register */
#define IO_CFG_RST                                        (uint8_t)0x00
#define IO_CFG_MSK                                        (uint8_t)0xFF

/* single flag bits & multi-bit-field masks */
#define IRQ_OD                                            (uint8_t)0x80
#define IRQ_POL                                           (uint8_t)0x40
#define MISO_OD                                           (uint8_t)0x20
#define XOUT_OD                                           (uint8_t)0x10
#define PACTL_OD                                          (uint8_t)0x08
#define PACTL_GPIO                                        (uint8_t)0x04
#define SPI_3_PIN                                         (uint8_t)0x02
#define IRQ_GPIO                                          (uint8_t)0x01

/* GPIO Control register */
#define GPIO_CTRL_RST                                     (uint8_t)0x00
#define GPIO_CTRL_MSK                                     (uint8_t)0xF0

/* single flag bits & multi-bit-field masks */
#define XOUT_OP                                           (uint8_t)0x80
#define MISO_OP                                           (uint8_t)0x40
#define PACTL_OP                                          (uint8_t)0x20
#define IRQ_OP                                            (uint8_t)0x10
#define XOUT_IP                                           (uint8_t)0x08
#define MISO_IP                                           (uint8_t)0x04
#define PACTL_IP                                          (uint8_t)0x02
#define IRQ_IP                                            (uint8_t)0x01

/* Transaction Configuration register */
#define XACT_CFG_RST                                      (uint8_t)0x80

/* single flag bits & multi-bit-field masks */
#define ACK_EN                                            (uint8_t)0x80
#define FRC_END_STATE                                     (uint8_t)0x20
#define END_STATE_MSK                                     (uint8_t)0x1C
#define ACK_TO_MSK                                        (uint8_t)0x03

/* END_STATE field values */
#define END_STATE_SLEEP                                   (uint8_t)0x00
#define END_STATE_IDLE                                    (uint8_t)0x04
#define END_STATE_TXSYNTH                                 (uint8_t)0x08
#define END_STATE_RXSYNTH                                 (uint8_t)0x0C
#define END_STATE_RX                                      (uint8_t)0x10

/* ACK_TO field values */
#define ACK_TO_4X                                         (uint8_t)0x00
#define ACK_TO_8X                                         (uint8_t)0x01
#define ACK_TO_12X                                        (uint8_t)0x02
#define ACK_TO_15X                                        (uint8_t)0x03

/* Framing Configuration register */
#define FRAMING_CFG_RST                                   (uint8_t)0xA5
/* single flag bits & multi-bit-field masks */
#define SOP_EN                                            (uint8_t)0x80
#define SOP_LEN                                           (uint8_t)0x40
#define LEN_EN                                            (uint8_t)0x20
#define SOP_THRESH_MSK                                    (uint8_t)0x1F

/* data Threshold 32 register */
#define DAT32_THRESH_RST                                  (uint8_t)0x05
#define DAT32_THRESH_MSK                                  (uint8_t)0x0F

/* data Threshold 64 register */
#define DAT64_THRESH_RST                                  (uint8_t)0x0E
#define DAT64_THRESH_MSK                                  (uint8_t)0x1F

/* RSSI register */
#define RSSI_RST                                          (uint8_t)0x20
/* single flag bits & multi-bit-field masks */
#define SOP_RSSI                                          (uint8_t)0x80
#define LNA_STATE                                         (uint8_t)0x20
#define RSSI_LVL_MSK                                      (uint8_t)0x1F

/* EOP Control register */
#define EOP_CTRL_RST                                      (uint8_t)0xA4
/* single flag bits & multi-bit-field masks */
#define HINT_EN                                           (uint8_t)0x80
#define HINT_EOP_MSK                                      (uint8_t)0x70
#define EOP_MSK                                           (uint8_t)0x0F

/* CRC Seed registers */
#define CRC_SEED_LSB_RST                                  (uint8_t)0x00
#define CRC_SEED_MSB_RST                                  (uint8_t)0x00
/* CRC related values USB CRC-16 */
#define CRC_POLY_MSB                                      (uint8_t)0x80
#define CRC_POLY_LSB                                      (uint8_t)0x05
#define CRC_RESI_MSB                                      (uint8_t)0x80
#define CRC_RESI_LSB                                      (uint8_t)0x0D

/* RX CRC Field registers */
#define RX_CRC_LSB_RST                                    (uint8_t)0xFF
#define RX_CRC_MSB_RST                                    (uint8_t)0xFF

/* Synth Offset registers */
#define TX_OFFSET_LSB_RST                                 (uint8_t)0x00
#define TX_OFFSET_MSB_RST                                 (uint8_t)0x00

/* single flag bits & multi-bit-field masks */
#define STRIM_MSB_MSK                                     (uint8_t)0x0F
#define STRIM_LSB_MSK                                     (uint8_t)0xFF

/* Mode Override register */
#define MODE_OVERRIDE_RST                                 (uint8_t)0x00
#define FRC_AWAKE                                         (uint8_t)0x03
#define FRC_AWAKE_OFF_1                                   (uint8_t)0x01
#define FRC_AWAKE_OFF_2                                   (uint8_t)0x00
/* single flag bits & multi-bit-field masks */
#define DIS_AUTO_SEN                                      (uint8_t)0x80
#define SEN_TXRXB                                         (uint8_t)0x40
#define FRC_SEN                                           (uint8_t)0x20
#define FRC_AWAKE_MSK                                     (uint8_t)0x18
#define MODE_OVRD_FRC_AWAKE                               (uint8_t)0x18
#define MODE_OVRD_FRC_AWAKE_OFF_1                         (uint8_t)0x08
#define MODE_OVRD_FRC_AWAKE_OFF_2                         (uint8_t)0x00
#define RST                                               (uint8_t)0x01
#define FRC_PA                                            (uint8_t)0x02

/* RX Override register */
#define RX_OVERRIDE_RST                                   (uint8_t)0x00
/* single flag bits & multi-bit-field masks */
#define ACK_RX                                            (uint8_t)0x80
#define EXTEND_RX_TX                                      (uint8_t)0x40
#define MAN_RXACK                                         (uint8_t)0x20
#define FRC_RXDR                                          (uint8_t)0x10
#define DIS_CRC0                                          (uint8_t)0x08
#define DIS_RXCRC                                         (uint8_t)0x04
#define ACE                                               (uint8_t)0x02

/* TX Override register */
#define TX_OVERRIDE_RST                                   (uint8_t)0x00
/* single flag bits & multi-bit-field masks */
#define ACK_TX_SEN                                        (uint8_t)0x80
#define FRC_PREAMBLE                                      (uint8_t)0x40
#define DIS_TX_RETRANS                                    (uint8_t)0x20
#define MAN_TXACK                                         (uint8_t)0x10
#define OVRRD_ACK                                         (uint8_t)0x08
#define DIS_TXCRC                                         (uint8_t)0x04
#define CO                                                (uint8_t)0x02
#define CYFISNP_TXINV                                     (uint8_t)0x01



/*      File Function Detail */
#define CODESTORE_BYTE7_SOF_RST                           (uint8_t)0x17
#define CODESTORE_BYTE6_SOF_RST                           (uint8_t)0xFF
#define CODESTORE_BYTE5_SOF_RST                           (uint8_t)0x9E
#define CODESTORE_BYTE4_SOF_RST                           (uint8_t)0x21
#define CODESTORE_BYTE3_SOF_RST                           (uint8_t)0x36
#define CODESTORE_BYTE2_SOF_RST                           (uint8_t)0x90
#define CODESTORE_BYTE1_SOF_RST                           (uint8_t)0xC7
#define CODESTORE_BYTE0_SOF_RST                           (uint8_t)0x82


#define CODESTORE_BYTE7_DCODE0_RST                        (uint8_t)0x01
#define CODESTORE_BYTE6_DCODE0_RST                        (uint8_t)0x2B
#define CODESTORE_BYTE5_DCODE0_RST                        (uint8_t)0xF1
#define CODESTORE_BYTE4_DCODE0_RST                        (uint8_t)0xDB
#define CODESTORE_BYTE3_DCODE0_RST                        (uint8_t)0x01
#define CODESTORE_BYTE2_DCODE0_RST                        (uint8_t)0x32
#define CODESTORE_BYTE1_DCODE0_RST                        (uint8_t)0xBE
#define CODESTORE_BYTE0_DCODE0_RST                        (uint8_t)0x6F

#define CODESTORE_BYTE7_DCODE1_RST                        (uint8_t)0x02
#define CODESTORE_BYTE6_DCODE1_RST                        (uint8_t)0xF9
#define CODESTORE_BYTE5_DCODE1_RST                        (uint8_t)0x93
#define CODESTORE_BYTE4_DCODE1_RST                        (uint8_t)0x97
#define CODESTORE_BYTE3_DCODE1_RST                        (uint8_t)0x02
#define CODESTORE_BYTE2_DCODE1_RST                        (uint8_t)0xFA
#define CODESTORE_BYTE1_DCODE1_RST                        (uint8_t)0x5C
#define CODESTORE_BYTE0_DCODE1_RST                        (uint8_t)0xE3

#define PREAMBLE_CODE_MSB_RST                             (uint8_t)0x33
#define PREAMBLE_CODE_LSB_RST                             (uint8_t)0x33
#define PREAMBLE_LEN_RST                                  (uint8_t)0x02

#define XTAL_CFG_RST                                      (uint8_t)0x00

/* Clock Override */
#define CLK_OVERRIDE_RST                                  (uint8_t)0x00 /* Clock Override Default Value */
#define RXF                                               (uint8_t)0x02

/* Clock Enable Register */
#define CLK_EN_RST                                        (uint8_t)0x00 /*Clock Enable Default Value */
#define RXF                                               (uint8_t)0x02

/* Receiver Abort Register */
#define RX_ABORT_RST                                      (uint8_t)0x00 
#define ABORT_EN                                          (uint8_t)0x20

/* Auto Calibration Time */
#define AUTO_CAL_TIME_RST                                 (uint8_t)0x0C
#define AUTO_CAL_TIME_MAX                                 (uint8_t)0x3C

/* Auto Calibration Offset */
#define AUTO_CAL_OFFSET_RST                               (uint8_t)0x00
#define AUTO_CAL_OFFSET_MINUS_4                           (uint8_t)0x14


#endif /*_CYFIREGS_H_*/
