//*****************************************************************************
//*****************************************************************************
//  FILENAME: CYFISNP_Config.h
//  Version:0.99 , Updated on 20/07/2013
//
//  DESCRIPTION: <HUB> Star Network Protocol User and Target Configuration
//-----------------------------------------------------------------------------
//  Copyright (c) Cypress Semiconductor 2013. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef CYFISNP_CONFIG_H
#define CYFISNP_CONFIG_H

#include "main.h"

// ---------------------------------------------------------------------------
// CYFISNP_MAX_NODES 
// ---------------------------------------------------------------------------
#define CYFISNP_MAX_NODES       103                           // 63//127//0x0a
#define NET_PARAMETER_ADDR      (uint32_t)ADDR_FLASH_PAGE_31   //0x0807800    // Binding data is stored at here

// ---------------------------------------------------------------------------
// CYFISNP_BCD_PAYLOAD_MAX 
// ---------------------------------------------------------------------------
#define CYFISNP_BCD_PAYLOAD_MAX 0x01//0x05 // Backward ch data payload


// ---------------------------------------------------------------------------
// FCD_PAYLOAD_MAX - Minor RAM usage, not designed to be changed
// ---------------------------------------------------------------------------
#define CYFISNP_FCD_PAYLOAD_MAX 14   // Fwd Ch data Payload len (up to 14)


// ---------------------------------------------------------------------------
//
// External PA installed 
//
// ---------------------------------------------------------------------------
#define CYFISNP_EXTERNAL_PA    1     // 1 if external PA is used


// ---------------------------------------------------------------------------
//
// Set the Hub's PA Level for all Hub transmissions (set manually here)
//
// ---------------------------------------------------------------------------
#ifdef CYFISNP_PROTOCOL_C  // Visibility only in <protocol.c>
// ---------------------------------------------------------------------------
// Table of possible PA levels
// ---------------------------------------------------------------------------
#define PA0         0x00            // typically -35 dBm
#define PA1         0x01            // typically -30 dBm
#define PA2         0x02            // typically -24 dBm
#define PA3         0x03            // typically -18 dBm
#define PA4         0x04            // typically -13 dBm
#define PA5         0x05            // typically - 5 dBm
#define PA6         0x06            // typically   0 dBm
#define PA7         0x07            // typically  +4 dBm
// The following are only available when an external PA is installed
#if CYFISNP_EXTERNAL_PA
#define EXT_PA_ON   0x10
#define PA0_PLUS    (PA0+EXT_PA_ON) // typically external_PA_gain + -35 dBm
#define PA1_PLUS    (PA1+EXT_PA_ON) // typically external_PA_gain + -30 dBm
#define PA2_PLUS    (PA2+EXT_PA_ON) // typically external_PA_gain + -24 dBm
#define PA3_PLUS    (PA3+EXT_PA_ON) // typically external_PA_gain + -18 dBm
#define PA4_PLUS    (PA4+EXT_PA_ON) // typically external_PA_gain + -13 dBm
#define PA5_PLUS    (PA5+EXT_PA_ON) // typically external_PA_gain + - 5 dBm
#define PA6_PLUS    (PA6+EXT_PA_ON) // typically external_PA_gain +   0 dBm
#define PA7_PLUS    (PA7+EXT_PA_ON) // typically external_PA_gain +  +4 dBm
#endif  // CYFISNP_EXTERNAL_PA

#if !CYFISNP_EXTERNAL_PA
    #define PA_LEVEL_USED   PA6
#else   
    #define PA_LEVEL_USED   PA6_PLUS    // ~+10dBm for FirstTouch RF kit
#endif  // CYFISNP_EXTERNAL_PA
#endif   // CYFISNP_PROTOCOL_C


// ---------------------------------------------------------------------------
// AUTO_ASSIGN_DEVICE_ID (or "on-the-fly) means hub assigns any ID it wants.
//  The alternative is for the node to request an ID and the hub assigns that
//   ID (possibly replacing an prior node).
// ---------------------------------------------------------------------------
#define AUTO_ASSIGN_DEVICE_ID   0x1


// ---------------------------------------------------------------------------
//
// Bind Mode Network Parameters used during Hub/Node Bind Req/Rsp.  Casual
//   User's won't change bind sop/seed, but someday someone may want to change.
//   Hub and Node settings MUST match to bind.
// ---------------------------------------------------------------------------

#define CYFISNP_BIND_MODE_SOP           0       // {0-9} = Bind SOP Code
#define CYFISNP_BIND_MODE_CRC_SEED      0       //    0  = Bind CRC Seed

// ---------------------------------------------------------------------------

#ifdef CYFISNP_PROTOCOL_C  // Visibility only in <protocol.c>
// ---------------------------------------------------------------------------
//
// Bind Mode Network Channels used during Hub/Node Bind Req/Rsp.
//   Hub and Node settings MUST match to bind.
//
// NOTE: this table uses "channel" numbers, and are 2MHz LESS THAN frequency.
//       So Channel 0 = 2.402GHz and Channel 77 = 2.479GHz
// ---------------------------------------------------------------------------
const unsigned char CYFISNP_BIND_CH_SEQ[] = {
	  0,      //10,    
    14,    
    31,      
    46,
    56,
    0x4d    //58      
    };
    
    

#endif  // CYFISNP_PROTOCOL_C
#endif  // CYFISNP_CONFIG_H
// ###########################################################################
