//*****************************************************************************
//*****************************************************************************
//  FILENAME: CYFISNP_Flash_Utility.h
//  Version:0.99 , Updated on 29/08/2017
//
//  DESCRIPTION: <HUB> CYFISNP Protocol Net/device Store
//-----------------------------------------------------------------------------
//  Copyright (c) Artaflex INC 2017. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef CYFISNP_FLASH_UTILITY_H
#define CYFISNP_FLASH_UTILITY_H

#include "typedef.h"
#include "cyfi_hal.h"
#include "CYFISNP_Protocol.h"

#define PROFILE_TABLE0_START_ADDR   ADDR_FLASH_PAGE_32
#define PROFILE_TABLE1_START_ADDR   ADDR_FLASH_PAGE_33

#define MAGIC_SIGNATURE        0x4546
#define HALF_NODE_SIZE         (CYFISNP_MAX_NODES/2 + 1)
typedef struct 
{
//byte magic; 
	uint8_t GID[6]; 
  uint8_t tAlarm;
//uint8_t tR;
//uint8_t AlarmMode;
//uint8_t lpEnable;
	uint8_t playerNo;
	uint8_t name[20];
//uint8_t loc[9];
//uint8_t pwrMode;
//uint8_t proxEnable;
//uint8_t mntLoc;    
} NODE_PROFILE;

#define NODE_PROFILE_SIZE     sizeof(NODE_PROFILE)
#define TABLE_SIZE            (NODE_PROFILE_SIZE*HALF_NODE_SIZE + 2)
	
typedef struct 
{
	uint16_t magic; 
	NODE_PROFILE nodeProfile[HALF_NODE_SIZE];
	uint8_t RESV[2048 - TABLE_SIZE];
} NODE_TBL;

extern NODE_TBL *pNodeTable[2];
extern NODE_TBL *pNodeTableFlash[2];

uint8_t Flash_Program(uint8_t *WriteBuffer, uint16_t len);

void Node_Table_Init(void);
uint8_t Table_Program(uint8_t *WriteBuffer, uint16_t len, uint32_t table_addr);

#endif // CYFISNP_FLASH_UTILITY_H
