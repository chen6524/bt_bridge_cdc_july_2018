/**
  ******************************************************************************
  * File Name          : gpio.h
  * Description        : This file contains all the functions prototypes for 
  *                      the gpio  
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __gpio_H
#define __gpio_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN Private defines */
	 
//#define Radio_MISO_Pin GPIO_PIN_4
//#define Radio_MISO_GPIO_Port GPIOB
//#define Radio_CLK_Pin GPIO_PIN_3
//#define Radio_CLK_GPIO_Port GPIOB
//#define Radio_MOSI_Pin GPIO_PIN_5
//#define Radio_MOSI_GPIO_Port GPIOB
//#define IRQ_RF_Pin GPIO_PIN_0
//#define IRQ_RF_GPIO_Port GPIOA
//#define Radio_CS_Pin GPIO_PIN_4
//#define Radio_CS_GPIO_Port GPIOA


#define Radio_MOSI_High()   HAL_GPIO_WritePin(Radio_MOSI_GPIO_Port, Radio_MOSI_Pin, GPIO_PIN_SET)
#define Radio_MOSI_LOW()    HAL_GPIO_WritePin(Radio_MOSI_GPIO_Port, Radio_MOSI_Pin, GPIO_PIN_RESET)

#define Radio_CS_High()     HAL_GPIO_WritePin(Radio_CS_GPIO_Port, Radio_CS_Pin, GPIO_PIN_SET)
#define Radio_CS_LOW()      HAL_GPIO_WritePin(Radio_CS_GPIO_Port, Radio_CS_Pin, GPIO_PIN_RESET)

#define Radio_Int_Disable() HAL_NVIC_DisableIRQ(EXTI0_IRQn)
#define Radio_Int_Enable()  HAL_NVIC_Enable(EXTI0_IRQn)
	
#define BT_RST_HIGH()       HAL_GPIO_WritePin(BT_RST_GPIO_Port, BT_RST_Pin, GPIO_PIN_SET)
#define BT_RST_LOW()        HAL_GPIO_WritePin(BT_RST_GPIO_Port, BT_RST_Pin, GPIO_PIN_RESET)
 
#define PROG_Ctrl_HIGH()    HAL_GPIO_WritePin(PROG_Ctrl_GPIO_Port, PROG_Ctrl_Pin, GPIO_PIN_SET)
#define PROG_Ctrl_LOW()     HAL_GPIO_WritePin(PROG_Ctrl_GPIO_Port, PROG_Ctrl_Pin, GPIO_PIN_RESET)
//#define PROG_Ctrl_Init()   HAL_GPIO_Init(PROG_Ctrl_GPIO_Port, PROG_Ctrl_Pin)
#define PROG_Ctrl_DeInit()  HAL_GPIO_DeInit(PROG_Ctrl_GPIO_Port, PROG_Ctrl_Pin)

#define Radio_RST_HIGH()    HAL_GPIO_WritePin(nReset_GPIO_Port, nReset_Pin, GPIO_PIN_SET)
#define Radio_RST_LOW()     HAL_GPIO_WritePin(nReset_GPIO_Port, nReset_Pin, GPIO_PIN_RESET)
	 
#define RF_POWER_On()       HAL_GPIO_WritePin(RF_ON_GPIO_Port, RF_ON_Pin, GPIO_PIN_SET)
#define RF_POWER_Off()      HAL_GPIO_WritePin(RF_ON_GPIO_Port, RF_ON_Pin, GPIO_PIN_RESET)
#define IRQ_RF_STATUS()     HAL_GPIO_ReadPin(IRQ_RF_GPIO_Port, IRQ_RF_Pin)
/* USER CODE END Private defines */

void MX_GPIO_Init(void);

/* USER CODE BEGIN Prototypes */
void PROG_Pin_mode(uint8_t mode);
/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ pinoutConfig_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
